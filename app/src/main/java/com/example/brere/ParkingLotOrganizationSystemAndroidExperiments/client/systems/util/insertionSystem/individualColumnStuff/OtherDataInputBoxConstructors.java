package com.example.brere.ParkingLotOrganizationSystemAndroidExperiments.client.systems.util.insertionSystem.individualColumnStuff;

import android.support.annotation.NonNull;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.text.method.DigitsKeyListener;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.example.brere.ParkingLotOrganizationSystemAndroidExperiments.client.dataFieldStuff.DataField;
import com.example.brere.ParkingLotOrganizationSystemAndroidExperiments.client.dataFieldStuff.DataFieldList;

/**
 * this is a class full of all of the DataInputBoxConstructors that are simple, and only require
 * modification of pre existing DataInputBoxes to function
 */
public class OtherDataInputBoxConstructors {
	/**
	 * creates a DataInputBoxConstructor for a DefaultDataInputBox which is used to enter passwords
	 * @return a DataInputBoxConstructor for a DefaultDataInputBox which is used to enter passwords
	 */
	public static DataInputBox.DataInputBoxConstructor constructPasswordDataFieldInputBox() {
		return DefaultDataInputBox.constructDataFieldInputBoxWithCustomDataField(InputType.TYPE_CLASS_TEXT |
						InputType.TYPE_TEXT_VARIATION_PASSWORD);
	}

	/**
	 * creates a DataInputBoxConstructor for a DefaultDataInputBox which is used to enter simple numbers
	 * @return a DataInputBoxConstructor for a DefaultDataInputBox which is used to enter simple numbers
	 */
	public static DataInputBox.DataInputBoxConstructor constructNumberDataFieldInputBox() {
		return DefaultDataInputBox.constructDataFieldInputBoxWithCustomDataField(InputType.TYPE_CLASS_NUMBER);
	}
	/**
	 * creates a DataInputBoxConstructor for a DefaultDataInputBox which is used to big peices of data
	 * @return a DataInputBoxConstructor for a DefaultDataInputBox which is used to enter big peices of data
	 */
	public static DataInputBox.DataInputBoxConstructor constructBigDataFieldInputBox() {
		return new DataInputBox.DataInputBoxConstructor() {
			private static final long serialVersionUID = 229830L;

			@Override
			public DataInputBox constructDataInputBox(DataField d, DataFieldList listFieldBelongsTo, @NonNull ViewGroup parent, int viewType) {
				DefaultDataInputBox box=(DefaultDataInputBox) DefaultDataInputBox.constructDataFieldInputBox().constructDataInputBox(d,listFieldBelongsTo,parent,viewType);
				box.getField().setSingleLine(false);
				box.getField().setLayoutParams(new LinearLayout.LayoutParams(1000,LinearLayout.LayoutParams.WRAP_CONTENT));
				return box;
			}
		};
	}
	/**
	 * creates a DataInputBoxConstructor for a DefaultDataInputBox which is used to enter dates
	 * @return a DataInputBoxConstructor for a DefaultDataInputBox which is used to enter dates
	 */
	public static DataInputBox.DataInputBoxConstructor constructDateDataFieldInputBox() {
		return new DataInputBox.DataInputBoxConstructor() {
			private static final long serialVersionUID = 22430L;

			@Override
			public DataInputBox constructDataInputBox(DataField d, DataFieldList listFieldBelongsTo, @NonNull ViewGroup parent, int viewType) {
				DefaultDataInputBox box=(DefaultDataInputBox) DefaultDataInputBox.constructDataFieldInputBoxWithCustomDataField(InputType.TYPE_CLASS_NUMBER|InputType.TYPE_NUMBER_VARIATION_NORMAL).constructDataInputBox(d,listFieldBelongsTo,parent,viewType);
				box.getField().setKeyListener(DigitsKeyListener.getInstance("0123456789/"));

				box.getField().addTextChangedListener(new TextWatcher() {
					private boolean delete;
					@Override
					public void beforeTextChanged(CharSequence charSequence,  int start,int count,int after) {
						delete=false;
						if(after==0){
							delete=true;
						}
					}

					@Override
					public void onTextChanged(CharSequence charSequence, int start, int before, int count) {

					}

					@Override
					public void afterTextChanged(Editable editable) {

						if(editable.length()==2&&!delete){
							editable.append('/');
						}
						else if(editable.length()==5&&!delete){
							editable.append('/');
						}

						else if(!delete&&(editable.length()>2&&editable.charAt(2)!='/'||editable.length()>5&&editable.charAt(5)!='/')){
							String s=editable.toString();
							StringBuffer withoutSlashes=new StringBuffer(s.replace("/",""));
							withoutSlashes.insert(2,'/');
							if(withoutSlashes.length()>4){
								withoutSlashes.insert(5,'/');
							}
							editable.clear();
							editable.append(withoutSlashes);
						}
					}

				});
				return box;
			}
		};
	}
	/**
	 * creates a DataInputBoxConstructor for a DefaultDataInputBox which is used to enter phone numbers
	 * @return a DataInputBoxConstructor for a DefaultDataInputBox which is used to enter phone numbers
	 */
	public static DataInputBox.DataInputBoxConstructor constructPhoneDataFieldInputBox() {
		return new DataInputBox.DataInputBoxConstructor() {
			private static final long serialVersionUID = 22420L;

			@Override
			public DataInputBox constructDataInputBox(DataField d, DataFieldList listFieldBelongsTo, @NonNull ViewGroup parent, int viewType) {
				DefaultDataInputBox box=(DefaultDataInputBox) DefaultDataInputBox.constructDataFieldInputBoxWithCustomDataField(InputType.TYPE_CLASS_NUMBER|InputType.TYPE_NUMBER_VARIATION_NORMAL).constructDataInputBox(d,listFieldBelongsTo,parent,viewType);
				box.getField().setKeyListener(DigitsKeyListener.getInstance("0123456789-"));

				box.getField().addTextChangedListener(new TextWatcher() {
					private boolean delete;
					@Override
					public void beforeTextChanged(CharSequence charSequence,  int start,int count,int after) {
						delete=false;
						if(after==0){
							delete=true;
						}
					}

					@Override
					public void onTextChanged(CharSequence charSequence, int start, int before, int count) {

					}

					@Override
					public void afterTextChanged(Editable editable) {

						if(editable.length()==3&&!delete){
							editable.append('-');
						}
						else if(editable.length()==7&&!delete){
							editable.append('-');
						}

						else if(!delete&&(editable.length()>3&&editable.charAt(3)!='-'||editable.length()>7&&editable.charAt(7)!='-')){
							String s=editable.toString();
							StringBuffer withoutSlashes=new StringBuffer(s.replace("-",""));
							withoutSlashes.insert(3,'-');
							if(withoutSlashes.length()>6){
								withoutSlashes.insert(7,'-');
							}
							editable.clear();
							editable.append(withoutSlashes);
						}
					}

				});
				return box;
			}
		};
	}

}
