package com.example.brere.ParkingLotOrganizationSystemAndroidExperiments.shared.sentObjects.clientToServer.dataBaseFunctions;

import com.example.brere.ParkingLotOrganizationSystemAndroidExperiments.client.dataFieldStuff.DataFieldList;
import com.example.brere.ParkingLotOrganizationSystemAndroidExperiments.shared.sentObjects.ServerToClient.ErrorData;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
/**
 * this is a class for modifying tuples in the database. the class inputs a
 * datafeildList (basically a tuple in the table) and modifies the coresponding
 * tuple in the database based on the tuples keys
 *
 * it looks like UPDATE +tablename+ SET (column1=data1,column2=data2,...) where
 * key1=datak1 AND key2=datak2 ...
 *
 * @author brere
 */
public class ModificationDataBaseFunction extends DataBaseFunction{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -5598726142570671411L;
	/**
	 * Instantiates a new modification data base function.
	 *
	 * @param data the dataset of the freshly modified data
	 */
	public ModificationDataBaseFunction(DataFieldList data) {
		super(data,"modification");
	}
	/* (non-Javadoc)
	 * @see shared.serverClientCommunicationFramework.serverOrdersFromClientToServer.dataBaseFunctions.DataBaseFunction#check(client.dataFieldStuff.DataFieldList)
	 */
	@Override
	protected void check(DataFieldList data) {
		if(!data.hasAllKeys()) {
			throw new IllegalArgumentException("error, you tried to modify a thing without giving the keys");
		}
	}

	/**
	 * Searches the Database to check if all of the primary keys exist.
	 *
	 * @param jdbcConnection the jdbc connection it uses to check
	 * @throws SQLException the SQL exception if there is an sql exception while checking the keys
	 */
	private void checkDBPrimaryKeys(Connection jdbcConnection) throws SQLException{
		SearchDataBaseFunction search=new SearchDataBaseFunction(super.getData().getKeys());
		ArrayList<DataFieldList> searchResult = search.execute(jdbcConnection);
			if(searchResult.isEmpty()){
				throw new IllegalArgumentException("We were unable to find your thing to modify " + this.getClass().getName());
			}
		
	}
	/* (non-Javadoc)
	 * @see shared.serverClientCommunicationFramework.serverOrdersFromClientToServer.dataBaseFunctions.DataBaseFunction#execute(java.sql.Connection)
	 */
	@Override
	public ArrayList<DataFieldList> execute(Connection jdbcConnection) throws SQLException {
		checkDBPrimaryKeys(jdbcConnection);
		String sql = "UPDATE " + super.getData().getTable().getDataBaseTitle() + " SET ";
		int i=0;

		for( i=0;i<super.getData().size()-1;i++) {	
			sql+=" "+super.getData().get(i).getColumn().getDataBaseColumnName()+"= ?,";
		}
		sql+=" "+super.getData().get(i).getColumn().getDataBaseColumnName()+"= ?";
		
		//gets the label of the primary key.  ASSUMES primary key HAS A COLUMN NUBMER OF 1.
		DataFieldList primaryKeys=super.getData().getKeys();

		sql+=" WHERE "+super.createOneSearchComparer(primaryKeys.get(0));
		
		for (int j = 1; j < primaryKeys.size(); j++) {
			sql += " AND " + super.createOneSearchComparer(primaryKeys.get(j));
		}
		
			PreparedStatement statement = jdbcConnection.prepareStatement(sql);
			int statementNumber=1;
			
			for( int k =0;k<super.getData().size();k++) {	
				statement.setString(statementNumber, super.getData().get(k).getData());
				statementNumber++;
			}
			for(int k=0;k<primaryKeys.size();k++) {
				statement.setString(statementNumber, primaryKeys.get(k).getData());
				statementNumber++;
			}
			statement.executeUpdate();
			
		ArrayList<DataFieldList> lists=new ArrayList<DataFieldList>();
		lists.add(super.getData());

		return lists;
	}
	/* (non-Javadoc)
	 * @see shared.serverClientCommunicationFramework.serverOrdersFromClientToServer.dataBaseFunctions.DataBaseFunction#giveErrorMessage(java.lang.Exception, java.sql.Connection)
	 */
	@Override
	protected ErrorData giveErrorMessage(Exception e, Connection jdbcConnection) throws SQLException {
		ErrorData s=super.checkRefrencesInThisList(jdbcConnection, super.getData());
		if(s!=null) {
			return s;
		}
		return super.doStandardErrorProcedure(e);
	}

}