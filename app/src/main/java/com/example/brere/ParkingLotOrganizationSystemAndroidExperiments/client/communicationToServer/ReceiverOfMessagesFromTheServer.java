package com.example.brere.ParkingLotOrganizationSystemAndroidExperiments.client.communicationToServer;

import android.os.Handler;
import android.os.Looper;

import com.example.brere.ParkingLotOrganizationSystemAndroidExperiments.shared.sentObjects.ServerToClient.ClientsInstructionsForReceivingData;
import com.example.brere.ParkingLotOrganizationSystemAndroidExperiments.shared.sentObjects.ServerToClient.MessageFromServerToClient;

import java.util.ArrayList;
import java.util.function.Consumer;

/**
 * this class recives messages from the server, and each time it does, it executes their printMessage function.
 * ie this should probably be a lambda expression or something,
 * @author brere
 *
 */
//TODO turn this into a lambda expression for simplicity

public class ReceiverOfMessagesFromTheServer implements Consumer<MessageFromServerToClient>{
	private ArrayList<ClientsInstructionsForReceivingData<?>> storage;
	private Consumer<String> errorMessageDisplayer;
	private Consumer<Runnable> uiThread=new Handler(Looper.getMainLooper())::post;
	public ReceiverOfMessagesFromTheServer(ArrayList<ClientsInstructionsForReceivingData<?>> storage,Consumer<String>errorMessageDisplayer) {
		this.storage=storage;
		this.errorMessageDisplayer=errorMessageDisplayer;
	}
	@Override
	public void accept(MessageFromServerToClient list) {
		uiThread.accept(()->{
			try {
				list.printMessage(storage);
			}catch(IllegalArgumentException e) {
				errorMessageDisplayer.accept(e.getMessage());
			}
		});

	}

}
