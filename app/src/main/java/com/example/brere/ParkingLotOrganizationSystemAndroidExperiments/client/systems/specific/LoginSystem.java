package com.example.brere.ParkingLotOrganizationSystemAndroidExperiments.client.systems.specific;

import android.content.Intent;
import android.os.Bundle;

import com.example.brere.ParkingLotOrganizationSystemAndroidExperiments.client.dataFieldStuff.DataField;
import com.example.brere.ParkingLotOrganizationSystemAndroidExperiments.client.dataFieldStuff.DataFieldList;
import com.example.brere.ParkingLotOrganizationSystemAndroidExperiments.client.dataFieldStuff.TableCollection;
import com.example.brere.ParkingLotOrganizationSystemAndroidExperiments.client.systems.util.MessageDisplayer;
import com.example.brere.ParkingLotOrganizationSystemAndroidExperiments.client.systems.util.insertionSystem.MultiServerCallMethod;
import com.example.brere.ParkingLotOrganizationSystemAndroidExperiments.client.systems.util.insertionSystem.UserInputSystem;
import com.example.brere.ParkingLotOrganizationSystemAndroidExperiments.client.systems.util.insertionSystem.InsertionView;
import com.example.brere.ParkingLotOrganizationSystemAndroidExperiments.pretend.ControlSystemsActivity;
import com.example.brere.ParkingLotOrganizationSystemAndroidExperiments.shared.sentObjects.clientToServer.dataBaseFunctions.SearchDataBaseFunction;

import java.util.ArrayList;

/**
 * this is a controlSystem made for the user to log in. They can log into their profile using this system
 */
public class LoginSystem extends UserInputSystem{
	/**
	 * creates a bundle that you can launch a new LoginSystem using
	 * @return a bundle that you can launch a new LoginSystem using
	 */
	public static Bundle createBundleForSystem() {
			DataFieldList loginstuff=new DataFieldList(TableCollection.USERTABLE.table);
			loginstuff.add(new DataField(TableCollection.USERTABLE.ID,null));
			loginstuff.add(new DataField(TableCollection.USERTABLE.PASSWORD,null));
			ArrayList<DataFieldList> arrayList=new ArrayList<>();
			arrayList.add(loginstuff);

			Bundle b = UserInputSystem.createBundleForSystem("login",arrayList);
			ControlSystemsActivity.CreateControlSystem instantiate = LoginSystem::new;
			b.putSerializable(ControlSystemsActivity.CONTROLSYSTEMGENERATEORID, instantiate);
			return b;
		}
	@Override
	public void setView(InsertionView view) {
		super.setView(view);
		addDataFieldListConsumerButton("login",
				new MultiServerCallMethod(this::onsuccessfulSearch, null, SearchDataBaseFunction::new, super::getServ));
	}

	/**
	 * when the server searches the database (for the user id/password combination)
	 * , and it is completed successfully, this method will be called
	 * @param dataFieldLists the result of the search for the user id/password combination
	 */
	private void onsuccessfulSearch(ArrayList<DataFieldList> dataFieldLists) {
		if(dataFieldLists.isEmpty()){
			MessageDisplayer.displayErrorMessage(this.getContext(),"unknown username/password combination, please try again");
		}
		else{
			DataFieldList userData=dataFieldLists.get(0);
			UserInputSystem.setUserData(userData);
			selectWhichUserTypeToOpen(userData);
		}
	}

	/**
	 * This method decides which type of system to open, using the inputed userData. like it decides
	 * to open either an admin system, or a guard system
	 * @param userData the users data which it deicides what type of system to open based on.
	 */
	private void selectWhichUserTypeToOpen(DataFieldList userData) {
		Bundle systemBundle=null;
		if(userData.get(TableCollection.USERTABLE.SUPERSSN).getData()==null){
			systemBundle=UserSystemsBundleCreationMethods.createAdminSystem();
		}
		else{
			systemBundle=UserSystemsBundleCreationMethods.createGuardSystem();
		}
		startTheUserSystem(systemBundle);

	}

	/**
	 * starts either admin or Guard control system using the inputted bundle
	 * @param systemBundle the bundle that is used to start the system
	 */
	private void startTheUserSystem(Bundle systemBundle) {
		Intent intent = new Intent(this.getActivity(), ControlSystemsActivity.class);
		intent.putExtras(systemBundle);
		startActivity(intent);
		super.getInsertionView().getAdapter().setTextInvisable();
	}


}
