package com.example.brere.ParkingLotOrganizationSystemAndroidExperiments.client.systems.util.insertionSystem.individualColumnStuff;

import com.example.brere.ParkingLotOrganizationSystemAndroidExperiments.client.dataFieldStuff.DataField;

import java.util.ArrayList;
import java.util.function.*;

/**
 * The Class DataFieldInputManager. which is a class that takes a supplier of strings, and when its method copyDataIntoDataFields() is
 * activated, it gets the string from the supplier, and puts in in all datafields it has
 */
public class DataFieldInputManager {
	/** The input, whichs output gets copied into the datafields. */
	private Supplier<String> input;
	/** The data fields to copy into, so the string from the supplier gets copied into these. */
	private ArrayList<DataField> dataFieldsToCopyInto;

	/**
	 * Instantiates a new data field input manager.
	 *
	 * @param input the input supplier of strings whichs data will be copied into all data fields
	 * @param first the first datafield which will accept the string from input.
	 */
	public DataFieldInputManager(Supplier<String> input, DataField first) {
		super();

		this.input = input;
		this.dataFieldsToCopyInto=new ArrayList<DataField>();
		add(first);
	}
	/**
	 * it takes the string value from the Supplier<String> and copies the string value into
	 * each dataField it has
	 *
	 * @throws IllegalArgumentException when the string is not a valid user input, ie
	 * for an integer only expression, they inputed a letter
	 */
	public final void copyDataIntoDataFields() throws IllegalArgumentException {
		String userInput=getUserInput();
		for(DataField field: dataFieldsToCopyInto) {
			field.getColumn().checkUserInput(userInput);
			field.setData(userInput);
		}
	}
	/**
	 * Temporarily copy data into data fields, which will the string in the supplier into each datafield, without checking the
	 * input
	 */
	public final void temporarilyCopyDataIntoDataFields(){
		String userInput=getUserInput();
		dataFieldsToCopyInto.forEach((DataField d)->{ d.setData(userInput);});
	}
	/**
	 * gets the string from the supplier, modifies it to fit the standards of the system,
	 * like by triming the input, and if it is an empty string, setting the input to null;
	 *
	 * @return the input from the supplier, modidfied to the standards of the system.
	 */
	private String getUserInput(){
		if(input==null){
			System.out.println(dataFieldsToCopyInto);
		}
		String userInput=input.get();
		if(userInput!=null) {
			userInput=userInput.trim();
			if(userInput.equals("")) {
				userInput=null;
			}
		}
		return userInput;
	}
	/**
	 * Adds the datafield, so it will store the ouptut when copydataintoDataFields is called
	 *
	 * @param d the d datafield which will now store the output when copydataintoDataFields is called
	 */
	public void add(DataField d) {
		dataFieldsToCopyInto.add(d);
	}
	/**
	 * Gets the first data field.
	 *
	 * @return the first data field
	 */
	public DataField getFirstDataField() {
		return dataFieldsToCopyInto.get(0);
	}
	/**
	 * Gets the supplier of strings, whos value will be copied into the datafields
	 *
	 * @return the input
	 */
	public Supplier<String> getInput() {
		return input;
	}
	/**
	 * Gets the supplier of strings, whos value will be copied into the datafields
	 * @return the input
	 */
	public void setInput(Supplier<String> input) {
		this.input = input;
	}

	@Override
	public String toString() {
		return dataFieldsToCopyInto.toString();
	}
}
