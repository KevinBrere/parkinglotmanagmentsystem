package com.example.brere.ParkingLotOrganizationSystemAndroidExperiments;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.example.brere.ParkingLotOrganizationSystemAndroidExperiments.client.communicationToServer.ClientsCommunicationSystem;
import com.example.brere.ParkingLotOrganizationSystemAndroidExperiments.client.dataFieldStuff.DataFieldList;
import com.example.brere.ParkingLotOrganizationSystemAndroidExperiments.client.dataFieldStuff.TableCollection;
import com.example.brere.ParkingLotOrganizationSystemAndroidExperiments.client.systems.ControlSystem;
import com.example.brere.ParkingLotOrganizationSystemAndroidExperiments.client.systems.specific.LoginSystem;
import com.example.brere.ParkingLotOrganizationSystemAndroidExperiments.client.systems.util.MessageDisplayer;
import com.example.brere.ParkingLotOrganizationSystemAndroidExperiments.client.systems.util.controlSystemHandeler.ControlSystemHandeler;
import com.example.brere.ParkingLotOrganizationSystemAndroidExperiments.client.systems.util.insertionSystem.InsertionSystem;
import com.example.brere.ParkingLotOrganizationSystemAndroidExperiments.client.systems.util.insertionSystem.UserInputSystem;
import com.example.brere.ParkingLotOrganizationSystemAndroidExperiments.client.systems.util.listSystem.ListSystem;
import com.example.brere.ParkingLotOrganizationSystemAndroidExperiments.pretend.ControlSystemsActivity;
import com.example.brere.ParkingLotOrganizationSystemAndroidExperiments.pretend.ListenerMethod;
import com.example.brere.ParkingLotOrganizationSystemAndroidExperiments.pretend.OriginalView;

import java.util.ArrayList;

public class MainActivity extends ControlSystemsActivity {

    @Override
    public void onCreate(Bundle savedInstanceState) {

//		ControlSystem.setServ(
//				(new ClientsCommunicationSystem(this.getApplicationContext())).getCommunicator());


		this.getIntent().putExtras(LoginSystem.createBundleForSystem());

		super.onCreate(savedInstanceState);
		getSupportActionBar().setDisplayHomeAsUpEnabled(false);

	}
//    @Override
//    public void onResume() {
//		super.onResume();
//
//		ListenerMethod meow = ListenerMethod::goMeow;
//		ListenerMethod moo = ListenerMethod::goMoo;
//		ListenerMethod woof = ListenerMethod::goWoof;
//		ListenerMethod oink = ListenerMethod::goOink;
//
//		ArrayList<Bundle> bundles = new ArrayList<>();
//		bundles.add(OriginalView.createBundleForSystem("cat", meow));
//		bundles.add(OriginalView.createBundleForSystem("dog", woof));
//		Bundle home = ControlSystemHandeler.createBundleForSystem("home", bundles);
//
//
//		ArrayList<Bundle> bundles2 = new ArrayList<>();
//		bundles2.add(OriginalView.createBundleForSystem("cow", moo));
//		bundles2.add(OriginalView.createBundleForSystem("pig", oink));
//		Bundle farm = ControlSystemHandeler.createBundleForSystem("farm", bundles2);
//
//
//		ArrayList<Bundle> grandMenu = new ArrayList<>();
//
//		grandMenu.add(home);
//		grandMenu.add(farm);
//
//		DataFieldList userlist = TableCollection.USERTABLE.table.getAsDataFieldList();
//		userlist.get(0).setData("working");
//
//		grandMenu.add(InsertionSystem.createBundleForSystem(userlist));
//		grandMenu.add(ListSystem.createBundleForSystem(TableCollection.USERTABLE.table));
//
//		Intent intent = new Intent(this, ControlSystemsActivity.class);
//		intent.putExtras(ControlSystemHandeler.createBundleForSystem("main menu", grandMenu));
//		startActivity(intent);
//	}

}
