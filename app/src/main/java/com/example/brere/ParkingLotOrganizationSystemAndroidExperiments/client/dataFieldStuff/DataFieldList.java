package com.example.brere.ParkingLotOrganizationSystemAndroidExperiments.client.dataFieldStuff;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;


/**
 * The Class DataFieldList, which is basically a custom vector for holding
 * DataFields. A dataField is basically an object which holds data for a peice
 * of data in the database, and the column that the data belongs in. The
 * difference between this and an ordinary vector, is that it is custom built to
 * hold data for only a single row of the database. To maintain this, it does
 * error checking it checks if there are duplicate labels in the vector, and
 * whether all of the labels belong to the same table. it also checks if the
 * vector is to big.
 *
 * there are a LOT of uses for this class. like you can search using Columns for
 * one. it also automatically checks if the user entered a wrong number of
 * labels for insertion. It also allowed for automatic searching and deleting of
 * stuff in the database. we only had 1 method for each search, delete, modify,
 * insertion, and because of the way this vector worked, it was impossible for
 * anyone to mess up the insertion process.
 */
public class DataFieldList implements Serializable, Iterable<DataField> {
	/** The table that the datafield list represents. like is it a user, or a car or... */
	private Table table;
	/** The list which holds the dataFields */
	private ArrayList<DataField> list;
	/**
	 * Instantiates a new data field list, which is empty (table is for checking)
	 * @param table the table which the columns entered will be checked against
	 */
	public DataFieldList(Table table) {
		this.table = table;
		list = new ArrayList<DataField>();
	}
	/**
	 * Gets the table which all of the columns belong to
	 * @return the table
	 */
	public Table getTable() {
		return table;
	}
	/**
	 * Instantiates a new data field list, copying the columns from the previous list.
	 * @param list the list which will be copied.
	 */
	public DataFieldList(DataFieldList list) {
		this(list.getTable());
		for (int i = 0; i < list.size(); i++) {
			String feildIn = null;
			if (list.get(i).getData() != null) {
				feildIn = new String(list.get(i).getData());
			}
			this.add(new DataField(list.get(i).getColumn().copy(list.get(i).getColumn().getColumnNumber()), feildIn));
		}
	}

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 8160451334623592865L;

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "DataFieldList [list=" + list + "]";
	}


	/**
	 * adds the inputed DataField to the list.
	 *
			 * @param fieldAdded
	 *            the DataField which will be added to the list
	 * @throws IllegalArgumentException
	 *             when the inputed DataField has the same label as another member
	 *             of the list, or the inputed DataField belongs to a different
	 *             table
	 */
	public void add(DataField fieldAdded) throws IllegalArgumentException {
		checkNewInput(fieldAdded);
		list.add(fieldAdded);
	}

	/**
	 * gets the field corresponding to the index.
	 *
	 * @param i the index of the field being retrieved
	 * @return the DataField with the inputed index
	 */
	public DataField get(int i) {
		return list.get(i);
	}

	/**
	 * Deletes the dataField at the index from the list.
	 *
	 * @param i     the index of the removed datafield
	 */
	public void remove(int i) {
		list.remove(i);
	}

	/**
	 * deletes a DataField in the list which has the inputed label.
	 *
	 * @param column
	 *            the column of the DataField which will be deleted
	 */
	public void remove(Column column) {
		if (column == null) {
			throw new IllegalArgumentException("error, you tried to delete null ya bum");
		}
		for (int i = 0; i < list.size(); i++) {
			if (column.equals(list.get(i).getColumn())) {
				remove(i);
				return;
			}
		}
	}

	/**
	 * gets the member based on the column inputed. ie if you input user.id, it
	 * would get the the DataField of the id. Warning: if it cant find a DataField
	 * with that label, it will return null;
	 *
	 * @param label  the column of the DataField you want to get
	 * @return the DataField which has the inputed label
	 */
	public DataField get(Column label) {
		if (label == null) {
			throw new IllegalArgumentException("error, you tried to get null ya poo");
		}
		for (int i = 0; i < list.size(); i++) {
			if (label.equals(list.get(i).getColumn())) {
				return list.get(i);
			}
		}
		return null;
	}

	/**
	 * checks if the list is empty.
	 *
	 * @return if the list is empty
	 */
	public boolean isEmpty() {
		return list.isEmpty();
	}

	public boolean isFull() {
		if (list.isEmpty()) {
			return false;
		}
		return list.size() == getTable().getColumns().size();

	}


	/**
	 * checks if an added input can be added to the list.
	 *
	 * @param toBeChecked
	 *            the new input which will be checked if it is valid
	 */
	private void checkNewInput(DataField toBeChecked) {
		if (toBeChecked == null) {
			throw new IllegalArgumentException("error, you tried to enter a null dataField");
		}
		checkNewInputsTable(toBeChecked);
		checkIfInputIsAlreadyInList(toBeChecked);
	}

	/**
	 * checks if the inputed parameter has the same table as table object.
	 * REQUIRES there to be one input in the list already
	 *
	 * @param toBeChecked the DataField which will be checked
	 */
	private void checkNewInputsTable(DataField toBeChecked) {
		for (int i = 0; i < getTable().getColumns().size(); i++) {
			if (getTable().getColumns().get(i).equals(toBeChecked.getColumn())) {
				return;
			}
		}
		throw new IllegalArgumentException(
				"error, tried to add a peice of data which belonged in a diffrent table");
	}
	/**
	 * gets the size of the list
	 * @return the size of the list
	 */
	public int size() {
		return list.size();
	}

	/**
	 * checks if any DataField in the list has the same label as the inputed
	 * Datafield.
	 *
	 * @param toBeChecked 	the DataField which will be checked
	 */
	private void checkIfInputIsAlreadyInList(DataField toBeChecked) {
		for (int i = 0; i < list.size(); i++)
			if (toBeChecked.getColumn().equals(list.get(i).getColumn())) {
				System.out.println("duplicates: inserting:" + toBeChecked.getColumn().getColumnName() + ": " + toBeChecked.getData());
				System.out.println("duplicates: in list:" + list.get(i).getColumn().getColumnName() + ": " + list.get(i).getData());

				throw new IllegalArgumentException("error, tried to add a duplicate peice of data");
			}
	}

	/**
	 * checks if the list has one DataField for each column of its corresponding
	 * table in the database.
	 *
	 * @return whether the list has one DataField for each column of its
	 *         corresponding table in the database
	 */
	public boolean isReadyForInsertion() {
		if (isEmpty()) {
			return false;
		}

		ArrayList<Column> insertList = getTable().getColumnsForInsertion();

		return checkIfElementsMatchInsertion(insertList);

	}

	/**
	 * checks if all of the nessisary elements are there for the insertion process.
	 *
	 * @param insertList the insert list
	 * @return true, if successful
	 */
	private boolean checkIfElementsMatchInsertion(ArrayList<Column> insertList) {

		int i, j;
		for (i = 0; i < insertList.size(); i++) {

			for (j = 0; j < list.size() - 1; j++) {
				if (insertList.get(i).equals(list.get(j).getColumn())) {
					break;
				}
			}
			if (!insertList.get(i).equals(list.get(j).getColumn())) {
				System.out.println("thinks that the inputed list does not include" + insertList.get(i).getColumnName());
				System.out.println(list);
				return false;
			}
		}
		return true;
	}
	/**
	 * Equals.
	 *
	 * @param rightList the right list
	 * @return true, if successful
	 */
	public boolean equals(DataFieldList rightList) {
		if (size() != rightList.size()) {
			return false;
		}
		for (int i = 0; i < size(); i++) {
			if (!this.list.get(i).equals(rightList.get(i))) {
				return false;
			}
		}
		return true;
	}
	/**
	 * Gets a new dataFieldList which has all the keys that are in the dataFieldList
	 *
	 * @return the keys
	 */
	public DataFieldList getKeys() {
		DataFieldList newlist = new DataFieldList(this.getTable());
		for (int i = 0; i < list.size(); i++) {
			if (list.get(i).getColumn().isKey()) {
				newlist.add(new DataField(list.get(i).getColumn(), list.get(i).getData()));
			}
		}
		return newlist;
	}
	/**
	 * Checks if the List has each key that is availible in the table
	 * @return true, if successful
	 */
	public boolean hasAllKeys() {
		int i, j;
		ArrayList<Column> keys = this.getTable().getKeys();
		for (i = 0; i < keys.size(); i++) {

			for (j = 0; j < list.size() - 1; j++) {
				if (keys.get(i).equals(list.get(j).getColumn())) {
					break;
				}
			}
			if (!keys.get(i).equals(list.get(j).getColumn())) {
				System.out.println("thinks that the inputed list does not include" + keys.get(i));
				System.out.println(list);
				return false;
			}
		}

		return true;
	}
	/**
	 * Gets the all refrences that are in the List
	 *
	 * @return the all refrences that are in the list.
	 */
	public DataFieldList getAllRefrences() {
		DataFieldList refrences = new DataFieldList(this.getTable());
		for (int i = 0; i < this.size(); i++) {
			if (this.list.get(i).getColumn() instanceof Reference) {
				refrences.add(this.list.get(i));
			}
		}

		return refrences;
	}

	/**
	 * gets the key of the table refrenced. Assumes that the other table has only 1
	 * key.
	 *
	 * @param refrence
	 *            the forign key to the other table
	 * @return the key to the other table.
	 */
	//TODO make support multiple keys
	public DataFieldList getPrimaryKeyOfRefrencedTable(Reference refrence) {
		DataField dataInThis = this.get(refrence);
		if (dataInThis == null) {
			return null;
		}

		DataFieldList list = new DataFieldList(refrence.getRefrenceTable());
		list.add(new DataField(refrence.getRefrenceColumn(), dataInThis.getData()));
		return list;

	}
	/**
	 * returns a new DataFieldList which holds each dataField, and does not return the hidden items
	 *
	 * @return the list without hidden items
	 */
	public DataFieldList getListWithoutHiddenItems() {
		DataFieldList withoutHidden = new DataFieldList(this.getTable());
		for (DataField d : this.list) {
			if (!d.getColumn().isHiddenFromSearches()) {
				withoutHidden.add(d);
			}
		}
		return withoutHidden;
	}
	/* (non-Javadoc)
	 * @see java.lang.Iterable#iterator()
	 */
	@Override
	public Iterator<DataField> iterator() {
		return list.iterator();
	}
	/**
	 * copies all of the data which is the same from this dataFieldList into another dataFieldList.
	 * like if any dataField has the same column, it copies the data from this DatafieldList into the other
	 * DataFieldList.
	 *
	 * @param d the dataFieldList which will be copied into
	 */
	public void copyDataIntoAnotherDataFieldList(DataFieldList d) {
		for (DataField inThis : this) {
			DataField data = d.get(inThis.getColumn());
			if (data != null) {
				data.setData(inThis.getData());
			}
		}
	}
}