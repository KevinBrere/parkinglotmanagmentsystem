package com.example.brere.ParkingLotOrganizationSystemAndroidExperiments.pretend;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.support.v7.widget.RecyclerView.ViewHolder;

import com.example.brere.ParkingLotOrganizationSystemAndroidExperiments.R;

import java.util.ArrayList;

public class KevinsFirstRecyclerViewAdapter extends RecyclerView.Adapter<ViewHolder>  {
    private ArrayList<String> strings;

    public KevinsFirstRecyclerViewAdapter(ArrayList<String> strings) {
        this.strings = strings;
    }
    private static class KevinViewHolder extends ViewHolder{

        TextView view;
        public KevinViewHolder(@NonNull View itemView) {
            super(itemView);
            this.view = itemView.findViewById(R.id.textView);
        }
        public void setText(String s){
            view.setText(s);
        }
    }
    public ViewHolder onCreateViewHolder(ViewGroup parent,
                                                   int viewType) {

        // create a new view
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.kevin_list_entity, parent, false);

        ViewHolder vh = new KevinViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        KevinViewHolder hold=(KevinViewHolder) holder;
        hold.setText(strings.get(position));
    }

    @Override
    public int getItemCount() {
        return strings.size();
    }

    public void onItemClick(KevinsFirstRecyclerViewAdapter parent, View view, int position, long id) {
        this.addString(strings.get(0));
    }
    public void addString(String s){
        strings.add(s);
        this.notifyDataSetChanged();
    }

}
