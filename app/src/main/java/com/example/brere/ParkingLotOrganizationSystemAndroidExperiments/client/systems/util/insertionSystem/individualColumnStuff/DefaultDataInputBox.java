package com.example.brere.ParkingLotOrganizationSystemAndroidExperiments.client.systems.util.insertionSystem.individualColumnStuff;


import android.support.annotation.NonNull;
import android.text.InputFilter;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;

import com.example.brere.ParkingLotOrganizationSystemAndroidExperiments.R;
import com.example.brere.ParkingLotOrganizationSystemAndroidExperiments.client.dataFieldStuff.DataField;
import com.example.brere.ParkingLotOrganizationSystemAndroidExperiments.client.dataFieldStuff.DataFieldList;

/**
 * this is the default Data Input box. Without programmer setting the DataInputBox constructor to
 * something else, all Data will be inputted using this class by default. The user simply fills out
 * a simple EditText to get their input.
 */
public class DefaultDataInputBox extends DataInputBox {
/** serial id */
	private static final long serialVersionUID = 1L;
	/**the editText that the user inputs */
	private EditText field;
	/** the textview that displays the label that tells the user what they are inputing */
	private TextView label;

	/**
	 * creates a DataInputBoxConstructor which will create a DefualtDataInputBox, and will set the
	 * Input Type of the EditText to the same as the type
	 * @param type the type of EditText that you want to use
	 * @return a DataInputBoxConstructor which constructs a DefualtDataInputBox with the Edittext
	 * Field set the the type as the inputed type
	 */
	public static DataInputBox.DataInputBoxConstructor constructDataFieldInputBoxWithCustomDataField( int type) {
		return new DataInputBox.DataInputBoxConstructor() {
			private static final long serialVersionUID = 22420L;
			@Override
			public DataInputBox constructDataInputBox(DataField d, DataFieldList listFieldBelongsTo, @NonNull ViewGroup parent, int viewType) {
				DefaultDataInputBox box=(DefaultDataInputBox) DefaultDataInputBox.constructDataFieldInputBox().constructDataInputBox(d,listFieldBelongsTo,parent,viewType);
				box.getField().setInputType(type);
				return box;
			}
		};
	}

	/**
	 * creates a DataInputBoxConstructor which will create a DefualtDataInputBox, with the defualt
	 * input type
	 * @return a DataInputBoxConstructor which constructs a normal DefaultDataInputBox
	 */
	public static DataInputBoxConstructor constructDataFieldInputBox() {
		return new DataInputBoxConstructor() {
			private static final long serialVersionUID = 22400L;
			@Override
			public DataInputBox constructDataInputBox(DataField d, DataFieldList listFieldBelongsTo, @NonNull ViewGroup parent, int viewType) {
				View v = LayoutInflater.from(parent.getContext())
						.inflate(R.layout.default_data_input_box_view, parent, false);
				return new DefaultDataInputBox(d,listFieldBelongsTo,v);
			}
		};
	}

	/**
	 * constructs a DefualtDataInputBox
	 * @param data the data which is being displayed by the input box
	 * @param list the list that the data belongs to
	 * @param v the view that the user sees
	 */
	public DefaultDataInputBox(DataField data,DataFieldList list, View v) {
		super(v,data,list );
		this.label =v.findViewById(R.id.DefaultDataInputBoxLabelForInput);
		label.setText(data.getColumn().getColumnName());


		this.field = v.findViewById(R.id.DefaultDataInputBoxInputArea);
		field.setFilters(new InputFilter[] {new InputFilter.LengthFilter(data.getColumn().getMaxSize())});
	}
	@Override
	public void setEditable(boolean edit) {
		field.setEnabled(edit);
	}
	@Override
	protected void setUsersInputText(String s) {
		field.setText(s);
	}
	@Override
	public void setTextInvisable() {
		field.setText("");
	}
	@Override
	public String get() {
		return field.getText().toString();
	}
	/**gets the editText that the user uses to input*/
	public EditText getField() {
		return field;
	}
}
