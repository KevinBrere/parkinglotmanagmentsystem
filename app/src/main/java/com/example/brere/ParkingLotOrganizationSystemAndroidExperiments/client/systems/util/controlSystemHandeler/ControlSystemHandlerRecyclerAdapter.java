package com.example.brere.ParkingLotOrganizationSystemAndroidExperiments.client.systems.util.controlSystemHandeler;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.brere.ParkingLotOrganizationSystemAndroidExperiments.R;
import com.example.brere.ParkingLotOrganizationSystemAndroidExperiments.client.systems.ControlSystem;

import java.util.ArrayList;
import java.util.function.Consumer;

/**
 * this is the RecyclerAdapter that the ControlSystemHandeler uses to make the menu between control system
 * this recyclerAdapter will accept an arrayList of bundles, one for each system that needs to be created
 * and a consumer<SingleSystemViewHolder> which is an actionListener for each view created in the
 * recyclerview
 */
public class ControlSystemHandlerRecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>  {



	/** an arrayList of bundles which hold data for launching each system */
	private ArrayList<Bundle> bundles;
	/** a consumer<singleSystemViewHolder> which will be activated each time something in the list
	 * is clicked */
	private Consumer<SingleSystemViewHolder> listener;

	/**
	 * instantiates a new ControlSystemRecyclierAdapter, instantiating each field
	 * @param bundles the bundles for instantating the other ControlSystems
	 * @param listener a consumer<Bundle> which will launch the coresponding activity using that bundle
	 */
	public ControlSystemHandlerRecyclerAdapter(@NonNull ArrayList<Bundle> bundles, @NonNull Consumer<SingleSystemViewHolder> listener) {
		this.bundles = bundles;
		this.listener=listener;
	}

	/**
	 * the singleSystemViewHolder which is basically a ViewHolder for a single slide in the menu
	 * this class has a bundle for the ControlSystem it represents.
	 */
	public static class SingleSystemViewHolder extends RecyclerView.ViewHolder {
		/** the bundle for the controlSystem it represents*/
		private   Bundle bun;
		/** a textView  which is the textview the user sees */
		public TextView view;
		/** instantiates a new SingelSystemViewHolder
		 * @param  itemView the view which the user interacts with*/
		public SingleSystemViewHolder(@NonNull View itemView) {
			super(itemView);
			this.view = itemView.findViewById(R.id.textView);
		}
		/** sets the text that the user sees
		 * @param s the new text which the user sees */
		public void setText(String s){
			view.setText(s);
		}
		/** sets the bundle of the system it represents */
		private void setBundle(Bundle b){
			this.bun=b;
		}
		/** gets the bundle of the system it represents*/
		public Bundle getBundle(){
			return bun;
		}
	}
	@Override
	public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent,
													  int viewType) {

		// create a new view
		View v = LayoutInflater.from(parent.getContext())
				.inflate(R.layout.kevin_list_entity, parent, false);
		SingleSystemViewHolder vh = new SingleSystemViewHolder(v);
		v.setOnClickListener((View v2)-> listener.accept(vh));
		return vh;
	}

	@Override
	public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {

		SingleSystemViewHolder s=(SingleSystemViewHolder)holder;
		s.setBundle(bundles.get(position));

		s.setText(bundles.get(position).getString(ControlSystem.CONTROLSYSTEMTITLE));
	}

	@Override
	public int getItemCount() {
		return bundles.size();
	}
}
