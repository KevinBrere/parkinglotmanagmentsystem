package com.example.brere.ParkingLotOrganizationSystemAndroidExperiments.shared.sentObjects.ServerToClient;

import java.io.Serializable;
/**
 * this class is basically some error data from the server to the client. This
 * includes a basic error message and a Flag showing what type of error occured.
 * like if there was an error while inserting data into the database do to
 * refrential integraty, the string would be an error message, and the ErrorType
 * would be REFRENCEERROR
 */
public class ErrorData  implements Serializable{
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -3624034459012679127L;
	/**
	 * The Enum ErrorType, which is a flag showing what type of error occured.
	 */
	public static enum ErrorType{
		/** The refrenceerror, so referential integraty constraint failed */
		REFRENCEERROR,
		/** The unkown error */
		UNKOWN,
		/** some Illegal Argument happened */
		ILLEGALARGUMENT,
		/** It was successful, but had to be sent as an error to a errorMessageHandeler */
		DEFAULTSUCESSMESSAGE,
		/** error with server comunication. */
		SERVERCOMMUNICATIONERROR,
		/** database entity integraty constraint failed. */
		ENTITYINTEGRATY;
	}
	/** The message of the error, which will be displayed to the user. */
	private String message;
	/** The error type. */
	private ErrorType errorType;

	/**
	 * Instantiates a new error data
	 * @param message the message which will be displayed to the user
	 * @param errorType the type of error that occured
	 */
	public ErrorData(String message, ErrorType errorType) {
		super();
		this.message = message;
		this.errorType = errorType;
	}
	/*
	 * (non-Javadoc)
	 *
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "ErrorData message: " + message + ", errorType: " + errorType + "]";
	}
	/**
	 * Gets the message to the user
	 * @return the message to the user
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * Sets the message to the user
	 * @param message  the new message
	 */
	public void setMessage(String message) {
		this.message = message;
	}
	/**
	 * Gets the error type.
	 * @return the error type
	 */
	public ErrorType getErrorType() {
		return errorType;
	}
	/**
	 * Sets the error type.
	 * @param errorType the new error type
	 */
	public void setErrorType(ErrorType errorType) {
		this.errorType = errorType;
	}
}
