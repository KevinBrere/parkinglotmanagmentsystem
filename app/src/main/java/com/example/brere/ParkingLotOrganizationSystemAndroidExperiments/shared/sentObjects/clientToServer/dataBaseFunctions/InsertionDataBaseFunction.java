package com.example.brere.ParkingLotOrganizationSystemAndroidExperiments.shared.sentObjects.clientToServer.dataBaseFunctions;
import com.example.brere.ParkingLotOrganizationSystemAndroidExperiments.client.dataFieldStuff.DataFieldList;
import com.example.brere.ParkingLotOrganizationSystemAndroidExperiments.shared.sentObjects.ServerToClient.ErrorData;


import java.sql.*;
import java.util.ArrayList;


/**
 * this is a class for inserting into the database. it basically takes a
 * datafieldList, and turns it into and insertion sql statement.
 *
 *
 * it would look something like this Insert
 * into+tablename+values(column1=data1,column2=data2...)
 *
 * @author brere
 *
 */
public class InsertionDataBaseFunction extends DataBaseFunction{
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 6120614945779742681L;
	/**
	 * Instantiates a new insertion data base function, which will insert the dataFieldList into the database
	 *
	 * @param table the dataFieldList which will be inserted into the database
	 */
	public InsertionDataBaseFunction(DataFieldList table) {
		super(table,"insertion");
	}
	/* (non-Javadoc)
	 * @see shared.serverClientCommunicationFramework.serverOrdersFromClientToServer.dataBaseFunctions.DataBaseFunction#check(client.dataFieldStuff.DataFieldList)
	 */
	@Override
	protected void check(DataFieldList data) {
		if(!data.isReadyForInsertion())		
			throw new IllegalArgumentException("The data is not ready for insertion");
	}

	/* (non-Javadoc)
	 * @see shared.serverClientCommunicationFramework.serverOrdersFromClientToServer.dataBaseFunctions.DataBaseFunction#execute(java.sql.Connection)
	 */
	@Override
	public ArrayList<DataFieldList> execute(Connection jdbcConnection) throws SQLException {
		DataFieldList table = super.getData();
		PreparedStatement statement;
		
		String tableName = table.getTable().getDataBaseTitle();
		String insert = "INSERT INTO "+ tableName +"(";
		String values = " values (";
		int i;
		for (i = 0; i < table.size() - 1; i++)
		{
	//		System.out.println(table.get(i).getColumn().getDataBaseColumnName() + " = " + super.getData().get(i).getData());
			insert+= table.get(i).getColumn().getDataBaseColumnName() + ", ";
			values+= "?,";
		}
		insert+=table.get(i).getColumn().getDataBaseColumnName()+")";
		values+="?)";
		
		insert += values;		
		
			statement = jdbcConnection.prepareStatement(insert);
			for(int j = 0; j < table.size(); j++)
			{
				String data = table.get(j).getData();
				statement.setString(j+1, data);
			}
			
			statement.executeUpdate();	
			
		ArrayList<DataFieldList> list=new ArrayList<DataFieldList>();
		list.add(super.getData());
		return list;
	}

	/**
	 * Checks the database if there is any other data already in the database with a matching key
	 * if there is, it returns ErrorData with the correct message. if there is not, it returns null.
	 * @param jdbcConnection the jdbc connection it will use to check the database
	 * @param data the data that will be checked if it shares a key with another peice of data in the database
	 * @return null if no error was found, and an errorData with the coresponding message if an error was found
	 * @throws SQLException the SQL exception if there was an sql exception while checking the database.
	 */
	private ErrorData checkKeys(Connection jdbcConnection, DataFieldList data) throws SQLException{
		DataFieldList keys=data.getKeys();
		ArrayList<String> errors=new ArrayList<String>();
		for(int i=0;i<keys.size();i++) {
			if(keys.get(i).getData()==null) {
				errors.add("error, you did not input a value for the key "+keys.get(i).getColumn());
			}
		}
		if(errors.isEmpty()) {
			SearchDataBaseFunction func=new SearchDataBaseFunction(keys);
			ArrayList<DataFieldList> searchResult=func.execute(jdbcConnection);
			
			if(!searchResult.isEmpty()) {
				errors.add(createUniqunessErrorMessage(keys));				
			}
		}
		if(!errors.isEmpty()) {
			return new ErrorData(super.generateErrorMessage(errors),ErrorData.ErrorType.ENTITYINTEGRATY);
		}
		return null;
	}
	/**
	 * Creates an error message telling the user that there is already another entity in the database with the same
	 * key
	 * @param keys the keys of the non unique data
	 * @return an error message telling the user that another entity exists with those exact keys
	 */
	private String createUniqunessErrorMessage(DataFieldList keys) {
		
		String uniqnessErrorMessage="error, an entry with the value(s)  ";
		for(int i=0;i<keys.size()-1;i++){
			uniqnessErrorMessage+=keys.get(i).getColumn().getColumnName()+": "+keys.get(i).getData()+", ";
		}
		uniqnessErrorMessage+=keys.get(keys.size()-1).getColumn().getColumnName()+": "+keys.get(keys.size()-1).getData();
		
		uniqnessErrorMessage+=" already exists, please change those value(s), and try again after";

		return uniqnessErrorMessage;
	}
	/* (non-Javadoc)
	 * @see shared.serverClientCommunicationFramework.serverOrdersFromClientToServer.dataBaseFunctions.DataBaseFunction#giveErrorMessage(java.lang.Exception, java.sql.Connection)
	 */
	@Override
	protected ErrorData giveErrorMessage(Exception e,Connection jdbcConnection) throws SQLException {
		ErrorData s=this.checkKeys(jdbcConnection, super.getData());
		if(s==null) {s=super.checkRefrencesInThisList(jdbcConnection,super.getData());}
		if(s==null){
			s=super.doStandardErrorProcedure(e);}
		return s;
	}
}