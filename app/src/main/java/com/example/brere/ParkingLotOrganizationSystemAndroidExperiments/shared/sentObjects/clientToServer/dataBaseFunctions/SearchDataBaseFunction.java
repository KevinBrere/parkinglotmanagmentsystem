package com.example.brere.ParkingLotOrganizationSystemAndroidExperiments.shared.sentObjects.clientToServer.dataBaseFunctions;

import com.example.brere.ParkingLotOrganizationSystemAndroidExperiments.client.dataFieldStuff.DataField;
import com.example.brere.ParkingLotOrganizationSystemAndroidExperiments.client.dataFieldStuff.DataFieldList;
import com.example.brere.ParkingLotOrganizationSystemAndroidExperiments.client.dataFieldStuff.Table;
import com.example.brere.ParkingLotOrganizationSystemAndroidExperiments.shared.sentObjects.ServerToClient.ErrorData;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 * this is the database function which searches based on the inputs in a
 * datafieldList. for each input in the data fieldList, it outputs the
 * corresponding data into another datafieldlist.
 *
 * The way it creates the sql function is through using a giant string, where it
 * basically takes each input of the datafieldList, and outputs Select * from +
 * tablename+where+ column1=data1 +AND +column2=data2+... for whatever you input
 * into the list. because of this, it will work with any input.
 *
 * @author brere
 *
 */
public class SearchDataBaseFunction extends DataBaseFunction{
	/** The Constant serialVersionUID. */

	private static final long serialVersionUID = -5598726142570671411L;
	/**
	 * Instantiates a new search data base function, which will exectute a search operation based on
	 * the AND'ing of each datafield in data
	 * @param data the data which will be searched for
	 */
	public SearchDataBaseFunction(DataFieldList data) {
		super(data,"search");
	}
	/* (non-Javadoc)
	 * @see shared.serverClientCommunicationFramework.serverOrdersFromClientToServer.dataBaseFunctions.DataBaseFunction#check(client.dataFieldStuff.DataFieldList)
	 */
	@Override
	protected void check(DataFieldList data) {
		
	}
	/* (non-Javadoc)
	 * @see shared.serverClientCommunicationFramework.serverOrdersFromClientToServer.dataBaseFunctions.DataBaseFunction#execute(java.sql.Connection)
	 */
	@Override
	public ArrayList<DataFieldList> execute(Connection jdbcConnection) throws SQLException {
		
		String sql = "SELECT * FROM " + super.getData().getTable().getDataBaseTitle();
		
		if(!super.getData().isEmpty()) {
			sql+=" WHERE "+super.createOneSearchComparer(super.getData().get(0));
		}
		
		for (int i = 1; i < super.getData().size(); i++) {
			sql += " AND " + super.createOneSearchComparer(super.getData().get(i));
		}
		
		ArrayList<DataFieldList> tableMatch;
		
			System.out.println(sql);
			PreparedStatement statement = jdbcConnection.prepareStatement(sql);
			
			for(int i=0;i<super.getData().size();i++) {
				statement.setString(i+1, super.getData().get(i).getData());
			}
			
			
			ResultSet myClient = statement.executeQuery();

			tableMatch = new ArrayList<DataFieldList>();
			
			while (myClient.next()) {
			
				tableMatch.add(getResult(myClient, super.getData().getTable()));
			}

		
		return tableMatch;

		}

	/**
	 *  translates a single result set (tuple) into a datafeildList.
	 *
	 * @param myClient the results of a search,
	 * @param table the table that the resultSet belongs to
	 * @return the resultSet parsed into a DataFieldList
	 * @throws IllegalArgumentException when it tries to get a column number that does not exist.
	 * @throws SQLException the SQL exception
	 */
private DataFieldList getResult(ResultSet myClient, Table table) throws IllegalArgumentException, SQLException {
DataFieldList list = new DataFieldList(table);
for (int i = 1; i < table.getColumns().size()+1; i++) {			
	list.add(new DataField(table.getColumnBasedOnColumnNumber(i),(String) myClient.getObject(i)));
}
return list;
}
	/* (non-Javadoc)
	 * @see shared.serverClientCommunicationFramework.serverOrdersFromClientToServer.dataBaseFunctions.DataBaseFunction#giveErrorMessage(java.lang.Exception, java.sql.Connection)
	 */
@Override
protected ErrorData giveErrorMessage(Exception e, Connection jdbcConnection) {
	return super.doStandardErrorProcedure(e);
}

}
