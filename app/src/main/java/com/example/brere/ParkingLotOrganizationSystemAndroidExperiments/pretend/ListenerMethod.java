package com.example.brere.ParkingLotOrganizationSystemAndroidExperiments.pretend;


import java.io.Serializable;

/**
 * same as before.  ie this a method which accepts a veiw and a model. the methods use the veiw and model each time the
 * ListenerMethod activates.
 *
 * the only real diffrence is that I added some more animal noise methods.
 */
public interface ListenerMethod extends Serializable {
	public void execute(KevinsFirstRecyclerViewAdapter m);
	
	public static void goMoo(KevinsFirstRecyclerViewAdapter m) {
		m.addString("moooo");
	}
	public static void goOink(KevinsFirstRecyclerViewAdapter m) {
		m.addString("oink");
	}
	public static void goMeow(KevinsFirstRecyclerViewAdapter m) {
		m.addString("meow");
	}
	public static void goWoof(KevinsFirstRecyclerViewAdapter m) {
		m.addString("woof");
	}

}
