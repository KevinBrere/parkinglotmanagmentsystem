package  com.example.brere.ParkingLotOrganizationSystemAndroidExperiments.client.dataFieldStuff;

import java.io.Serializable;


/**
 * The Class DataField, which is an object that represents a single peice of
 * data that will go into the database. the object holds 2 things. The column
 * that will be entered into the database and the string which will go into that
 * column.
 *
 * ie say you wanted to search for a user in the database with the first name
 * kevin. you would make one of these, with the label firstname, and the data as
 * kevin.
 *
 */
public class DataField implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -7196015725269776698L;

	/** The column that the data will go into. */
	private Column column;

	/**  The data that will be inserted/searched in the table. */
	private String data;

	/**
	 * returns the column the data belogns to.
	 *
	 * @return the column the data belongs to
	 */
	public Column getColumn() {
		return column;
	}

	/**
	 * returns the data which may be stored in the database.
	 *
	 * @return the data which may be store din the database
	 */
	public String getData() {
		return data;
	}

	/**
	 * sets the data which will be stored in the database to the set value.
	 *
	 * @param newData
	 *            the new value of the data which will be stored in the database
	 */
	public void setData(String newData) {
		this.data = newData;
	}

	/**
	 * constructs the DataField object.
	 *
	 * @param column
	 *            the column that the data belongs to
	 * @param data
	 *            the data which will be stored/searched for in the database
	 */
	public DataField(Column column, String data) {
		
		if (data!=null&&data.length() > column.getMaxSize()) {
			throw new IllegalArgumentException("error, your " + column.getColumnName().toLowerCase()
					+ " input is too big, it must be less than " + column.getMaxSize());
		}
		this.column = column;
		this.data = data;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		return column.getColumnName() + ": " + data;
	}
	/**
	 * checks if the datafields are equal
	 * @param field the field which will be checked if it is equal to the other
	 * @return if the two datafields are equal
	 */
	public boolean equals(DataField field) {
		if(field==null) {
			return false;
		}
		boolean isBothStringsEqual=this.getData()==null&&field.getData()==null;
		
		if((this.getData()!=null&&field.getData()!=null)) {
			isBothStringsEqual=this.getData().equals(field.getData());
		}
		
		return this.getColumn().equals(field.getColumn())&&isBothStringsEqual;
	}
	
}
