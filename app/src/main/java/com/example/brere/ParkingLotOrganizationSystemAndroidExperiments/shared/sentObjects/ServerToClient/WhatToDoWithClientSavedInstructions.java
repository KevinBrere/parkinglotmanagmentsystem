package com.example.brere.ParkingLotOrganizationSystemAndroidExperiments.shared.sentObjects.ServerToClient;

import java.io.Serializable;
import java.util.ArrayList;
/**
 * The Enum WhatToDoWithClientSavedInstructions, which basically tells the client wheter it should
 * delete or keep the instructions it has saved for what to do when the server responds.
 * like if the client wants to print the data the server returns back, and the client wants to delete that instruction after
 * it is executed, you would use the enum DELETE.
 */
public enum WhatToDoWithClientSavedInstructions implements Serializable{
	/** delete the instructions. */
	DELETE(WhatToDoWithClientSavedInstructions::delete),
	/**  keep the instructions. */
	KEEP(WhatToDoWithClientSavedInstructions::keep);
	/**
	 * Instantiates a new what to do with client saved instructions.
	 * @param output the function for handeling the output
	 */
	private WhatToDoWithClientSavedInstructions(FunctionForTheOutput output) {
		this.output=output;
	}
	/** The output. */
	private FunctionForTheOutput output;
	/**
	 * this is a method which is used for executing/keeping the clients instructions
	 * @param storage the storage which the instructions are saved in
	 * @param id the id of the thing we are searching for
	 * @return the clients instructions for receiving data with the matching id, after it has been handled in the way the enum wants to
	 */
	public ClientsInstructionsForReceivingData<?> execute(ArrayList<ClientsInstructionsForReceivingData<?>> storage, int id) {
		return output.execute(storage, id);
	}

	/**
	 * this is a functional interface for the function the enum wants, like if the enum wants to delete, this is the function
	 *it uses to delete, etc
	 */
	private static interface FunctionForTheOutput{
		/**
		 *this is a functional interface for the function the enum wants, like if the enum wants to delete, this is the function
		 *it uses to delete, etc
		 *
		 * @param storage where the instructions are kept
		 * @param id the id of the instructions it is searching for
		 * @return the clients instructions for receiving data with the matching id, after it has been deleted/kept
		 */
		ClientsInstructionsForReceivingData<?> execute(ArrayList<ClientsInstructionsForReceivingData<?>> storage,int id);
	}
	/**
	 * This is the method the DELETE enum uses to delete stuff from the arrayList, it is o(n)
	 * @param storage the storage that is deleting from
	 * @param id the id of the thing it is deleting
	 * @return the clients instructions for receiving data with the matching id, which is no longer in the list.
	 */
	public static ClientsInstructionsForReceivingData<?> delete(ArrayList<ClientsInstructionsForReceivingData<?>> storage, int id) {
		ClientsInstructionsForReceivingData<?> found=keep(storage,id);
		if(found!=null) {
			storage.remove(found);
		}
		return found;
	}
	/**
	 * this is the function the KEEP enum uses to keep the data in the list, it simply searches the list and returns the matching one
	 * @param storage the place the instructions are stored
	 * @param id the id of the instruction it is searching for
	 * @return the clients instructions for receiving data with the matching id.
	 */
	public static ClientsInstructionsForReceivingData<?> keep(ArrayList<ClientsInstructionsForReceivingData<?>> storage, int id) {
		for(int i=0;i<storage.size();i++) {
			if(storage.get(i).compareID(id)) {
				return storage.get(i);
			}
		}//I know this is kind of wrong, but there should be NO CIRCUMSTANCES WHERE you cant find it
		throw new IllegalArgumentException("error, we could not find your data in "+WhatToDoWithClientSavedInstructions.KEEP.getClass().getName()+"\r\n, make sure you got the correct id, inputed id="+id);
	}
}
