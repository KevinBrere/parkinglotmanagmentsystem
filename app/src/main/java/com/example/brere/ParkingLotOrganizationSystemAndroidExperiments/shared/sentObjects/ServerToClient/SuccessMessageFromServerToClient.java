package com.example.brere.ParkingLotOrganizationSystemAndroidExperiments.shared.sentObjects.ServerToClient;

import java.util.ArrayList;
/**
 * The Class SuccessMessageFromServerToClient, which basically sends data from the server to the client when
 * the server is successful at executing its serverFunction
 */
public class SuccessMessageFromServerToClient implements MessageFromServerToClient{
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -992979405379340195L;
	/** The what to do with the clients saved instructions. basically this enum tells the client what to do with the instructions it has
	 * saved, like whether to delete those instructions or save them for later use */
	private WhatToDoWithClientSavedInstructions whatToDo;
	/** The data from operation. */
	private Object dataFromOperation;
	/** The id of the client saved instructions */
	int id;
	/**
	 * this is a message from the server to the client to tell the client that their use was done successfully.  in this
	 * it will give the message back to the cleint. 
	 * @param whatToDo what to do with the instructions on the client side, ie whether to save, delete, etc it
	 * @param id the id of the saved instructions on the client side
	 * @param dataFromOperation the data wich is needed in the success operation. 
	 */
	public SuccessMessageFromServerToClient(WhatToDoWithClientSavedInstructions whatToDo,int id, Object dataFromOperation) {
		super();
		this.whatToDo = whatToDo;
		this.dataFromOperation = dataFromOperation;
		this.id=id;
	}
	/* (non-Javadoc)
	 * @see shared.serverClientCommunicationFramework.serverMessageFromServerToClient.MessageFromServerToClient#printMessage(java.util.ArrayList)
	 */
	@Override
	public  void printMessage(ArrayList<ClientsInstructionsForReceivingData<?>> storage) {
		ClientsInstructionsForReceivingData<?> storageResult=whatToDo.execute(storage, id);
		
		storageResult.receiveSucessData(dataFromOperation);
	}
}
