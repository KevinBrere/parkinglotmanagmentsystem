package  com.example.brere.ParkingLotOrganizationSystemAndroidExperiments.client.dataFieldStuff;

import java.util.ArrayList;

/**
 * this is a class for refrencing two tables with each other.
 *
 * REQUIRES THE REFERED TO TABLE HAVING ONLY ONE KEY
 *
 * @author brere
 *
 */
public class Reference extends Column{
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 3589919710094589888L;
	/** The column that will be referenced. */
	private final Column refrenceColumn;
	/** The deletion constraint, like when the data is deleted from the database, what will be
	 * done to the tuple referencing it */
	private final DeleteConstraint deletionConstraint;
	/** is the Reference auto complete, which means that if this Reference is added to an insertion System that
	 * already has the Column this object refrences, will the dataField be completed automatically (like will it be set to the value of the
	 * refrenced column)?
	 */
	private boolean autoComplete;
	/**
	 * The Enum DeleteConstraint, which represents what should happen if the tuple the Refrence refrences is deleted.
	 * like will it autoMatically delete this tuple as well, or set the Refrence null, etc.
	 */
	public enum DeleteConstraint{
		/** the enum representing that if the Refered tuple is deleted, you should set this refrences value to null */
		SETNULL("SET NULL"),
		/** The enum representing theat if the Refered tuple is deleted, you should delete the refering tuple automatically */
		DELETEAUTOMATICALLY("CASCADE"),
		/** The enum representing that if the Refered tuple is about to be deleted, the system should stop the user, and tell them
		 * that there is something refering to it, and they must change that before deleting that tuple */
		STOPUSER("CASCADE");
		/**
		 * Instantiates a new delete constraint.
		 *
		 * @param sqlConstraint the sql constraint, which is the string that sql uses to determine what to do on
		 * thier end.
		 */
		 DeleteConstraint(String sqlConstraint) {
			this.sqlConstraint=sqlConstraint;
		}
		/** the sql constraint, which is the string that sql uses to determine what to do on*/
		private String sqlConstraint;
		/* (non-Javadoc)
		 * @see java.lang.Enum#toString()
		 */
		public String toString() {
			return sqlConstraint;
		}
	}
	/**
	 * Instantiates a new reference object
	 *
	 * @param columnName the name of the column
	 * @param maxSize the max size of the column
	 * @param columnNumber the number of the column
	 * @param isNessicaryForInsertion the is nessicary for insertion
	 * @param isKey is the column a key
	 * @param colInRefrenceTable the column that it references
	 * @param deletionConstraint what to do when the referenced column is deleted
	 */
	public Reference(String columnName, int maxSize, int columnNumber, boolean isNessicaryForInsertion, boolean isKey,
					 Column colInRefrenceTable, DeleteConstraint deletionConstraint) {
		super(columnName, maxSize, columnNumber, isNessicaryForInsertion, isKey);

		this.refrenceColumn=colInRefrenceTable;
		this.deletionConstraint=deletionConstraint;
		
		this.setChecksForUserInput(new ArrayList<CheckerOfUserInput>(colInRefrenceTable.getChecksForUserInput()));
		this.setUserInputBoxConstructor(colInRefrenceTable.getUserInputBoxConstructor());
		this.autoComplete=true;
		refrenceColumn.getTableWhichItBelongsTo().addRefrenceeToThisTable(this);
	}
	/**
	 * Instantiates a new reference object, copying the values from the other, one except that its column number is set to i
	 * @param ref the refrence object that will be copied
	 * @param i the new column number
	 */
	public Reference(Reference ref, int i) {
		super(ref,i);

				this.refrenceColumn=ref.getRefrenceColumn().copy(ref.getRefrenceColumn().getColumnNumber());
				this.deletionConstraint=ref.deletionConstraint;
				this.autoComplete=ref.isAutoComplete();
				this.setUserInputBoxConstructor(this.getRefrenceColumn().getUserInputBoxConstructor());
	}
	/* (non-Javadoc)
	 * @see client.dataFieldStuff.Column#copy(int)
	 */
	@Override
	public Reference copy(int i) {
		return new Reference(this,i);
	}

	/**
	 * Checks if the reference should be automatically completed if its referenced column is already
	 * in an insertionSystem
	 * @return true, if is auto complete
	 */
	public boolean isAutoComplete() {
		return autoComplete;
	}
	/**
	 * Sets whether the reference should be automatically completed if its referenced column is already
	 * in an insertionSyste,
	 *
	 * @param autoComplete the new auto complete
	 */
	public void setAutoComplete(boolean autoComplete) {
		this.autoComplete = autoComplete;
	}
	/**
	 * Gets the table that it refers to.
	 * @return the table that the refrence refers to
	 */
	public Table getRefrenceTable() {
		return getRefrenceColumn().getTableWhichItBelongsTo();
	}
	/**
	 * Gets the column it refers to
	 * @return the column it refers to
	 */
	public Column getRefrenceColumn() {
		return refrenceColumn;
	}
	/**
	 * Gets what to do if the Refered to tuple is deleted
	 * @return what to do if the Refered to tuple is deleted
	 */
	public DeleteConstraint getDeletionConstraint() {
		return deletionConstraint;
	}
	/**
	 * checks if the Reference object refers to the other object, either directly or indirectly
	 * (ie if the column that this object references is also a Reference, and if it refers to the Column)
	 * @param col the col that will be checked if this Reference refers to it.
	 * @return true, if this object refers to the inputed column, either directly or indirectly.
	 */
	public boolean refersTo(Column col) {
		if(this.refrenceColumn.equals(col)) {
			return true;
		}
		if(refrenceColumn instanceof Reference) {
			return refersTo(((Reference)refrenceColumn).getRefrenceColumn());
		}
		return false;
	}

}
