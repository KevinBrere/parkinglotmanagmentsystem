package com.example.brere.ParkingLotOrganizationSystemAndroidExperiments.server;

import com.example.brere.ParkingLotOrganizationSystemAndroidExperiments.shared.clientAndServerCommunicationSystems.ReceiverFromSocket;
import com.example.brere.ParkingLotOrganizationSystemAndroidExperiments.shared.clientAndServerCommunicationSystems.SenderToSocket;
import com.example.brere.ParkingLotOrganizationSystemAndroidExperiments.shared.clientAndServerCommunicationSystems.SocketCommunicationConstructor;
import com.example.brere.ParkingLotOrganizationSystemAndroidExperiments.shared.sentObjects.ServerToClient.MessageFromServerToClient;
import com.example.brere.ParkingLotOrganizationSystemAndroidExperiments.shared.sentObjects.clientToServer.ServerFunction;
import com.example.brere.ParkingLotOrganizationSystemAndroidExperiments.shared.stuffInTheServer.ServerSystemsToolBox;

import java.net.Socket;

/**
 * The Class SingleClientServerSide, which is a class which serves a single client. so each time a client
 * connects, one singleClientServerSide object will serve them
 */
public class SingleClientServerSide {

	/** The receiver of the clients inputs. */
	// chage string later
	private ReceiverFromSocket<ServerFunction<?>> receiver;

	/** The tool box, which the serverFunctions recieved from receiverFromSocket will use */
	private ServerSystemsToolBox toolBox;

	/**
	 * Instantiates a new single client server side.
	 *
	 * @param sock the socket used for communicating with the client
	 */
	public SingleClientServerSide(Socket sock) {
		SocketCommunicationConstructor<MessageFromServerToClient, ServerFunction<?>> constructor = new SocketCommunicationConstructor<MessageFromServerToClient, ServerFunction<?>>(
				sock, System.out::println);
		SenderToSocket<MessageFromServerToClient> sender = constructor.getSender();
		toolBox = new ServerSystemsToolBox(sender);
		constructor.getSocketReceiver().setConsumerOfOutput(this::receiveValue);
		receiver = constructor.getSocketReceiver();
	}

	/**
	 * Receives a serverFunction from the cleint, and executes its command
	 * @param function the function from the client
	 */
	public void receiveValue(ServerFunction<?> function) {
		function.accept(toolBox);
	}

	/**
	 * Gets the receiver.
	 *
	 * @return the receiver
	 */
	public ReceiverFromSocket<ServerFunction<?>> getReceiver() {
		return receiver;
	}

}