package com.example.brere.ParkingLotOrganizationSystemAndroidExperiments.shared.sentObjects.ServerToClient;

import java.util.function.Consumer;

public class ClientsInstructionsForReceivingData<T> {
	/** The id of the message sent to the sever. */
	private int id;
	/** Instructions for what to do when the sever sends an error message back. */
	private Consumer<ErrorData> errorDisplayer;
	/** Instructions for what to do when the server sends a success message back */
	private Consumer<T> receiver;
	/**
	 * Instantiates a new clients instructions for receiving data.
	 *
	 * @param id the id of the message sent to the server
	 * @param errorDisplayer Instructions for what to do when the sever sends an error message back.
	 * @param receiver Instructions for what to do when the server sends a success message back
	 */
	public ClientsInstructionsForReceivingData(int id, Consumer<ErrorData> errorDisplayer,
			Consumer<T> receiver) {
		super();
		this.id = id;
		this.errorDisplayer = errorDisplayer;
		this.receiver = receiver;
	}
	/**
	 * this method is called when the server had an error, so the errorData needs to be sent
	 * into the errorDisplayer
	 *
	 * @param errorMessage the error message which will be sent to the client
	 */
	public void displayErrorMessage(ErrorData errorMessage) {
	 errorDisplayer.accept(errorMessage);
	}
	/**
	 * this method is called when the server was successful, so the successful message is sent
	 * back to the user
	 *
	 * @param data the data that is sent to the user
	 */
	@SuppressWarnings("unchecked")
	public void receiveSucessData(Object data) {
		T recieved=null;
		try {
			recieved=(T) data;
		}catch(ClassCastException e) {
			throw new IllegalArgumentException("error with casting in ClientsInstructionsForReceiving, please contact customer support for more details: error code"+e.getMessage()+" "+e.getLocalizedMessage());
		}
	 receiver.accept(recieved);
	}
	/**
	 * Compares the id of this object, and the inputed id.
	 *
	 * @param id the id which will be compared
	 * @return true, if the ids are the same
	 */

	public boolean compareID(int id){
		return this.id==id;
	}
	
	
}
