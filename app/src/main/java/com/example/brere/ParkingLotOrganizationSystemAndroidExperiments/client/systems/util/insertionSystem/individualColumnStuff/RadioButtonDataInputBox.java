package com.example.brere.ParkingLotOrganizationSystemAndroidExperiments.client.systems.util.insertionSystem.individualColumnStuff;

import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.example.brere.ParkingLotOrganizationSystemAndroidExperiments.R;
import com.example.brere.ParkingLotOrganizationSystemAndroidExperiments.client.dataFieldStuff.DataField;
import com.example.brere.ParkingLotOrganizationSystemAndroidExperiments.client.dataFieldStuff.DataFieldList;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * This is a class for inputing data using radioButtons. Like if you have only a few values that are
 * valid, then you can use the RadioButtonDataInputBox to display only a few radioButtons for that input
 */
public class RadioButtonDataInputBox extends DataInputBox {
	/**
	 * creates a DataInputBoxConstructor for RadioButtonDataInputBox, which uses the entered buttonNames
	 * for the values that you can enter. Like one radioButton for each string entered
	 * @param buttonNames the text that will be displayed next to each radiobutton. ie that radioButtons value
	 * @return a DataInputBoxConstructor which will construct a RadioButtonDataInputBox that has a radiobutton
	 * which can select between the given strings
	 */
	public static DataInputBoxConstructor constructDataFieldInputBox(String... buttonNames) {

		return new DataInputBoxConstructor() {
			private static final long serialVersionUID = 22401L;
			@Override
			public DataInputBox constructDataInputBox(DataField d, DataFieldList listFieldBelongsTo, @NonNull ViewGroup parent, int viewType) {

				ArrayList<String> arrayList = new ArrayList<>(Arrays.asList(buttonNames));
				View v = LayoutInflater.from(parent.getContext())
						.inflate(R.layout.radio_button_input_box, parent, false);

				return new RadioButtonDataInputBox(v,d,listFieldBelongsTo, arrayList);
			}
		};
	}
	/**serial id */
	private static final long serialVersionUID = 8663142290635324307L;

	/** the next id of the next radiobutton added to the buttonGroup  */
	private static int nextID=0;
	/**all of the radioButtons in the view */
	private final ArrayList<RadioButton> buttons;
	/**The RadioGroup for all of the buttons */
	private final RadioGroup buttonGroup;

	/**
	 * constructs a new RadioButtonDataInputBox
	 * @param view the view that the user will see
	 * @param data the data that the RadioButonDataInputBox represents
	 * @param listDataBelongsTo the list that the data belongs to
	 * @param options the diffrent options that the user can choose between. like 1 string for each
	 *                radio button
	 */
	public RadioButtonDataInputBox(View view, DataField data, DataFieldList listDataBelongsTo, ArrayList<String> options) {
		super(view,data,listDataBelongsTo);

		buttons = new ArrayList<>();
		TextView label=view.findViewById(R.id.RadioButtonLabel);
		buttonGroup=view.findViewById(R.id.RadioButtonGroup);
		buttonGroup.setOrientation(LinearLayout.HORIZONTAL);
		for(int i=0;i<options.size();i++){
			RadioButton button=new RadioButton(view.getContext());
			button.setId(nextID++);
			button.setText(options.get(i));
			buttons.add(button);

			RadioGroup.LayoutParams rprms = new RadioGroup.LayoutParams(RadioGroup.LayoutParams.WRAP_CONTENT, RadioGroup.LayoutParams.WRAP_CONTENT);

			buttonGroup.addView(button,i,rprms);
		}

        label.setText(data.getColumn().getColumnName());
	}

	@Override
	public String get() {

		buttons.forEach((RadioButton but)-> System.out.println(but.getText()));

		for(RadioButton current: buttons){
			if(current.isChecked()){
				return (String) current.getText();
			}
		}
		return null;
	}

	@Override
	public void setEditable(boolean edit) {
		buttonGroup.setEnabled(edit);
	}

	@Override
	protected void setUsersInputText(String s) {

		for (RadioButton current : buttons) {
			 if ( current.getText().equals(s)) {
				setButtonChecked( current);
				return;
			}

		}
		throw new IllegalArgumentException(
				"error, the set Text does not match any radio button in RadioButtonDataInputBox, please contact support for help");
	}

	/**
	 *  when the text that was set is determined to match the RadioButton, then it checks the inputed
	 *  radiobutton
	 * @param but the button which will be checked
	 */
	private void setButtonChecked(RadioButton but) {
		setTextInvisable();
		but.setChecked(true);
	}

	@Override
	public void setTextInvisable() {
		buttonGroup.clearCheck();
	}

}
