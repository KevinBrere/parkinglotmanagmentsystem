package com.example.brere.ParkingLotOrganizationSystemAndroidExperiments.client.systems.util.insertionSystem;

import android.os.Bundle;
import android.support.annotation.NonNull;

import com.example.brere.ParkingLotOrganizationSystemAndroidExperiments.client.dataFieldStuff.DataFieldList;
import com.example.brere.ParkingLotOrganizationSystemAndroidExperiments.client.dataFieldStuff.TableCollection;
import com.example.brere.ParkingLotOrganizationSystemAndroidExperiments.client.systems.util.insertionSystem.individualColumnStuff.DataInputBox;
import com.example.brere.ParkingLotOrganizationSystemAndroidExperiments.pretend.ControlSystemsActivity;
import com.example.brere.ParkingLotOrganizationSystemAndroidExperiments.shared.sentObjects.clientToServer.dataBaseFunctions.DataBaseFunction;
import com.example.brere.ParkingLotOrganizationSystemAndroidExperiments.shared.sentObjects.clientToServer.dataBaseFunctions.DeletingDataBaseFunction;
import com.example.brere.ParkingLotOrganizationSystemAndroidExperiments.shared.sentObjects.clientToServer.dataBaseFunctions.ModificationDataBaseFunction;

import java.util.ArrayList;

/**
 * this is a UserInputSystem for Modifying a set of data. Like say you want to modify a user in a database
 * by changing their name, you would use this activity
 *
 * REQUIRES: the DataFieldList in the insertion view to be the same
 */
public class ModificationSystem extends UserInputSystem {
	/**
	 * creates a bundle for launching a ModificationSystem for modifying the inputed dataFieldLists
	 * @param dataFieldLists the data that will be modified by the user
	 * @return a bundle for launching a ModificationSystem for modifying the inputed dataFieldLists
	 */
	public static Bundle createBundleForSystem(@NonNull ArrayList<DataFieldList> dataFieldLists) {
		Bundle b = UserInputSystem.createBundleForSystem("modify a " + dataFieldLists.get(0).getTable().getTitle(), dataFieldLists);
		ControlSystemsActivity.CreateControlSystem instantiate = ModificationSystem::new;
		b.putSerializable(ControlSystemsActivity.CONTROLSYSTEMGENERATEORID, instantiate);
		return b;
	}
	/**
	 * creates a bundle for launching a ModificationSystem for modifying the inputed dataFieldList
	 * @param list the data that will be modified by the user
	 * @return a bundle for launching a ModificationSystem for modifying the inputed dataFieldList
	 */
	public static Bundle createBundleForSystem(@NonNull DataFieldList list) {
		ArrayList<DataFieldList> dataFieldLists = new ArrayList<>();
		dataFieldLists.add(list);
		return ModificationSystem.createBundleForSystem(dataFieldLists);
	}

	@Override
	public void setView(InsertionView view) {
		super.setView(view);
		if(super.getUserData().get(TableCollection.USERTABLE.SUPERSSN).getData()==null) {
			addDataFieldListConsumerButton("modify",
					new MultiServerCallMethod(null, null, this::createServerFunction, super::getServ));
			addDataFieldListConsumerButton("delete",
					new MultiServerCallMethod(null, null, DeletingDataBaseFunction::new, super::getServ));
		}
	}

	/**
	 * creates the server Function that the ModificationSystem wants to use
	 * @param list the list that will go into the modifying database function
	 * @return the DataBaseFunction for modifying a user.
	 */
	public DataBaseFunction createServerFunction(DataFieldList list) {
		return new ModificationDataBaseFunction(list);
	}
	@Override
	protected InsertionView createInsetionView(ArrayList<InsertionAdapter.DataInputBoxConstructor> constructors, ArrayList<InsertionAdapter.onDataInputBoxBind> onbind) {
				onbind.add((DataInputBox box, int i)->{
			if(box.getDataField().getColumn().isKey()){
				box.setEditable(false);
			}
		});
		return super.createInsetionView(constructors,onbind);
	}

}