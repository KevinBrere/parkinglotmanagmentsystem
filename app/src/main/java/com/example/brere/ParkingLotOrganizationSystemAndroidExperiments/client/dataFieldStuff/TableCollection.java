package  com.example.brere.ParkingLotOrganizationSystemAndroidExperiments.client.dataFieldStuff;

import com.example.brere.ParkingLotOrganizationSystemAndroidExperiments.client.communicationToServer.ServerCommunicator;
import com.example.brere.ParkingLotOrganizationSystemAndroidExperiments.client.systems.util.insertionSystem.individualColumnStuff.OtherDataInputBoxConstructors;
import com.example.brere.ParkingLotOrganizationSystemAndroidExperiments.client.systems.util.insertionSystem.individualColumnStuff.SpinnerDataInputBox;
import com.example.brere.ParkingLotOrganizationSystemAndroidExperiments.client.systems.util.insertionSystem.individualColumnStuff.TimeDurationInputBox;
import com.example.brere.ParkingLotOrganizationSystemAndroidExperiments.shared.sentObjects.ServerToClient.ErrorData;

import java.util.ArrayList;
/**
 * this is a class primarily for programmer interaction. It holds a table object
 * for each table in the database
 *
 * @author brere
 *
 */
public class TableCollection {
	// when you add a table, make sure to add it to the creation table function
	/** Table for users */
	public static final UserTable USERTABLE=new UserTable();
	/** Table for Tickets */
	public static final TicketTable TICKETTABLE = new TicketTable();
	/** Table for veichles */
    public static final VehicleTable VEHICLETABLE = new VehicleTable();
	/** Table for Disputes */
	public static final DisputeTable DISPUTETABLE = new DisputeTable();
	/** Table for parking lots */
	public static final ParkingLotTable PARKINGLOTTABLE = new ParkingLotTable();
	/** Table for parking passes */
	public static final ParkingPassTable PARKINGPASSTABLE =  new ParkingPassTable();
	/** Table for owners */
	public static final OwnerTable OWNERTABLE = new OwnerTable();
	/** Table for assignments */
	public static final AssignmentRelationShipTable ASSIGNMENTS = new AssignmentRelationShipTable();
	/** Table for work days */
	public static final WorkDaysTable WORKDAYS = new WorkDaysTable();
	/** An arrayList of tables which holds each table that exists */
	private static final Table[] tables={USERTABLE.table,TICKETTABLE.table,VEHICLETABLE.table,DISPUTETABLE.table, PARKINGLOTTABLE.table,
   PARKINGPASSTABLE.table,OWNERTABLE.table,ASSIGNMENTS.table,WORKDAYS.table};
	/**
	 * a nessicary evil. this is basically an object which sets up the Refrences for
	 * each table. make sure this obect is below the instantiaton of each Table
	 */
	private static RefreceSetterUpper refreceSetterUpper=new RefreceSetterUpper();

	public static class UserTable{
    	public Table table;
        public final Column ID;
        public final Column FIRSTNAME;
        public final Column LASTNAME;
        public final Column USERNAME;
        public final Column PASSWORD;
        public final Column EMPLOYMENTSTATUS;
        public final Column PHONENUMBER;
        public final Column EMAIL;
        public Reference SUPERSSN;
        ArrayList<Column> cols;


        public UserTable(){

        	table=new Table("USERS");
            cols=new ArrayList<Column>();
            
            ID=new Column("ID",8,1,true,true);
            ID.addCheck(CheckerOfUserInput.checkIfColumnIsAnInteger());

//            String[] arrs= {"1","2","55"};
//           	ID.setUserInputBoxConstructor(SetterUpOfDataFieldInputManager.createDataInputBoxInputConstructor(SpinnerDataInputBox.constructDataFieldInputBox(arrs)));

					ID.setUserInputBoxConstructor(SetterUpOfDataFieldInputManager.createDataInputBoxInputConstructor(OtherDataInputBoxConstructors.constructNumberDataFieldInputBox()));
//					ID.setUserInputBoxConstructor(SetterUpOfDataFieldInputManager.createDataInputBoxInputConstructor(TimeDurationInputBox.constructDataFieldInputBox()));

			cols.add(ID);
            
            FIRSTNAME=new Column("First Name",8,2,true,false);
            FIRSTNAME.addCheck(CheckerOfUserInput.checkIfColumnIsOnlyLetters());
            cols.add(FIRSTNAME);
            LASTNAME=new Column("Last Name",20,3,true,false);
            LASTNAME.addCheck(CheckerOfUserInput.checkIfColumnIsOnlyLetters());

            cols.add(LASTNAME);
            USERNAME=new Column("Username",15,4,true,false);
            cols.add(USERNAME);
            PASSWORD=new Column("Password",30,5,true,false);
			PASSWORD.setUserInputBoxConstructor(SetterUpOfDataFieldInputManager.createDataInputBoxInputConstructor(OtherDataInputBoxConstructors.constructPasswordDataFieldInputBox()));

			PASSWORD.setHiddenFromSearches(true);
            
            cols.add(PASSWORD);
            EMPLOYMENTSTATUS=new Column("Employment Status",9,6,true,false);

            EMPLOYMENTSTATUS.setUserInputBoxConstructor(SetterUpOfDataFieldInputManager.createDataInputBoxInputConstructor(SpinnerDataInputBox.constructDataFieldInputBox("new", "pernament","temporary","fired","hired")));
            cols.add(EMPLOYMENTSTATUS);

            PHONENUMBER=new Column("Phone No.",12,7,true,false);
            PHONENUMBER.addCheck(CheckerOfUserInput.checkIfColumnIsValidPhoneNumber());
			PHONENUMBER.setUserInputBoxConstructor(SetterUpOfDataFieldInputManager.createDataInputBoxInputConstructor(OtherDataInputBoxConstructors.constructPhoneDataFieldInputBox()));

			cols.add(PHONENUMBER);
            
            EMAIL=new Column("E-Mail",30,8,true,false);
            EMAIL.addCheck(CheckerOfUserInput.checkIfColumnIsAValidEmail());

            cols.add(EMAIL);

           //spaghetti i know
           table.setUp( cols);
        }
		/**
		 * sets up the refrecnes in this table
		 */
    	public void setupRefrences() {
            SUPERSSN=new Reference("SupervisorID",8,9,false,false,ID, Reference.DeleteConstraint.STOPUSER);
            SUPERSSN.setAutoComplete(false);
            cols.add(SUPERSSN);
            
            
            table.setUp(cols);
    	}

    }
    
    public static class TicketTable
    {
    	public Table table;
    	public final Column TICKETNUMBER;
    	public Reference PLATENUMBER;
    	public Reference LOTNO;
    	public final Column DATE;
    	public final Column DESCRIPTION;
    	public final Column REASON;
    	public Reference CREATOR;
    	private ArrayList<Column> cols;
    	public TicketTable()
    	{
    		table=new Table("TICEKTS");
    		cols = new ArrayList<Column>();
    		
    		TICKETNUMBER = new Column("Ticket No.", 8, 1, true,true);
			TICKETNUMBER.setUserInputBoxConstructor(SetterUpOfDataFieldInputManager.createDataInputBoxInputConstructor(OtherDataInputBoxConstructors.constructNumberDataFieldInputBox()));
			TICKETNUMBER.addCheck(CheckerOfUserInput.checkIfColumnIsAnInteger());

    		DATE = new Column("Date", 10, 4, true,false);
    		DATE.addCheck(CheckerOfUserInput.checkIfColumnIsValidDate());
			DATE.setUserInputBoxConstructor(SetterUpOfDataFieldInputManager.createDataInputBoxInputConstructor(OtherDataInputBoxConstructors.constructDateDataFieldInputBox()));

			DESCRIPTION = new Column("Description", 250, 5, true,false);
			DESCRIPTION.setUserInputBoxConstructor(SetterUpOfDataFieldInputManager.createDataInputBoxInputConstructor(OtherDataInputBoxConstructors.constructBigDataFieldInputBox()));
    		REASON = new Column("Reason", 250, 6, true,false);
			REASON.setUserInputBoxConstructor(SetterUpOfDataFieldInputManager.createDataInputBoxInputConstructor(OtherDataInputBoxConstructors.constructBigDataFieldInputBox()));

    		cols.add(TICKETNUMBER);
    		cols.add(DATE);
    		cols.add(DESCRIPTION);
    		cols.add(REASON);

    	}
		/**
		 * sets up the refrecnes in this table
		 */
    	public void setupRefrences() {
    		//reference
    		PLATENUMBER = new Reference("Plate No.", 8, 2, true,false,VEHICLETABLE.PLATENUMBER, Reference.DeleteConstraint.DELETEAUTOMATICALLY);
    		cols.add(PLATENUMBER);
    		LOTNO = new Reference("Lot No.", 12, 3, false,false,PARKINGLOTTABLE.lotNumber, Reference.DeleteConstraint.SETNULL);
    		cols.add(LOTNO);
    		CREATOR = new Reference("Creator", 8, 7, false, false,USERTABLE.ID, Reference.DeleteConstraint.SETNULL);
    		cols.add(CREATOR);
    		table.setUp( cols);
    	}
    	
    }
    
    public static class VehicleTable
    {
    	public Table table;
    	public final Column PLATENUMBER;
    	public final Column MAKE;
    	public final Column MODEL;
    	public final Column COLOUR;
    	
    	
    	public VehicleTable()
    	{
    		ArrayList<Column> cols = new ArrayList<Column>();
    		PLATENUMBER = new Column("Plate No.", 8, 1, true,true);
    		//no checks because custom license plates are weird
    		MAKE = new Column("Make", 15, 2, true,false);
    		MODEL = new Column("Model", 15, 3, true,false);
    		COLOUR = new Column("Colour", 15, 4, true,false);
    		
    		cols.add(PLATENUMBER);
    		cols.add(MAKE);
    		cols.add(MODEL);
    		cols.add(COLOUR);
    		
    		table = new Table("VEHICLES", cols);
    	}
    }
    
    public static class DisputeTable
    {
    	public Table table;
    	public final Column DISPUTENUMBER;
    	public Reference TICKETNO;
    	public final Column DESCRIPTION;
    	public final Column REASON;
    	public Reference CREATOR;
    	public Reference OWNERSIN;
    	private ArrayList<Column> cols;
    
    	public DisputeTable()
    	{
    		table=new Table("DISPUTES");
    		cols = new ArrayList<Column>();
    		DISPUTENUMBER = new Column("Dispute No.", 8, 1, true,true);
    		DISPUTENUMBER.addCheck(CheckerOfUserInput.checkIfColumnIsAnInteger());
			DISPUTENUMBER.setUserInputBoxConstructor(SetterUpOfDataFieldInputManager.createDataInputBoxInputConstructor(OtherDataInputBoxConstructors.constructNumberDataFieldInputBox()));

			DESCRIPTION = new Column("Description", 250, 3, true,false);
			DESCRIPTION.setUserInputBoxConstructor(SetterUpOfDataFieldInputManager.createDataInputBoxInputConstructor(OtherDataInputBoxConstructors.constructBigDataFieldInputBox()));

			REASON = new Column("Reason", 250, 4, true,false);
			REASON.setUserInputBoxConstructor(SetterUpOfDataFieldInputManager.createDataInputBoxInputConstructor(OtherDataInputBoxConstructors.constructBigDataFieldInputBox()));

			cols.add(DISPUTENUMBER);
    		cols.add(DESCRIPTION);
    		cols.add(REASON);
    	
    	}
		/**
		 * sets up the refrecnes in this table
		 */
    	protected void setupRefrences() {
    		TICKETNO = new Reference("Ticket No.", 8, 2, true,true,TICKETTABLE.TICKETNUMBER, Reference.DeleteConstraint.DELETEAUTOMATICALLY);
    		cols.add(TICKETNO);
    		CREATOR = new Reference("Creator", 8, 5, false, false,USERTABLE.ID, Reference.DeleteConstraint.SETNULL);
    		cols.add(CREATOR);
    		OWNERSIN= new Reference("Owner SIN",10,6,true,false,OWNERTABLE.SIN, Reference.DeleteConstraint.STOPUSER);
    		cols.add(OWNERSIN);
    		table.setUp(cols);
    	}
    }
    
    public static class ParkingPassTable
    {
    	public Table table;
    	public final Column SERIALNO;
    	public Reference ASSIGNEDLOT;
    	public final Column EXPIRATION;
    	private final ArrayList<Column> cols;
    	
    	public ParkingPassTable()
    	{

    		table=new Table("PARKINGPASSES");
    		cols=new ArrayList<Column>();
    		
    		SERIALNO = new Column("Searil No.", 8, 1, true,true);
    		SERIALNO.addCheck(CheckerOfUserInput.checkIfColumnIsAnInteger());
			SERIALNO.setUserInputBoxConstructor(SetterUpOfDataFieldInputManager.createDataInputBoxInputConstructor(OtherDataInputBoxConstructors.constructNumberDataFieldInputBox()));

			//we can add more checks later to serial number
    		EXPIRATION = new Column("Experation", 10, 3, true,false);
    		EXPIRATION.addCheck(CheckerOfUserInput.checkIfColumnIsValidDate());
			EXPIRATION.setUserInputBoxConstructor(SetterUpOfDataFieldInputManager.createDataInputBoxInputConstructor(OtherDataInputBoxConstructors.constructDateDataFieldInputBox()));

    		
    		cols.add(SERIALNO);
    		cols.add(EXPIRATION);
    	}
		/**
		 * sets up the refrecnes in this table
		 */
    	public void setupRefrences() {
    		ASSIGNEDLOT = new Reference("Assigned Lot", 12, 2, true,false,PARKINGLOTTABLE.lotNumber, Reference.DeleteConstraint.DELETEAUTOMATICALLY);
    		cols.add(ASSIGNEDLOT);
    		
    		table.setUp(cols);

    	}
    }

    public static class ParkingLotTable
    {
    	public Table table;
    	public final Column address;
    	public final Column lotNumber;
    	public final Column type;
    	
    	public ParkingLotTable()
    	{
    		ArrayList<Column> cols = new ArrayList<Column>();
    		address = new Column("Address", 40, 1, true,false);
    		lotNumber = new Column("Lot Number", 12, 2, true,true);
    		lotNumber.addCheck(CheckerOfUserInput.checkIfColumnIsAnInteger());
			lotNumber.setUserInputBoxConstructor(SetterUpOfDataFieldInputManager.createDataInputBoxInputConstructor(OtherDataInputBoxConstructors.constructNumberDataFieldInputBox()));


			type = new Column("Type", 10, 3, true,false);
    		String[] types= {"private", "public"};
			type.setUserInputBoxConstructor(SetterUpOfDataFieldInputManager.createDataInputBoxInputConstructor(SpinnerDataInputBox.constructDataFieldInputBox(types)));
			type.addCheck(CheckerOfUserInput.createCheckerForValues(types));

    		cols.add(address);
    		cols.add(lotNumber);
    		cols.add(type);
    		
    		table = new Table("PARKINGLOT", cols);
    	}
    }
    
    public static class OwnerTable{
    	
    	public Table table;
    	public final Column SIN;
    	public final Column firstName;
    	public final Column lastName;
    	public final Column dateOfBirth;
    	
    	public OwnerTable()
    	{
    		ArrayList<Column> cols = new ArrayList<Column>();
    		firstName = new Column("FirstName", 20, 1, true,false);
    		firstName.addCheck(CheckerOfUserInput.checkIfColumnIsOnlyLetters());
    		lastName = new Column("Last Name", 40, 2, true,false);
    		lastName.addCheck(CheckerOfUserInput.checkIfColumnIsOnlyLetters());
    		dateOfBirth = new Column("Date of Birth", 10, 3, true,false);
    		dateOfBirth.addCheck(CheckerOfUserInput.checkIfColumnIsValidDate());
			dateOfBirth.setUserInputBoxConstructor(SetterUpOfDataFieldInputManager.createDataInputBoxInputConstructor(OtherDataInputBoxConstructors.constructDateDataFieldInputBox()));

			SIN=new Column("Social Insurence No", 10, 4, true, true);
    		SIN.addCheck(CheckerOfUserInput.checkIfColumnIsAnInteger());
			SIN.setUserInputBoxConstructor(SetterUpOfDataFieldInputManager.createDataInputBoxInputConstructor(OtherDataInputBoxConstructors.constructNumberDataFieldInputBox()));



			cols.add(firstName);
    		cols.add(lastName);
    		cols.add(dateOfBirth);
    		cols.add(SIN);
    		table = new Table("OwnerTable", cols);
    	}
    	
    	
    }
    
    public static class AssignmentRelationShipTable
    {
    	public Table table;
    	public Reference guardID;
    	public Reference lotNO;
    	
    	public AssignmentRelationShipTable()
    	{
    		table=new Table("AssignmentRelationShipTable");
    	}
		/**
		 * sets up the refrecnes in this table
		 */
    	public void setupRefrences() {
    		ArrayList<Column> cols = new ArrayList<Column>();
    		guardID = new Reference("Guard ID", 10, 1, true,true,USERTABLE.ID, Reference.DeleteConstraint.DELETEAUTOMATICALLY);
    		lotNO = new Reference("Lot No.", 12, 2, true,true,PARKINGLOTTABLE.lotNumber, Reference.DeleteConstraint.DELETEAUTOMATICALLY);
    		
    		cols.add(guardID);
    		cols.add(lotNO);
    		
    		table.setUp( cols);
    	}
    }
    

    public static void instantiateAllTables(ServerCommunicator communicator) {
//    	DataFieldList list=new DataFieldList(TableCollection.USERTABLE.table);
//    	communicator.sendStuffToServer(null, TableCollection::doNothingOnError, new ModifyingDataBaseServerFunction(new TableCreationDataBaseFunction(list)));
//
//    	list=new DataFieldList(TableCollection.OWNERTABLE.table);
//    	communicator.sendStuffToServer(null, TableCollection::doNothingOnError, new ModifyingDataBaseServerFunction(new TableCreationDataBaseFunction(list)));
//
//    	list=new DataFieldList(TableCollection.PARKINGLOTTABLE.table);
//    	communicator.sendStuffToServer(null, TableCollection::doNothingOnError, new ModifyingDataBaseServerFunction(new TableCreationDataBaseFunction(list)));
//
//    	list=new DataFieldList(TableCollection.PARKINGPASSTABLE.table);
//    	communicator.sendStuffToServer(null, TableCollection::doNothingOnError, new ModifyingDataBaseServerFunction(new TableCreationDataBaseFunction(list)));
//
//    	list=new DataFieldList(TableCollection.TICKETTABLE.table);
//    	communicator.sendStuffToServer(null, TableCollection::doNothingOnError, new ModifyingDataBaseServerFunction(new TableCreationDataBaseFunction(list)));
//
//    	list=new DataFieldList(TableCollection.DISPUTETABLE.table);
//    	communicator.sendStuffToServer(null, TableCollection::doNothingOnError, new ModifyingDataBaseServerFunction(new TableCreationDataBaseFunction(list)));
//
//    	list=new DataFieldList(TableCollection.ASSIGNMENTS.table);
//     	communicator.sendStuffToServer(null, TableCollection::doNothingOnError, new ModifyingDataBaseServerFunction(new TableCreationDataBaseFunction(list)));
//
//    	list=new DataFieldList(TableCollection.VEHICLETABLE.table);
//    	communicator.sendStuffToServer(null, TableCollection::doNothingOnError, new ModifyingDataBaseServerFunction(new TableCreationDataBaseFunction(list)));
//
//    	list=new DataFieldList(TableCollection.WORKDAYS.table);
//    	communicator.sendStuffToServer(null, TableCollection::doNothingOnError, new ModifyingDataBaseServerFunction(new TableCreationDataBaseFunction(list)));
//
    }


	public static void doNothingOnError(ErrorData s) {
    	System.out.println(s.getMessage());
    }
    public static class WorkDaysTable
    {
    	public Table table;
    	public Reference guard;
    	public final Column monday;
    	public final Column tuesday;
    	public final Column wednesday;
    	public final Column thursday;
    	public final Column friday;
    	public final Column saturday;
    	public final Column sunday;
    	private final ArrayList<Column> cols;
    	
    	public WorkDaysTable()
    	{
    		cols = new ArrayList<Column>();
    		table=new Table("WorkDaysTable");
    		monday = new Column("Monday", 25, 2, true, false);
			monday.setUserInputBoxConstructor(SetterUpOfDataFieldInputManager.createDataInputBoxInputConstructor(TimeDurationInputBox.constructDataFieldInputBox()));

			tuesday = new Column("Tuesday", 25, 3, true, false);
			tuesday.setUserInputBoxConstructor(SetterUpOfDataFieldInputManager.createDataInputBoxInputConstructor(TimeDurationInputBox.constructDataFieldInputBox()));

			wednesday = new Column("Wednesday", 25, 4, true, false);
			wednesday.setUserInputBoxConstructor(SetterUpOfDataFieldInputManager.createDataInputBoxInputConstructor(TimeDurationInputBox.constructDataFieldInputBox()));

			thursday = new Column("Thursday", 25, 5, true, false);
			thursday.setUserInputBoxConstructor(SetterUpOfDataFieldInputManager.createDataInputBoxInputConstructor(TimeDurationInputBox.constructDataFieldInputBox()));

			friday = new Column("Friday", 25, 6, true, false);
			friday.setUserInputBoxConstructor(SetterUpOfDataFieldInputManager.createDataInputBoxInputConstructor(TimeDurationInputBox.constructDataFieldInputBox()));
			saturday = new Column("Saturday", 25, 7, true, false);
			saturday.setUserInputBoxConstructor(SetterUpOfDataFieldInputManager.createDataInputBoxInputConstructor(TimeDurationInputBox.constructDataFieldInputBox()));

			sunday = new Column("Sunday", 25, 8, true, false);
			sunday.setUserInputBoxConstructor(SetterUpOfDataFieldInputManager.createDataInputBoxInputConstructor(TimeDurationInputBox.constructDataFieldInputBox()));


			cols.add(monday);
    		cols.add(tuesday);
    		cols.add(wednesday);
    		cols.add(thursday);
    		cols.add(friday);
    		cols.add(saturday);
    		cols.add(sunday);

    	}
		/**
		 * sets up the refrecnes in this table
		 */
    	protected void setupRefrences() {
    		guard = new Reference("Guard ID", 8, 1, false, true,USERTABLE.ID, Reference.DeleteConstraint.DELETEAUTOMATICALLY);
    		cols.add(guard);
    		
    		table.setUp( cols);
    	}
    }
	/**
	 * a method which will get a table by its name.
	 * @param tableName the name of the table you want to get
	 * @return the table, with the same name as the inputed string
	 */
	protected static Table getTableByName(String tableName){
		for(Table table: tables) {
			if (table.getTitle().equals(tableName)) {
				return table;
			}
		}
		return null;
	}
	/**
	 * this is a currently unused method, which will get the Column in a table, that has the same tableName, columnName, and columnNumber
	 * @param tableName the name of the table that the column belongs to
	 * @param columnName the name of the column in the database
	 * @param columnNumber the number that the column has in the database
	 * @return the column with the same tableName, columnName, and columnNumber as the inputed parameters
	 * @throws IllegalArgumentException when it cannot find the column with matching parameters
	 */
	protected static Column getColumnWhichMatches(String tableName, String columnName, int columnNumber) throws  IllegalArgumentException{
    	Table found=getTableByName(tableName);
		if(found==null){
    		throw new IllegalArgumentException("error, the column serized did not belong to any table");
		}
		for(Column column: found.getColumns()){
    		if(column.getColumnName().equals(columnName)&&column.getColumnNumber()==columnNumber){
    			return column;
			}
		}
		throw new IllegalArgumentException("error, we could not find the column you wanted in the table spcified");
	}
	/**
	 * this is a class which sets up the refrecnes of the tables. its a dumb implementation i know, but doing it this way
	 * means that the tables do not need to be topoligically sorted when they are instantiated.
	 * @author brere
	 */
	private static class RefreceSetterUpper {
    	private RefreceSetterUpper(){
				USERTABLE.setupRefrences();
				WORKDAYS.setupRefrences();
				PARKINGPASSTABLE.setupRefrences();
				TICKETTABLE.setupRefrences();
				ASSIGNMENTS.setupRefrences();
				DISPUTETABLE.setupRefrences();
		}
	}

}
