package com.example.brere.ParkingLotOrganizationSystemAndroidExperiments.client.communicationToServer;

import android.content.Context;

import com.example.brere.ParkingLotOrganizationSystemAndroidExperiments.client.dataFieldStuff.TableCollection;
import com.example.brere.ParkingLotOrganizationSystemAndroidExperiments.client.systems.util.MessageDisplayer;
import com.example.brere.ParkingLotOrganizationSystemAndroidExperiments.shared.clientAndServerCommunicationSystems.SocketCommunicationConstructor;
import com.example.brere.ParkingLotOrganizationSystemAndroidExperiments.shared.sentObjects.ServerToClient.ClientsInstructionsForReceivingData;
import com.example.brere.ParkingLotOrganizationSystemAndroidExperiments.shared.sentObjects.ServerToClient.ErrorData;
import com.example.brere.ParkingLotOrganizationSystemAndroidExperiments.shared.sentObjects.ServerToClient.MessageFromServerToClient;
import com.example.brere.ParkingLotOrganizationSystemAndroidExperiments.shared.sentObjects.clientToServer.ServerFunction;

import java.util.ArrayList;
import java.util.function.Consumer;

/**
 * this is a class which instantiates the entire Clients communication system
 * with the server. use the getSeverComunicator method to get the
 * serverComunicator. there are also a couple of static methods which are used
 * as the defualt Error Message Diplayers
 *
 * @author brere
 */

public class ClientsCommunicationSystem {
	/** The receiving thread, which is the thread that recieves messages from the server */
	private Thread receivingThread;
	/** The server comunicator, which is the thing that the client communicats to the server using */
	private ServerCommunicator comunicator;
	/**
	 * the socketCommunicationConstructor which constructs the sender/reciever
	 */
	private SocketCommunicationConstructor<ServerFunction<?>,MessageFromServerToClient> constructor;

	/**
	 * Instantiates a new clients communication system, using the context to display error messages if something
	 * goes wrong with instantiating or recieving
	 * @param  context the context used to display error messages when things go wrong
	 */
	public ClientsCommunicationSystem(Context context) {
		//note, if you want to change the id from 1000, you must also change the server side in server->FinalProjectServer

		 constructor=
			//	 /*hotspot*/			new SocketCommunicationConstructor<ServerFunction<?>,MessageFromServerToClient>("192.168.43.166",1001,createServerMessageDisplayer(context));

		/*school*/			//new SocketCommunicationConstructor<ServerFunction<?>,MessageFromServerToClient>("10.13.163.49",1001,createServerMessageDisplayer(activity));
	/*home*/			new SocketCommunicationConstructor<ServerFunction<?>,MessageFromServerToClient>("192.168.1.8",1001,createServerMessageDisplayer(context));
		ArrayList<ClientsInstructionsForReceivingData<?>> storage=new ArrayList<ClientsInstructionsForReceivingData<?>>();
		
		comunicator=new ServerCommunicator(storage,constructor.getSender(),createErrorDisplayer(context));
		
		constructor.getSocketReceiver().setConsumerOfOutput(new ReceiverOfMessagesFromTheServer(storage,createServerMessageDisplayer(context)));
		receivingThread=new Thread(constructor.getSocketReceiver());
		receivingThread.start();
		TableCollection.instantiateAllTables(comunicator);
	}

	/**
	 * creates an error displayer, which will display error messages with the context.
	 * @param context the context that will be used to display error messages
	 * @return creates an error displayer, which will display error messages with the context.
	 */
	private Consumer<ErrorData> createErrorDisplayer(Context context) {
		return (ErrorData data)-> createServerMessageDisplayer(context).accept(data.getMessage());
	}

	/**
	 * creates an error displayer, which will display error messages with the context.
	 * @param context the context that will be used to display error messages
	 * @return creates an error displayer, which will display error messages with the context.
	 */
	private Consumer<String> createServerMessageDisplayer(Context context){
		return (String s)-> MessageDisplayer.displayErrorMessage(context,s);
	}

	/**
	 * Gets the ServerComunicator of the system.
	 * @return the comunicator
	 */
	public ServerCommunicator getCommunicator() {
		return comunicator;
	}

	/**
	 * gets the constructor of the receiver and sender
	 * @return the constructor of the  reciever and sender
	 */
	public SocketCommunicationConstructor<ServerFunction<?>, MessageFromServerToClient> getConstructor() {
		return constructor;
	}
}
