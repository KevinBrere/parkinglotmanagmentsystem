package com.example.brere.ParkingLotOrganizationSystemAndroidExperiments.shared.sentObjects.ServerToClient;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * this is a interface that is used for the server sending messages to the client. Basically, everything which gets
 * sent from the server to the client must use this interface. it has only one method, which is printMessage, which will print
 * the message as either an error message, or a success message, depending on if it was an error or not.
 * @author brere
 */
public interface MessageFromServerToClient extends Serializable{
	/**
	 * this is the method which will either print the message as an error, or print the message as a success,
	 * and handle the instructions depending on what the Client wants done with the instructions.
	 * @param messages the place that the instructions are stored are stored
	 */
	public void printMessage(ArrayList<ClientsInstructionsForReceivingData<?>> messages);
}
