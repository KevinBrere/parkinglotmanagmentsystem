package com.example.brere.ParkingLotOrganizationSystemAndroidExperiments.shared.sentObjects.clientToServer.dataBaseFunctions;

import com.example.brere.ParkingLotOrganizationSystemAndroidExperiments.client.dataFieldStuff.DataField;
import com.example.brere.ParkingLotOrganizationSystemAndroidExperiments.client.dataFieldStuff.DataFieldList;
import com.example.brere.ParkingLotOrganizationSystemAndroidExperiments.client.dataFieldStuff.Reference;
import com.example.brere.ParkingLotOrganizationSystemAndroidExperiments.shared.sentObjects.ServerToClient.ErrorData;
import com.example.brere.ParkingLotOrganizationSystemAndroidExperiments.shared.stuffInTheServer.ServerError;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 * this is a class which represents two methods, one to check if the
 * dataFieldList is good usable in its operation and one to use the
 * dataFieldList in its operation on the database.
 *
 * the method check, checks if the inputed datafieldlist is good or not, and
 * This method is called automatically upon instantiation
 *
 * The method execute is to do whatever it needs to do in the database, and to
 * return the result in a DataFieldList.
 *
 * Execute REQUIRES to throw a ServerError when something expected goes wrong,
 * and for the message of the ServerError's message to be set to something
 * useful.
 *
 * @author brere
 *
 */
public abstract class DataBaseFunction implements Serializable {
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 2316426679347321848L;
	/** The data which will be used in the dataBaseFunction. */
	private DataFieldList data;
	/** The name of function. */
	private String nameOfFunction;

	/**
	 * this is the method that happens client side, when it checks that the
	 * dataFeildList meets the objects specifications.
	 *
	 * @param data            the list which will be checked.
	 */
	protected abstract void check(DataFieldList data);
	/**
	 * Instantiates a new data base function.
	 *
	 * @param data the data which will Inputed into the dataBaseFunction
	 * @param nameOfFunction the name of function, like delete, etc. This parameter
	 * should be filled by the subclass.
	 */
	public DataBaseFunction(DataFieldList data, String nameOfFunction) {
		super();
		this.data = data;
		check(data);
		this.nameOfFunction = nameOfFunction;
	}

	/**
	 * this is the method which the server directly calls. its basically executes
	 * the server function below, but it also has some nice error catching stuff
	 * embedded into it.
	 *
	 * @param jdbcConection the jdbc conection
	 * @return value from execute();
	 * @throws ServerError the server error
	 */
	public ArrayList<DataFieldList> executeDataBasefunction(Connection jdbcConection) throws ServerError {

		try {
			return execute(jdbcConection);
		} catch (Exception e) {
			ErrorData error = null;
			checkOneError(e);
			try {
				error = giveErrorMessage(e, jdbcConection);
			} catch (Exception e2) {
				checkOneError(e2);
			}
			if (error == null) {
				error = doStandardErrorProcedure(e);
			}
			throw new ServerError(error);
		}
	}
	/**
	 * Gets the name of function.
	 *
	 * @return the name of function
	 */
	public String getNameOfFunction() {
		return nameOfFunction;
	}
	/**
	 * Checks one error if it is a server error. throws it if it is
	 *
	 * @param e the e
	 * @throws ServerError the server error
	 */
	private void checkOneError(Exception e) throws ServerError {
		if (e instanceof ServerError) {
			throw (ServerError) e;
		}
	}

	/**
	 * this is the method which executes on the server side. The point of this is to
	 * use the connection to perform tasks on the database, It is mean to return the
	 * result of the opration, or throw an illegalArgumentException if Something
	 * Goes Wrong.
	 *
	 * @param jdbcConnection            the connection to the database
	 * @return the output of the search/delete/etc.
	 * @throws SQLException the SQL exception
	 * @throws ServerError             when there is a specified error happening
	 */
	protected abstract ArrayList<DataFieldList> execute(Connection jdbcConnection) throws SQLException, ServerError;

	/**
	 * This is a method that gets called when an error occurs. in that case, it
	 * should return the message you want to give to the user;
	 *
	 * @param e            the exception that occured
	 * @param jdbcConnection the jdbc connection
	 * @return the message that the user sees when the exception occurs.
	 * @throws SQLException the SQL exception
	 */
	protected abstract ErrorData giveErrorMessage(Exception e, Connection jdbcConnection) throws SQLException;
	/**
	 * Gets the data.
	 *
	 * @return the data
	 */
	public DataFieldList getData() {
		return data;
	}

	/**
	 * this checks all of the refrences in the list to see if there are any
	 * refrential integraty violations. This is in here for sharing of code reasons
	 *
	 * @param jdbcConnection
	 *            the connection which it checks with
	 * @param table
	 *            the list that it checks if its members have any violations
	 * @return NULL IF THERE ARE NO VIOLATIONS, and the errorMessage if there are
	 *         violations.
	 * @throws SQLException
	 *             if there was an SQL error while checking the refrences (in
	 *             search)
	 */
	protected ErrorData checkRefrencesInThisList(Connection jdbcConnection, DataFieldList table) throws SQLException {
		DataFieldList refrences = table.getAllRefrences();
		ArrayList<String> errorMessages = new ArrayList<String>();

		for (int i = 0; i < refrences.size(); i++) {
			if (refrences.get(i).getColumn().isNonNull()) {
				DataFieldList keys = table.getPrimaryKeyOfRefrencedTable((Reference) refrences.get(i).getColumn());
				SearchDataBaseFunction search = new SearchDataBaseFunction(keys);
				ArrayList<DataFieldList> searchResult = search.execute(jdbcConnection);
				if (searchResult.isEmpty()) {
					errorMessages.add("your " + refrences.get(i).getColumn().getColumnName() + " value of "
							+ refrences.get(i).getData() + " did not match any known " + keys.getTable().getTitle());
				}
			}
		}
		if (!errorMessages.isEmpty()) {
			return new ErrorData(generateErrorMessage(errorMessages),ErrorData.ErrorType.REFRENCEERROR);
		}
		return null;
	}
	/**
	 * generates a large error message based on a list of smaller error messages.
	 *
	 * @param errorMessages            the smaller error message that the large error message will be
	 *            based on
	 * @return the large error message
	 */
	protected String generateErrorMessage(ArrayList<String> errorMessages) {
		String errorMessage = "We have found " + errorMessages.size() + " error(s) with the " + nameOfFunction
				+ " \r\n";

		for (int i = 0; i < errorMessages.size(); i++) {
			errorMessage += errorMessages.get(i) + "\r\n";
		}
		System.out.println(errorMessage);
		return errorMessage;
	}

	/**
	 * NOTE: WILL NOT WORK WITH WEAK ENTITIS/MULTI KEYS YET The methoth checs if
	 * there are any refrences refering to the list, that have the "stop" deletion
	 * attribute. it gives an errror message describing the error if there are, and
	 * null if there are none.
	 *
	 * @param jdbcConnection            the connection
	 * @param data2            the data list which will be chekced
	 * @return NULL IF THERE ARE NO VIOLATIONS, an error message if there are
	 * @throws SQLException the SQL exception
	 */
	//TODO NEEDS TO BE CHANGED TO SUPPORT WEAK ENTITIES/MULTI KEYS
		public ErrorData checkRefrencesTothisList(Connection jdbcConnection, DataFieldList data2) throws SQLException {
		ArrayList<Reference> refrences = data2.getTable().getRefrencesToThisTable();

		ArrayList<String> errorMessages = new ArrayList<String>();

		for (Reference ref : refrences) {

			if (ref.getDeletionConstraint() == Reference.DeleteConstraint.STOPUSER) {
				String data = data2.getKeys().get(0).getData();
				DataFieldList list = new DataFieldList(ref.getTableWhichItBelongsTo());
				list.add(new DataField(ref, data));

				SearchDataBaseFunction searcher = new SearchDataBaseFunction(list);

				ArrayList<DataFieldList> searchResult = searcher.execute(jdbcConnection);
				if (!searchResult.isEmpty()) {
					errorMessages.add("there is a " + ref.getRefrenceTable().getTitle() + " which has a "
							+ ref.getColumnName() + " which refers to what you are trying to delete"
							+ " please change that and try again after");
				}

			}
		}

		if (!errorMessages.isEmpty()) {
			return (new ErrorData(generateErrorMessage(errorMessages),ErrorData.ErrorType.REFRENCEERROR));
		} else {
			return null;
		}
	}
	/**
	 * returns a comparison statement for the data in d, for a prepared statement.
	 * like if the data is null, then it retuns "DataBaseColumnName IS ?", otherwise
	 * it returns "DataBaseColumnName = ?"
	 *
	 * @param d
	 *            the data that the comparison string will be based on
	 * @return a string which that compares the dataField columnName to its data in
	 *         the correct fashion
	 */
	protected String createOneSearchComparer(DataField d) {
		String operator = "= ?";
		if (d.getData() == null) {
			operator = " IS ?";
		}
		return d.getColumn().getDataBaseColumnName() + operator;
	}
	/**
	 * this method constructs a standard error message for an exception with an
	 * unknown cause. this is used when no other, more sophisticated methods worked,
	 * ie we dont know what the error is.
	 *
	 * @param e
	 *            the exception that was thrown, that we dont know what caused it
	 * @return an ErrorData which Coresponds to the exception being thrown
	 */
	protected ErrorData doStandardErrorProcedure(Exception e) {
		String error = "an unknown error has ocured with our " + nameOfFunction
				+ ". please contact support for more information, \r\n error code: " + e.getMessage();

		error += "\r\n error details: " + e.getLocalizedMessage();
		if (e instanceof SQLException) {
			error += "\r\n" + handleSqlException((SQLException) e, false, "");
		}
		
		return new ErrorData(error,ErrorData.ErrorType.UNKOWN);
	}

	/**
	 * this is a method I made to handle an sql exception. It prints all of the
	 * "nextExceptions" using e.printmessage. i tried to make it so that it does not
	 * give every detail to the user, but this can easily be changed.
	 *
	 * @param e
	 *            the error which will be inputed
	 * @param toprint
	 *            whether it should be printed or not
	 * @param s
	 *            the string which it will keep appending. put "" if you have doubts
	 * @return the message which will be returned to the user.
	 */
	private String handleSqlException(SQLException e, boolean toprint, String s) {

		if (e == null) {
			return s;
		}
		if (toprint) {
			e.printStackTrace();
		} else {
			s += "sql state: " + e.getSQLState();
		}

		return handleSqlException(e.getNextException(), true, s);
	}
}