package com.example.brere.ParkingLotOrganizationSystemAndroidExperiments.client.systems.util.insertionSystem.individualColumnStuff;

import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.example.brere.ParkingLotOrganizationSystemAndroidExperiments.R;
import com.example.brere.ParkingLotOrganizationSystemAndroidExperiments.client.dataFieldStuff.DataField;
import com.example.brere.ParkingLotOrganizationSystemAndroidExperiments.client.dataFieldStuff.DataFieldList;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * this is a DataInputBox which uses a spinner to read the users input. A spinner, is basically a
 * drop down menu. So the user will use a drop down menu to enter the data.
 */
public class SpinnerDataInputBox extends DataInputBox {
	/**
	 * this method creates a DataInputBoxConstructor which constructs a SpinnerDataInputBox which
	 * uses all of the strings in spinnerOptions as possible user inputs.
	 * @param spinnerOptions all of the possible user inputs
	 * @return a DataInputBoxConstructor which constru ts a SpinnerDataInputBox that uses the strings
	 * in spinnerOptions as possible user inputs
	 */
	public static DataInputBoxConstructor constructDataFieldInputBox(String... spinnerOptions) {
		return new DataInputBoxConstructor() {
			private static final long serialVersionUID = 22401L;
			@Override
			public DataInputBox constructDataInputBox(DataField d, DataFieldList listFieldBelongsTo, @NonNull ViewGroup parent, int viewType) {

				ArrayList<String> arrayList = new ArrayList<>(Arrays.asList(spinnerOptions));
				View v = LayoutInflater.from(parent.getContext())
						.inflate(R.layout.spinner_input_box, parent, false);

				return new SpinnerDataInputBox(v,d,listFieldBelongsTo, arrayList);
			}
		};
	}
	/** serial id*/
	private static final long serialVersionUID = 8663142290635324307L;
	/** the spinner the user uses to make inputs*/
	private Spinner spinner;
	/** the adapter the spinner uses to display strings */
	private ArrayAdapter<StringWithValue> adapter;
	/**the possible values that the user can enter */
	private ArrayList<StringWithValue> values;

	/**
	 * creates a new SpinnerDataInputBox which the user can interact with
	 * @param view the view that the SpinnerDataInputBox is displayed on
	 * @param data the data the spinnerDataInputBox is displaying
	 * @param listDataBelongsTo the list that the spinnerDataInputBox belongs to
	 * @param options the possible inputs that the user can input
	 */
	private SpinnerDataInputBox(View view, DataField data, DataFieldList listDataBelongsTo, ArrayList<String> options) {
		super(view,data,listDataBelongsTo);
		spinner=view.findViewById(R.id.spinnerInputBoxSpinner);
		values=new ArrayList<>();
		String noEntryString="please enter a value";
		if(!data.getColumn().isNonNull()){
			noEntryString="none";
		}
		values.add(new StringWithValue(noEntryString, null));
		for(String option: options){
			values.add(new StringWithValue(option,option));
		}
		adapter=new ArrayAdapter<>(view.getContext(),R.layout.support_simple_spinner_dropdown_item,values);
		adapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);

		spinner.setAdapter(adapter);
	}

	@Override
	public String get() {
		return ((StringWithValue)spinner.getSelectedItem()).value;
	}

	@Override
	public void setEditable(boolean edit) {
		spinner.setEnabled(edit);
	}

	@Override
	protected void setUsersInputText(String s) {
		for(StringWithValue next: values){
			if(s.equals(next.value)){
				spinner.setSelection(adapter.getPosition(next));
				return;
			}
		}
		throw new IllegalArgumentException("error, you tried to set a text of a spinnerinputbox which was impossible" +
				"as nothing matched the string value");
		}


	@Override
	public void setTextInvisable() {
		spinner.setSelection(0);
	}

	/**
	 * this is a simple class for displaying strings. it is what allows the spinner to say "please enter a value"
	 * without entering "please enter a value" into the database if it is still active
	 */
	private class StringWithValue{
		/** the string the user sees*/
		 String viewed;
		 /** the actual value that gets inputed into the database */
		 String value;

		/**
		 * creates a stringWithValue object
		 * @param viewed the string the user sees
		 * @param value the string that gets inputed into the database
		 */
		 StringWithValue(String viewed,String value) {
			this.viewed = viewed;
			this.value=value;
		}

		@Override
		public String toString() {
			return  viewed ;
		}
	}


}
