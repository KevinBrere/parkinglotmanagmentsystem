package com.example.brere.ParkingLotOrganizationSystemAndroidExperiments.client.systems;


import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;

import com.example.brere.ParkingLotOrganizationSystemAndroidExperiments.client.communicationToServer.ServerCommunicationService;
import com.example.brere.ParkingLotOrganizationSystemAndroidExperiments.client.communicationToServer.ServerCommunicator;
import com.example.brere.ParkingLotOrganizationSystemAndroidExperiments.client.dataFieldStuff.DataFieldList;
import com.example.brere.ParkingLotOrganizationSystemAndroidExperiments.client.dataFieldStuff.TableCollection;
import com.example.brere.ParkingLotOrganizationSystemAndroidExperiments.pretend.ControlSystemsActivity;


/**
 * this is a class which basically is an activity with a title, which is automatically connected to the server
 * through the serverCommunicationService. This is mostly here to support the ControlSystemHanleler
 */
public abstract class ControlSystem extends Fragment {
	/**
	 * static function which creates a bundle for any control system, parsing the bundle in a way
	 * specific to this controlSystem
	 * @param title the title of the system.
	 * @return A bundle which will instantiate this layer of the system correctly
	 */
	protected static Bundle createBundleForSystem(String title) {
		Bundle b = new Bundle();
		b.putString(ControlSystem.CONTROLSYSTEMTITLE, title);
		return b;
	}
	/** the data of the user. this is instantiated in LogInSystem*/
	private static DataFieldList userData;
	/**constant for use in bundle */
	public static final String CONTROLSYSTEMTITLE = "Pretendfinal.pretend.Controlsystem.title";
	/**
	 * just the name of the system
	 */
	private String title;


	@Override
	public void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		title = getActivity().getIntent().getExtras().getString(CONTROLSYSTEMTITLE);

	}


	@Override
	public void onSaveInstanceState(@NonNull Bundle savedInstanceState) {

		if (savedInstanceState.getString(CONTROLSYSTEMTITLE) == null) {
			savedInstanceState.putString(CONTROLSYSTEMTITLE, title);
		}
		super.onSaveInstanceState(savedInstanceState);
	}

	/**
	 * get the title of the system
	 * @return the title of the system
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * get the users data.
	 * @return the users data
	 */
	protected static DataFieldList getUserData() {
		return userData;
	}

	/**
	 * set the user data, so that all of the control systems can see the new users data
	 * @param userData the userData which the data will be set to
	 * @throws IllegalArgumentException when the DataFieldList sent in was not for a user
	 */
	protected static void setUserData(DataFieldList userData) throws IllegalArgumentException{
		if(!userData.getTable().equals(TableCollection.USERTABLE.table)){
			throw new IllegalArgumentException("error, DataFieldList set was not that of a user");
		}
		ControlSystem.userData = userData;
	}

	/**
	 * get the serverCommunicator the system uses to communicate with the server
	 * @return the serverCommunicator the system uses to communicate with the server
	 */
	public ServerCommunicator getServ() {
		ControlSystemsActivity activity = (ControlSystemsActivity) getActivity();
		return activity.getServerCommunicatorService().getServerComunicator();
	}

}
