package com.example.brere.ParkingLotOrganizationSystemAndroidExperiments.client.systems.util;

import android.app.Application;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Handler;
import android.os.Looper;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.widget.Toast;

import java.util.LinkedList;
import java.util.Queue;
import java.util.function.Consumer;

/**
 * this is a class that has static functions which simply give simple error messages to the user
 */
public class MessageDisplayer {
	/** the handler which hanldes the error messages */
	private static final Handler executor = new Handler(Looper.getMainLooper());

	/**
	 * display a simple toast error message to the user
	 * @param context the context of the error
	 * @param message the message which will be displayed to the user
	 */
	public static void displayErrorMessage(Context context, String message) {

		executor.post(() -> {
			int duration = Toast.LENGTH_LONG;
			Toast toast = Toast.makeText(context, message, duration);
			toast.show();
		});

	}

	/**
	 * display a simple alert message message to the user
	 * @param  title the title of the alert message
	 * @param context the context of the error
	 * @param message the message which will be displayed to the user
	 */
	public static void displayAlertMessage(Context context, String title, String message) {

		executor.post(() -> {
			AlertDialog.Builder builder = new AlertDialog.Builder(context);

			builder.setTitle(title);

			builder.setMessage(message)
					.setCancelable(true)
					.setPositiveButton("OK", new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int id) {
							dialog.dismiss();
						}
					});

			AlertDialog alertDialog = builder.create();
			alertDialog.show();
		});

	}

	/**
	 * display a simple alert message message to the user
	 * @param context the context of the error
	 * @param message the message which will be displayed to the user
	 */
	public static void displayAlertMessage(Context context, String message) {
		displayAlertMessage(context, "", message);
	}

//	public static class Executor implements Runnable {
//		protected Queue<Runnable> messageQue;
//		private boolean isRunning;
//		private Consumer<Runnable> consumer;
//
//
//
//		public Executor() {
//
//			messageQue = new LinkedList<>();
//			isRunning = false;
//		}
//
//		@Override
//		public void run() {
//			isRunning = true;
//
//			try {
//				while (true) {
//					while (messageQue.isEmpty()) {
//						Thread.sleep(50);
//					}
//						consumer.accept(messageQue.remove());
//				}
//			} catch (InterruptedException e) {
//				Log.e("error", "the message displayer in MessageDisplayer was Interrupted");
//			}
//			isRunning = false;
//		}
//
//		public boolean isRunning() {
//			return isRunning;
//		}
//		public void setConsumer(Consumer<Runnable> consumer) {
//			this.consumer = consumer;
//		}
//	}
}


//package com.example.brere.ParkingLotOrganizationSystemAndroidExperiments.client.systems.util;
//
//		import android.app.Activity;
//		import android.app.Application;
//		import android.content.Context;
//		import android.content.DialogInterface;
//		import android.os.AsyncTask;
//		import android.os.Handler;
//		import android.os.Looper;
//		import android.support.v7.app.AlertDialog;
//		import android.util.Log;
//		import android.widget.Toast;
//
//		import com.example.brere.ParkingLotOrganizationSystemAndroidExperiments.shared.serverClientCommunicationFramework.serverMessageFromServerToClient.ErrorData;
//
//		import java.io.IOException;
//		import java.util.LinkedList;
//		import java.util.Queue;
//		import java.util.function.Consumer;
//
//public class MessageDisplayer {
//
//	public static void displayErrorMessage(Activity activity, String message) {
//
//		activity.runOnUiThread(() -> {
//			int duration = Toast.LENGTH_LONG;
//			Toast toast = Toast.makeText(activity, message, duration);
//			toast.show();
//		});
//
//	}
//
//
//	public static void displayAlertMessage(Activity activity, String title, String message) {
//		activity.runOnUiThread(() -> {
//			AlertDialog.Builder builder = new AlertDialog.Builder(activity);
//
//			builder.setTitle(title);
//
//			builder.setMessage(message)
//					.setCancelable(true)
//					.setPositiveButton("OK", new DialogInterface.OnClickListener() {
//						public void onClick(DialogInterface dialog, int id) {
//							dialog.dismiss();
//						}
//					});
//
//			AlertDialog alertDialog = builder.create();
//			alertDialog.show();
//		});
//
//	}
//
//
//	public static void displayAlertMessage(Activity activity, String message) {
//		displayAlertMessage(activity, "", message);
//	}
//
////	public static class Executor implements Runnable {
////		protected Queue<Runnable> messageQue;
////		private boolean isRunning;
////		private Consumer<Runnable> consumer;
////
////
////
////		public Executor() {
////
////			messageQue = new LinkedList<>();
////			isRunning = false;
////		}
////
////		@Override
////		public void run() {
////			isRunning = true;
////
////			try {
////				while (true) {
////					while (messageQue.isEmpty()) {
////						Thread.sleep(50);
////					}
////						consumer.accept(messageQue.remove());
////				}
////			} catch (InterruptedException e) {
////				Log.e("error", "the message displayer in MessageDisplayer was Interrupted");
////			}
////			isRunning = false;
////		}
////
////		public boolean isRunning() {
////			return isRunning;
////		}
////		public void setConsumer(Consumer<Runnable> consumer) {
////			this.consumer = consumer;
////		}
////	}
//}


