package com.example.brere.ParkingLotOrganizationSystemAndroidExperiments.client.systems.util.listSystem;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SearchView;

import com.example.brere.ParkingLotOrganizationSystemAndroidExperiments.client.dataFieldStuff.Column;
import com.example.brere.ParkingLotOrganizationSystemAndroidExperiments.client.dataFieldStuff.DataField;
import com.example.brere.ParkingLotOrganizationSystemAndroidExperiments.client.dataFieldStuff.DataFieldList;
import com.example.brere.ParkingLotOrganizationSystemAndroidExperiments.client.dataFieldStuff.Table;
import com.example.brere.ParkingLotOrganizationSystemAndroidExperiments.client.systems.ControlSystem;
import com.example.brere.ParkingLotOrganizationSystemAndroidExperiments.client.systems.util.MessageDisplayer;
import com.example.brere.ParkingLotOrganizationSystemAndroidExperiments.client.systems.util.insertionSystem.ModificationSystem;
import com.example.brere.ParkingLotOrganizationSystemAndroidExperiments.pretend.ControlSystemsActivity;
import com.example.brere.ParkingLotOrganizationSystemAndroidExperiments.shared.sentObjects.clientToServer.ModifyingDataBaseServerFunction;
import com.example.brere.ParkingLotOrganizationSystemAndroidExperiments.shared.sentObjects.clientToServer.dataBaseFunctions.SearchDataBaseFunction;

import java.util.ArrayList;

/**
 * this is a ControlSystem which searches for things in the database. Like the user can select what they want
 * to search for, and then search for that thing using this controlSystem
 */
public class ListSystem extends ControlSystem {
	private static final String SEARCHINGTABLE="ParkingLotOrganizationSystem.client.systems.util.ListSystem.ListSystem.DataFieldListForSearching";

	/**
	 * Creates a  bundle for this system for launching a new Listsystem object
	 * @param table the inputed table that the user is searching for
	 * @return a bundle to launch a new ListSystem activity
	 */
	public static Bundle createBundleForSystem(Table table) {
		Bundle b = ControlSystem.createBundleForSystem("search for "+table.getTitle());
		ControlSystemsActivity.CreateControlSystem instantiate = ListSystem::new;
		b.putSerializable(ControlSystemsActivity.CONTROLSYSTEMGENERATEORID, instantiate);
		b.putSerializable(ListSystem.SEARCHINGTABLE, table.getColumns());
		return b;
	}
	/**the listview which will display the ui to the user */
	private ListView view;
	@Override
	public void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		ArrayList<Column> columns = (ArrayList<Column>) getActivity().getIntent().getExtras().getSerializable(SEARCHINGTABLE);
		columns.removeIf(column -> column.isHiddenFromSearches() );
		view = new ListView(columns, this::startModificationSystem);
	}
	@Override
	public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,@Nullable Bundle savedInstanceState){
		View listView=view.createView(inflater,container,this.getContext());
		view.getSearchView().setOnQueryTextListener(new SearchView.OnQueryTextListener() {
			@Override
			public boolean onQueryTextSubmit(String s) {
				return ListSystem.this.query(s);
			}
			@Override
			public boolean onQueryTextChange(String s) {
				return true;
			}
		});
	return listView;
	}

	/**
	 * this method tells the server to search the database for the given string. It will search the
	 * database to see if anyting in the database has same string value in the column matching the
	 * spinners selected column. like if the spinner states "ID", then this method will tell the
	 * server to see if any users have a matching ID with the given string value
	 *
	 * after the server completes its operation, the onSearchSuccess method is activated. if there
	 * is an error, a simple toast message will tell the user what went wrong
	 *
	 * it also checks the user input to see if it is a valid input
	 * @param s the string value that is being searched for
	 * @return if the search operation was fully handled (either by giving an error message, or by
	 * search operation completing.
	 */
	private boolean query(String s) {
		Column selected=(Column)view.getSpinner().getSelectedItem();
		if (selected==null){
			return false;
		}
		if(s!=null){
			s=s.trim();
			if(s.equals("")){
				s=null;
			}
		}
		try{
			selected.checkUserInput(s);
		}catch (IllegalArgumentException e){
			MessageDisplayer.displayErrorMessage(this.getContext(),e.getMessage());
			return true;
		}
		DataFieldList toQuery=new DataFieldList(selected.getTableWhichItBelongsTo());
		toQuery.add(new DataField(selected,s));

		super.getServ().sendStuffToServer(this::onSearchSuccess,null,new ModifyingDataBaseServerFunction(new SearchDataBaseFunction(toQuery)));
		return true;
	}

	/**
	 * when the server completes a successful search, it will activate this method
	 * @param lists the results of the successful search
	 */
	private void onSearchSuccess(ArrayList<DataFieldList> lists) {
		if(lists.isEmpty()){
			MessageDisplayer.displayErrorMessage(this.getContext(),"error, we could not find anything which matched your search");
			return;
		}
		for(int i=0;i<lists.size();i++){
			lists.set(i,lists.get(i).getListWithoutHiddenItems());
		}
		view.getAdapter().setDataList(lists);
	}

	/**
	 * this is a method which will start a modificationSystem based on the inputedList.
	 * @param list the data of the new modificationSystem which will be started
	 */
	public void startModificationSystem(DataFieldList list){
			Intent intent = new Intent(this.getActivity(), ControlSystemsActivity.class);
			intent.putExtras(ModificationSystem.createBundleForSystem(list));
			startActivity(intent);
	}
}
