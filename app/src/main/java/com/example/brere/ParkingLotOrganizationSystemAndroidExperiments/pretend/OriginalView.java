package com.example.brere.ParkingLotOrganizationSystemAndroidExperiments.pretend;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.example.brere.ParkingLotOrganizationSystemAndroidExperiments.R;
import com.example.brere.ParkingLotOrganizationSystemAndroidExperiments.client.systems.ControlSystem;

import java.util.ArrayList;

/**
 *this is a view, which represents a thing that is being shown to a user. ie its bascially a panel in the gui.
 *
 *
 *
 * REQUIRES IN INTENT BUNDLE
 *  String ControlSystem.CONTROLSYSTEMTITLE for title of system
 *  ControlSystemsActivity.CreateControlSystem CONTROLSYSTEMGENERATEORID which must be OriginalView::new
 *  ListenerMethod  OriginalView.ORIGINALVIEWLISTENER for what to do with the list
 *
 *
 */
public class OriginalView extends ControlSystem {

	private static final long serialVersionUID = 536512060081878929L;
	public static final String ORIGINALVIEWLISTENER="PretendFinal4.pretend.originalview.listener";
	private int i;

	/**
	 *
	 this is the main instantiator for the bundle if you want to use this ControlSystem. If you launch a
	 * ControlSystemsActivity using this, it will work
	 * @param title the title of the system
	 * @param meth the method which the system will use to add stuff to the list
	 * @return a bundle which you will be albe to launch the activity using Intent.putAll with
	 */
	public static Bundle createBundleForSystem(@NonNull String title, @NonNull ListenerMethod meth){
		Bundle b=new Bundle();
		b.putString(ControlSystem.CONTROLSYSTEMTITLE,title);

		b.putSerializable(ORIGINALVIEWLISTENER,meth);
		ControlSystemsActivity.CreateControlSystem instantiate=OriginalView::new;
		b.putSerializable(ControlSystemsActivity.CONTROLSYSTEMGENERATEORID,instantiate);


		return b;
	}


	public OriginalView(){
		super();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
							 Bundle savedInstanceState) {
		i=0;

		View rootView=inflater.inflate(R.layout.recyclier_vew_with_button,container,false);

		Button but=rootView.findViewById(R.id.buttonForMeowing);
		but.setText(this.getTitle());

		ArrayList<String> strings=new ArrayList<String>();
		KevinsFirstRecyclerViewAdapter adapt=new KevinsFirstRecyclerViewAdapter(strings);

		Bundle  intentsBundle=getActivity().getIntent().getExtras();

		ListenerMethod meth=(ListenerMethod)intentsBundle.getSerializable(ORIGINALVIEWLISTENER);

		RecyclerView rec=rootView.findViewById(R.id.recyclierForMeowing);

		rec.setLayoutManager(new LinearLayoutManager(this.getActivity().getApplicationContext()));
		rec.setAdapter(adapt);



		but.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				i++;
				meth.execute(adapt);
				if(i>5){
					startActivity(getActivity().getIntent());

				}
			}
		});
		return rootView;
	}
}
