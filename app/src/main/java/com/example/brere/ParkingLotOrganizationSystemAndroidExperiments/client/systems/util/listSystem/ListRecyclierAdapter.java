package com.example.brere.ParkingLotOrganizationSystemAndroidExperiments.client.systems.util.listSystem;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.brere.ParkingLotOrganizationSystemAndroidExperiments.R;
import com.example.brere.ParkingLotOrganizationSystemAndroidExperiments.client.dataFieldStuff.DataField;
import com.example.brere.ParkingLotOrganizationSystemAndroidExperiments.client.dataFieldStuff.DataFieldList;

import java.util.ArrayList;
import java.util.function.Consumer;

/**
 * this is a class for displaying search results when the user is searching for data in the database
 */
public class ListRecyclierAdapter extends RecyclerView.Adapter<ListRecyclierAdapter.DataFieldListDisplayer> {

	/**
	 * This is an ArrayList which has all of the search results in it
	 */
	private ArrayList<DataFieldList> dataFieldLists;
	/** this is the Consumer<DataFieldList> which will be activated each time that the user clicks on a search result */
	private Consumer<DataFieldList> displayerConsumer;

	/**
	 * creates a new ListRecyclierAdapter
	 * @param lists the search results
	 * @param dataFieldListConsumer the action that will be taken whenever a search result is clicked on
	 */
	public ListRecyclierAdapter(ArrayList<DataFieldList> lists, Consumer<DataFieldList> dataFieldListConsumer) {
		this.dataFieldLists = lists;
		this.displayerConsumer = dataFieldListConsumer;
	}

	@NonNull
	@Override
	public DataFieldListDisplayer onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

		View v = LayoutInflater.from(parent.getContext())
				.inflate(R.layout.search_result_layout, parent, false);
		DataFieldListDisplayer displayer = new DataFieldListDisplayer(v);

		v.setOnClickListener((View v2) -> displayerConsumer.accept(displayer.data));

		return displayer;
	}

	@Override
	public void onBindViewHolder(@NonNull DataFieldListDisplayer holder, int position) {
		holder.setData(dataFieldLists.get(position));
	}

	/**
	 * sets the search results that are displayed to the user
	 * @param dataFieldLists the search results that are displayed to the user
	 */
	public void setDataList(ArrayList<DataFieldList> dataFieldLists) {
		this.dataFieldLists = dataFieldLists;
		this.notifyDataSetChanged();
	}
	@Override
	public int getItemCount() {
		return dataFieldLists.size();
	}

	/**
	 * this class Displays the search results in a text box
	 */

	 static class DataFieldListDisplayer extends RecyclerView.ViewHolder {
	 	/** the main Field that it displays the text in (largest) */
		private TextView mainField;
		/** the smaller field that it displays the text in */
		private TextView subField;
		/** the data that the holder uses */
		private DataFieldList data;

		/**
		 * creates a new DataFieldListDisplayer
		 * @param itemView the view that the user sees
		 */
		private DataFieldListDisplayer(@NonNull View itemView) {
			super(itemView);
			mainField = itemView.findViewById(R.id.SearchResultMainText);
			subField = itemView.findViewById(R.id.SearchResultSubText);

		}

		/**
		 * sets the data that is being displayed to the user, and the saved data in the object
		 * @param d the dataFieldList that will be displayed to the user
		 */
		private void setData(DataFieldList d) {
			this.data = d;
			DataFieldList keys = d.getKeys();
			DataFieldList dataCopy = new DataFieldList(d);
			setupAField(mainField, keys, dataCopy);
			setupAField(subField, keys, dataCopy);
		}

		/**
		 * sets up a TextView so that the most important data in the list is being displayed
		 * @param field the TextView that will display the most important data
		 * @param keys the keys in the list
		 * @param dataCopy a copy of the oririginal dataFieldList. its data will be displayed in
		 *                 the textField
		 */
		private void setupAField(TextView field, DataFieldList keys, DataFieldList dataCopy) {
			if (!keys.isEmpty()) {
				field.setText(generateStringFromDataField(keys.get(0)));
				dataCopy.remove(keys.get(0).getColumn());

				keys.remove(0);
			} else {
				field.setText(generateStringFromDataField(dataCopy.get(0)));
				dataCopy.remove(0);
			}
		}
		/**parses the data from the dataField into a readable string, which the user will see
		 * @param dataField the data which will be turned into a readable string. */
		private String generateStringFromDataField(DataField dataField) {
			return dataField.getColumn().getColumnName() + ": " + dataField.getData();
		}
	}
}
