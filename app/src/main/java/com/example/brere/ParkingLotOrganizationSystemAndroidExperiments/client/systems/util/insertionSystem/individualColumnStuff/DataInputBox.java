package com.example.brere.ParkingLotOrganizationSystemAndroidExperiments.client.systems.util.insertionSystem.individualColumnStuff;


import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;

import com.example.brere.ParkingLotOrganizationSystemAndroidExperiments.client.dataFieldStuff.DataField;
import com.example.brere.ParkingLotOrganizationSystemAndroidExperiments.client.dataFieldStuff.DataFieldList;

import java.io.Serializable;
import java.util.function.Supplier;

/**
 * this is an abstract class for anything that the user uses to input data into the database. Like say
 * you want the user to input a ID, what you use to enter an id must extend DataInputBox.
 */
public abstract class DataInputBox extends RecyclerView.ViewHolder implements Supplier<String> {
	/**
	 * this is a functional interface for methods/classes that construct dataInputBoxes. These are used
	 * in UserInputSystem to construct the DataInputBoxes used to enter things into the database.
	 */
	public interface DataInputBoxConstructor extends Serializable{
		/**
		 * construct The dataInputBox for the data Inputed
		 * @param d the data that will be displayed in the dataInputBox
		 * @param listFieldBelongsTo the List that d belongs to
		 * @param parent the parent view that will make the Data
		 * @param viewType the index that the view has in the recyclerView
		 * @return a DataInputBox that the user can use to input the Data In d with.
		 */
			public  DataInputBox constructDataInputBox(DataField d,DataFieldList listFieldBelongsTo, @NonNull ViewGroup parent, int viewType);
	}

	/**
	 * the data that the DataInputBox holds
	 */
	private DataField data;
	/**
	 * the list that the DataField belongs to
	 */
	private DataFieldList listFieldBelongsTo;

	/**
	 * 	 * this is a  constcutor for datafield input box, which is basically an input box for the user
	 * to input 1 peice of data in. like the id, or the date, etc.
	 * @param view the view that the dataInputBox is.
	 * @param data the inputed field which it gets data on the column from, and will insert data into.
	 * @param listFieldBelongsTo the dataFieldList that the Data belongs to
	 */
	public DataInputBox(View view, DataField data,DataFieldList listFieldBelongsTo) {
		super(view);
		this.data=data;
		this.listFieldBelongsTo=listFieldBelongsTo;
	}

	/**
	 * gets the list that the data belongs to
	 * @return the DataFieldList that the data belongs to
	 */
	public DataFieldList getListFieldBelongsTo() {
		return listFieldBelongsTo;
	}

	/**
	 * gets the DataField that this DataInputBox is displaying
	 * @return the DataField that the DataInputBox is displaying
	 */
	public DataField getDataField() {
		return data;
	}

	/**
	 * sets the dataInput box to be editable/uneditable
	 * @param edit is it to be editable or uneditable
	 */
	public abstract void setEditable(boolean edit);

	/**
	 * set the text that the user inputs to the value. like if you want to have the input box to
	 * display a string value before the user inputs anything, this method would be called.
	 * @param s the string value that will be inputed into where the user inputs things
	 */
	public final void setText(String s){
		if(s==null){
			setTextInvisable();
			return;
		}
		setUsersInputText(s);
	}

	/**
	 * set the text that the user inputs to the value. like if you want to have the input box to
	 * display a string value before the user inputs anything, this method would be called.
	 * NOTE: you dont have to worry about null values, since setText already handles the null value
	 * @param s the string value that will be inputed into where the user inputs things
	 */
	protected abstract void setUsersInputText(String s);
	/**
	 * sets the users input invisable. like they cannot see it anymore
	 */
	public abstract void setTextInvisable();

	/**
	 * sets the text that is stored in the DataField visable. like if you stored "I LIKE GRAPES" in
	 * the DataField, then it would say "I LIKE GRAPES" in the DataInputBox
	 */
	public void setTextVisable() {
		String set=data.getData();
		if(set!=null) {
			set=set.trim();
		}
		this.setText(set);
	}





}
