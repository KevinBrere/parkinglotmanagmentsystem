package  com.example.brere.ParkingLotOrganizationSystemAndroidExperiments.client.dataFieldStuff;


import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
/**
 * The Interface CheckerOfUserInput, which is an interface for methods which check if a string is
 * an acceptable input for the given column. you should pass these methods using method refrences
 * to make sure the user inputs correct things into the database
 *
 * REQUIRES THE METHOD TO THROW AN ILLEGALARGUMENTEXCEPTION IF THE STRING IS NOT ACCEPTABLE
 */
public  interface CheckerOfUserInput extends Serializable {
	/**
	 * Check user input to see if it is a valid input
	 * @param s the user input that is being checked
	 * @param col the column that the user input belongs to
	 * @throws IllegalArgumentException when the user input was not valid
	 */
	public void checkInput(String s, Column col);
	/**
	 * returns a CheckerOfUserInput that checks if the string is null illegally.
	 * this is automatically added to each newly instaniated column object.
	 * @return a CheckerOfUserInput that checks if the string is null illegally.
	 */

	public static CheckerOfUserInput checkIfColumnIsIllegallyNull()

	{
		return new CheckerOfUserInput() {
			private static final long serialVersionUID = 3581919710094589888L;

			@Override
			public void checkInput(String s, Column col) {
				if (s == null) {
					if (col.isNonNull()) {
						throw new IllegalArgumentException(" error, please fill out " + col.getColumnName());
					}
				}
			}
		};
	}

	/**
	 * returns a checker which checks if the String input is too big.
	 * This is automatically added to each column object
	 * @return   a checker which checks if the String input is too big.
	 */
	public static CheckerOfUserInput checkIfInputIsTooBig() {
		return new CheckerOfUserInput() {
			private static final long serialVersionUID = 3569919710094589888L;

			@Override
			public void checkInput(String s, Column col) {
				if (s.length() > col.getMaxSize()) {
					throw new IllegalArgumentException("error, your " + col.getColumnName() + " input is too big, "
							+ "\r\n please make sure it is less than " + col.getMaxSize() + " long");
				}
			}
		};
	}
/**
 * returns a checker that checks if input is A double (like a number with a decimal).
 * @return a checker that checks if input is A double (like a number with a decimal).
 */
	public static CheckerOfUserInput checkIfColumnIsADouble() {
		return new CheckerOfUserInput() {
			private static final long serialVersionUID = 3589919710994589888L;

			@Override
			public void checkInput(String s, Column col) {
				try {
					Double.parseDouble(s);
				} catch (NumberFormatException nfe) {
					throw new IllegalArgumentException("error, your input for " + col.getColumnName() + " is not a number. please enter a number");
				}

			}
		};


	}
	/**
	 * returns a checker that checks if the input is an integer.
	 * @return a checker that checks if the input is an integer.
	 */
	public static CheckerOfUserInput checkIfColumnIsAnInteger() {

		return new CheckerOfUserInput() {
			private static final long serialVersionUID = 3589919710094580888L;

			@Override
			public void checkInput(String s, Column col) {
				try {
					Integer.parseInt(s);
				} catch (NumberFormatException nfe) {
					throw new IllegalArgumentException("error, your input for " + col.getColumnName() + " is not a valid number. Please enter a number");
				}
			}
		};
	}
	/**
	 * returns a checker that checks if the input is a valid email.
	 * @return a checker that checks if the input is a valid email.
	 */
	public static CheckerOfUserInput checkIfColumnIsAValidEmail() {
		return new CheckerOfUserInput() {
			private static final long serialVersionUID = 3589919730094589888L;

			@Override
			public void checkInput(String s, Column col) {

				//code skillfully borrowed from https://stackoverflow.com/questions/624581/what-is-the-best-java-email-address-validation-method

				Pattern pattern = Pattern.compile("^.+@.+(\\.[^\\.]+)+$");
				Matcher matcher = pattern.matcher(s);
				if (!matcher.matches()) {
					throw new IllegalArgumentException("error, please input a valid email for " + col.getColumnName());
				}
			}
		};
	}
	/**
	 * returns a checker that checks if the input has only Letters (no numbers, spaces, etc).
	 * @return a checker that checks if the input has only Letters (no numbers, spaces, etc).
	 */
	public static CheckerOfUserInput checkIfColumnIsOnlyLetters() {
		return new CheckerOfUserInput() {
			private static final long serialVersionUID = 3589919710494589888L;

			@Override
			public void checkInput(String s, Column col) {
				if (!s.matches("[a-zA-Z]+")) {
					throw new IllegalArgumentException("Error, you must only input letters for your " + col.getColumnName());
				}
			}
		};
	}
	/**
	 * returns a checker that checks if the input matches any of the strings in the list.
	 * @return a checker that checks if the input matches any of the strings in the list.
	 */
	public static CheckerOfUserInput createCheckerForValues(String[] list) {
		return new CheckerOfUserInput() {
			private static final long serialVersionUID = 3589915710094580888L;

			@Override
			public void checkInput(String s, Column col) {
				boolean found = false;
				for (int i = 0; i < list.length; i++) {
					if (s.equals(list[i])) {
						found = true;
					}
				}
				if (!found) {
					String error = "error, your " + col.getColumnName() + " value of " + s + " did not match any valid value \r\n"
							+ "please make sure to enter " + generateEnterableValues(s, col, list) + " as its value";

					throw new IllegalArgumentException(error);
				}
			}
		};
	}
	/**
	 * a helper function for createCheckerForValues. it simply creates an error messsage for the
	 * user input
	 * @param  s the user input that was not valid
	 * @param  col the column that the user input belonged to
	 * @param  list the possible valid user inputs
	 * @return an error message telling the user what went wrong
	 */
	 static String generateEnterableValues(String s, Column col, String[] list) {
		if (list.length == 0) {
			throw new IllegalArgumentException("error, the programmer entered an empty list to check values against");
		}
		String returned = list[0];
		for (int i = 1; i < list.length; i++) {
			returned += " or " + list[i];
		}
		if (!col.isNonNull()) {
			returned += " or nothing";
		}
		return returned;
	}
	/**
	 * returns a checker that checks if the input is exactly the max size of the column
	 * @return a checker that checks if the input is exactly the max size of the column.
	 */
	public static CheckerOfUserInput checkIfColumnIsExactlyMaxSize() {
		return new CheckerOfUserInput() {
			private static final long serialVersionUID = 3584919710094580888L;

			@Override
			public void checkInput(String s, Column col) {
				if (s.length() != col.getMaxSize()) {
					throw new IllegalArgumentException("Error, your " + col.getColumnName() + " must be exaclty " + col.getMaxSize() + " characters long");
				}
			}
		};
	}

	/**
	 * returns a checker that checks if the input is a valid phone number, with the XXX-XXX-XXXX form
	 * @return a checker that checks if the input is a valid phone number, with the XXX-XXX-XXXX form
	 */
	public static CheckerOfUserInput checkIfColumnIsValidPhoneNumber() {
		return new CheckerOfUserInput() {
			private static final long serialVersionUID = 3589919710094580818L;

			@Override
			public void checkInput(String s, Column col) {
				Pattern pattern = Pattern.compile("\\d{3}-\\d{3}-\\d{4}");
				Matcher matcher = pattern.matcher(s);
				if (!matcher.matches()) {
					throw new IllegalArgumentException("error, make sure your " + col.getColumnName() + " matches the following pattern \r\n "
							+ "XXX-XXX-XXXX with the dashes");
				}
			}
		};
	}

	/**
	 * returns a checker that checks if the input is a valid Date, with the MM/dd/yyyy format
	 * @return a checker that checks if the input is a valid Date, with the MM/dd/yyyy format
	 */
	//based on https://stackoverflow.com/questions/33968333/how-to-check-if-a-string-is-date
	public static CheckerOfUserInput checkIfColumnIsValidDate() {
		return new CheckerOfUserInput() {
			private static final long serialVersionUID = 3589911710094580888L;

			@Override
			public void checkInput(String s, Column col) {

				String dateformat = "MM/dd/yyyy";
				SimpleDateFormat dateFormat = new SimpleDateFormat(dateformat);
				dateFormat.setLenient(false);
				try {
					dateFormat.parse(s);
				} catch (ParseException pe) {
					throw new IllegalArgumentException("error, make sure your " + col.getColumnName() + " matches the following pattern \r\n "
							+ dateformat + " with the slashes, also make sure your month is put first");

				}
			}
		};

	}

}