package com.example.brere.ParkingLotOrganizationSystemAndroidExperiments.archives;


//import java.util.ArrayList;
//import java.util.function.Consumer;
//import java.util.function.Supplier;
//
//
//import client.communicationToServer.ServerCommunicator;
//import client.dataFieldStuff.DataField;
//import client.dataFieldStuff.DataFieldList;
//import client.dataFieldStuff.Reference;
//import client.systems.ControlSystem;
//import client.systems.util.insertionSystem.InsertionAdapter;
//import client.systems.util.insertionSystem.individualColumnStuff.DataFieldInputManager;
//import client.systems.util.insertionSystem.individualColumnStuff.DataInputBox;
/**
 * this is an archive of a userInputSystemWithSavedListeners I made which saves actionliseners, so you can dynamically change the 
 * actionlistners even when the system is closed. I changed this, because I wanted consistancy with the android version, and the android version 
 * SHOLD NOT support that functionality, due to activity lifecycle. 
 * 
 * undeo the commented out code wwhich states undo above it. 
 */
//public class UserInputSystemWithSavedListeners extends ControlSystem {
//	private ArrayList<DataFieldInputManager> managerList;
//	private ArrayList<DataFieldList> data;
//	private ArrayList<ActionListenerAndTitle> listeners;
//	private InsertionAdapter v;
//
//	public UserInputSystemWithSavedListeners(ServerCommunicator serv, String title, ArrayList<DataFieldList> data) {
//		super(serv, title);
//		this.data = data;
//		listeners = new ArrayList<ActionListenerAndTitle>();
//	}
//
//	@Override
//	public void open() {
//		managerList = new ArrayList<DataFieldInputManager>();
//		ArrayList<Supplier<String>> suppliers = new ArrayList<Supplier<String>>();
//		for (DataFieldList list : data) {
//			for (DataField field : list) {
//				Supplier<String> supplier = field.getColumn().getUserInputBoxConstructor()
//						.constructDataFieldInputBox(field);
//				boolean requiredNewManager = setupDataFieldsPlaceInTheManagers(field, supplier);
//				if (requiredNewManager) {
//					suppliers.add(supplier);
//				}
//			}
//		}
//		this.setView(createView(suppliers));
//
//	}
//
//	protected InsertionAdapter createView(ArrayList<Supplier<String>> suppliers) {
//		ArrayList<DataInputBox> box = new ArrayList<DataInputBox>();
//		for (Supplier<String> sup : suppliers) {
//			if (sup instanceof DataInputBox) {
//				box.add((DataInputBox) sup);
//			}
//		}
//		return new InsertionAdapter(box);
//	}
//
//	protected InsertionAdapter getInsertionView() {
//		return v;
//	}
//
//	protected ArrayList<DataFieldList> getList() {
//		return data;
//	}
//
//	protected void setView(InsertionAdapter view) {
//		v = view;
//		v.setVisible(true);
//	}
//
//	/**
//	 * this method puts the datafield in the correct DataFieldInputManager.
//	 * 
//	 * @param current
//	 *            the datafield which will be placed in the current input manager
//	 * @param supplier
//	 * @return if the datafield required a new manager.
//	 */
//	private boolean setupDataFieldsPlaceInTheManagers(DataField current, Supplier<String> supplier) {
//		if (current.getColumn() instanceof Reference && ((Reference) current.getColumn()).isAutoComplete()) {
//			Reference currentRefrence = (Reference) current.getColumn();
//
//			for (DataFieldInputManager currentManager : managerList) {
//				if (currentRefrence.refersTo(currentManager.getFirstDataField().getColumn())) {
//					currentManager.add(current);
//
//					return false;
//				}
//
//			}
//		}
//		DataFieldInputManager newManager = new DataFieldInputManager(supplier, current);
//		managerList.add(newManager);
//		return true;
//	}
//
//	/**
//	 * adds a button to the isnertio view which accepts this classes datafieldList
//	 * requires this class to be opened first.
//	 * 
//	 * @param buttonTitle
//	 * @param consumer
//	 */
//	public void addDataFieldListConsumerButton(String buttonTitle, Consumer<ArrayList<DataFieldList>> consumer) {
//		ActionListener listener = (ActionEvent arg0) -> {
//			try {
//				fillList();
//				consumer.accept(data);
//			//must be undone
//			//	v.setAllBoxesText();
//			} catch (IllegalArgumentException e) {
//				JOptionPane.showMessageDialog(null, e.getMessage());
//			}
//		};
//		listeners.add(new ActionListenerAndTitle(buttonTitle, listener));
//	}
//
//	private void addAllDataFieldListConsumerButtonToView() {
//		for(ActionListenerAndTitle actAndTitle: listeners) {
//			v.addButton(actAndTitle.title, actAndTitle.list);
//		}
//	}
//
//	/**
//	 * Fills the DataFieldlist with the entered data requires this object to be
//	 * opened first
//	 */
//	public void fillList() {
//		for (DataFieldInputManager manager : managerList) {
//			manager.copyDataIntoDataFields();
//		}
//	}
//
//	/**
//	 * reuires this classs to be opened first.
//	 */
//	@Override
//	public JPanel getDefaultPanel() {
//		return v;
//	}
//
//	@Override
//	public void close() {
//		managerList = null;
//		v = null;
//	}
//
//	private static class ActionListenerAndTitle {
//		public String title;
//		public ActionListener list;
//
//		public ActionListenerAndTitle(String title, ActionListener list) {
//			this.title = title;
//			this.list = list;
//		}
//	}
//}
