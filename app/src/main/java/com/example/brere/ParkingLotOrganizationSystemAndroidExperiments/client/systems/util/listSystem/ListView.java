package com.example.brere.ParkingLotOrganizationSystemAndroidExperiments.client.systems.util.listSystem;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.SearchView;
import android.widget.Spinner;

import com.example.brere.ParkingLotOrganizationSystemAndroidExperiments.R;
import com.example.brere.ParkingLotOrganizationSystemAndroidExperiments.client.dataFieldStuff.Column;
import com.example.brere.ParkingLotOrganizationSystemAndroidExperiments.client.dataFieldStuff.DataFieldList;
import java.util.ArrayList;
import java.util.function.Consumer;

/**
 * this is the class which is in charge of the UI for the searchSystem
 */
public class ListView {
	/** these are the possible things that the user can search for*/
	 private ArrayList<Column> possibleInputs;
	 /** this is the adapter for handeling all of the search rsults*/
	 private ListRecyclierAdapter adapter;
	 /** this is the view that the user will use to input searches */
	 private SearchView searchView;
	 /** this is the spinner for searching for things*/
	 private Spinner spinner;

	/**
	 * instantiates a new ListView
	 * @param possibleSearchInputs these are the possible things that the user can search for
	 * @param dataFieldListConsumer this is basically a method for an onClickListener. this method
	 *                              will be used each time a search result is clicked on
	 */
	 public ListView(ArrayList<Column> possibleSearchInputs,Consumer<DataFieldList> dataFieldListConsumer){
	 	this.possibleInputs=possibleSearchInputs;
	 	this.adapter=new ListRecyclierAdapter(new ArrayList<>(),dataFieldListConsumer);
	 }

	/**
	 * this method creates the view for the user to see.
	 * @param inflater the LayoutInflater used to create the view
	 * @param container the contrainer that the view bleongs to
	 * @param fragmentContext the context that the view belongs to
	 * @return a working search view, for the user to interact with
	 */
	public View createView(LayoutInflater inflater, @Nullable ViewGroup container, Context fragmentContext) {
	 	View rootView=inflater.inflate(R.layout.list_view_layout,container,false);
		RecyclerView rec=rootView.findViewById(R.id.listViewRecyclier);

		rec.setLayoutManager(new LinearLayoutManager(fragmentContext));
		rec.setAdapter(adapter);

		spinner=rootView.findViewById(R.id.listViewSpinner);

		ArrayAdapter<Column> arrayAdapter=new ArrayAdapter<>(fragmentContext,R.layout.support_simple_spinner_dropdown_item,possibleInputs);

		arrayAdapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
		spinner.setAdapter(arrayAdapter);
		searchView=rootView.findViewById(R.id.searchView);

		return rootView;
	}

	/**
	 * get the ListRecyclierAdapter that has all of the search results in it
	 * @return the ListRecyclierAdapter that has all of the search results in it
	 */
	public ListRecyclierAdapter getAdapter() {
		return adapter;
	}

	/**
	 * get the view the user uses to search for things in
	 * @return the view the user uses to search for things in
	 */
	public SearchView getSearchView() {
		return searchView;
	}

	/**
	 * get the spinner the user uses to select what thing they are searching for
	 * @return the spinner the user uses to select what they are searching for
	 */
	public Spinner getSpinner() {
		return spinner;
	}
}
