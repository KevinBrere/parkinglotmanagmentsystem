package com.example.brere.ParkingLotOrganizationSystemAndroidExperiments.shared.sentObjects.ServerToClient;

import java.util.ArrayList;

/**
 * this class is basically an error message which the server sends to the client. The client recieves this message, and
 * executes the printMessage function, which will then display the error message.
 */
public class ErrorMessageFromServerToClient implements MessageFromServerToClient{
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 8926601651899261501L;
	/** The what to do with the clients saved instructions. basically this enum tells the client what to do with the instructions it has
	 * saved, like whether to delete those instructions or save them for later use */
	private WhatToDoWithClientSavedInstructions whatToDo;
	/** The error data from the unsuccessful ServerFunction. */
	private ErrorData errorMessage;
	/** The id of the message being sent. */
	private int id;
	/**
	 * Instantiates a new error message from server to client.
	 * @param whatToDo what the client should do with its instructions after executing, ie keep or delete the instructons
	 * @param id the id of the message being sent
	 * @param errorMessage the error message being sent
	 */
	public ErrorMessageFromServerToClient(WhatToDoWithClientSavedInstructions whatToDo,int id, ErrorData errorMessage) {
		super();
		this.whatToDo = whatToDo;
		this.errorMessage = errorMessage;
		this.id=id;
	}
	/* (non-Javadoc)
	 * @see shared.serverClientCommunicationFramework.serverMessageFromServerToClient.MessageFromServerToClient#printMessage(java.util.ArrayList)
	 */
	@Override
	public void printMessage(ArrayList<ClientsInstructionsForReceivingData<?>> messages) {
		ClientsInstructionsForReceivingData<?> storage=whatToDo.execute(messages, id);
		storage.displayErrorMessage(errorMessage);
	}

}
