package com.example.brere.ParkingLotOrganizationSystemAndroidExperiments.client.systems.util.insertionSystem.individualColumnStuff;

import android.support.annotation.NonNull;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.text.method.DigitsKeyListener;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.brere.ParkingLotOrganizationSystemAndroidExperiments.R;
import com.example.brere.ParkingLotOrganizationSystemAndroidExperiments.client.dataFieldStuff.Column;
import com.example.brere.ParkingLotOrganizationSystemAndroidExperiments.client.dataFieldStuff.DataField;
import com.example.brere.ParkingLotOrganizationSystemAndroidExperiments.client.dataFieldStuff.DataFieldList;

import org.w3c.dom.Text;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * this is a dataInputBox which is used to enter a time duration. Like 3:00 am - 2:00 pm. that kind of thing
 * the user can easily input a time duration using this DataInputBox
 */
public class TimeDurationInputBox extends DataInputBox {
	/**
	 * creates a DataInputBoxConstructor which will construct a timeDurationInputBox
	 * @return a DataInputBoxConstructor which will construct a timeDurationInputBox
	 */
	public static DataInputBoxConstructor constructDataFieldInputBox() {
		return new DataInputBoxConstructor() {
			private static final long serialVersionUID = 22400L;
			@Override
			public DataInputBox constructDataInputBox(DataField d, DataFieldList listFieldBelongsTo, @NonNull ViewGroup parent, int viewType) {
				View v = LayoutInflater.from(parent.getContext())
						.inflate(R.layout.hours_input_box, parent, false);
				return new TimeDurationInputBox(d,listFieldBelongsTo,v);
			}
		};
	}
	/**serial id */
	private static final long serialVersionUID = 8408795428310827802L;

	/** a Time Input Box for the starting time*/
	private TimeInputBox start;
	/** a time imnput box for the ending time*/
	private TimeInputBox end;
	/** a RadioButtonDataInputBox for the start am/pm*/
	private RadioButtonDataInputBox startAm;
	/** a RadioButtonDataInputBox for the end am/pm*/
	private RadioButtonDataInputBox endAm;

	/**
	 * constructs a new TimeDurationInputBox
	 * @param data the data that this box is representing
	 * @param listFieldBelongsTo the list that the DataFieldList belongs to
	 * @param v the view that displays this to the user
	 */
	public TimeDurationInputBox(DataField data, DataFieldList listFieldBelongsTo, View v) {
		super(v, data, listFieldBelongsTo);

		((TextView)v.findViewById(R.id.hoursLabel)).setText(data.getColumn().getColumnName());
		Column startLabel=new Column("start time", 5, -1, true,false);
		Column endLabel=new Column("end time", 5, -1, true,false);
		Column amLabel=new Column("", 2, -1, true,false);

		start=new TimeInputBox(new DataField(startLabel,null),null,v.findViewById(R.id.startHours));
		end=new TimeInputBox(new DataField(endLabel,null),null,v.findViewById(R.id.endHours));

		startAm=new RadioButtonDataInputBox(v.findViewById(R.id.startAmPm),new DataField(amLabel,null),null,new ArrayList<>( Arrays.asList("am","pm")));
		endAm=new RadioButtonDataInputBox(v.findViewById(R.id.endAmPm),new DataField(amLabel,null),null,new ArrayList<>( Arrays.asList("am","pm")));
		setEditText(start);
		setEditText(end);
	}

	private void setEditText(TimeInputBox box) {
		box.getField().setLayoutParams(new LinearLayout.LayoutParams(300,150));
	}

	@Override
	public String get() {
		DataInputBox[] boxes = { start, startAm, end, endAm };
		String[] results = new String[boxes.length];

		for (int i = 0; i < boxes.length; i++) {
			results[i] = boxes[i].get();
			if (results[i] == null) {
				return null;
			}
		}
		return results[0] + " " + results[1] + " - " + results[2] + " " + results[3];
	}

	@Override
	public void setEditable(boolean edit) {
		start.setEditable(edit);
		end.setEditable(edit);
		startAm.setEditable(edit);
		endAm.setEditable(edit);

	}

	@Override
	protected void setUsersInputText(String s) {
		if (s == null) {
			setTextInvisable();
			return;
		}
		String[] splitByAm = s.split(" - ");
		if (splitByAm.length != 2) {
			throw new IllegalArgumentException("error, your input in setText in TimeDurtionInputBox is not valid");
		}
		setTextofPair(splitByAm[0], start, startAm);
		setTextofPair(splitByAm[1], end, endAm);
	}

	/**
	 * sets the Text of a given pair of both of the ams, or both of the pms
	 * @param string the string which represents a single time, ie "3:00 pm"
	 * @param start2 the TimeInputBox which displays the time
	 * @param startAm2 the RadioButtonINputBox, which displays if its am/pm
	 */
	private void setTextofPair(String string, TimeInputBox start2, RadioButtonDataInputBox startAm2) {
		String[] split = string.split(" ");
		if (split.length != 2) {
			throw new IllegalArgumentException("error, your input in setText in TimeDurtionInputBox is not valid");
		}
		start2.setText(split[0]);
		startAm2.setText(split[1]);
	}

	@Override
	public void setTextInvisable() {
		start.setTextInvisable();
		end.setTextInvisable();
		startAm.setTextInvisable();
		endAm.setTextInvisable();
	}

	/**
	 * this is  a DefaultDataInputBox, but specifically made to hold time inputs. like it automatically
	 * parses the time and adds a ':' where it is needed
	 */
	private static class TimeInputBox extends DefaultDataInputBox {
		/**serializable id */
		private static final long serialVersionUID = -7797251257829148839L;
//		public static DataInputBoxConstructor constructDataFieldInputBox() {
//			return new DataInputBoxConstructor() {
//				private static final long serialVersionUID = 22400L;
//				@Override
//				public DataInputBox constructDataInputBox(DataField d, DataFieldList listFieldBelongsTo, @NonNull ViewGroup parent, int viewType) {
//					View v = LayoutInflater.from(parent.getContext())
//							.inflate(R.layout.default_data_input_box_view, parent, false);
//					TimeInputBox box= new TimeInputBox(d,listFieldBelongsTo,v);
//					box.getField().setInputType(InputType.TYPE_CLASS_NUMBER|InputType.TYPE_NUMBER_VARIATION_NORMAL);
//					return box;
//				}
//			};
//		}

		/**
		 * constructs a new TimeInputBox
		 * @param data the data which it represents
		 * @param list the List that the data belongs to
		 * @param v the view that displays the TimeInputBox
		 */
		public TimeInputBox(DataField data, DataFieldList list, View v) {
			super(data, list, v);

			super.getField().setInputType(InputType.TYPE_CLASS_NUMBER|InputType.TYPE_NUMBER_VARIATION_NORMAL);
			super.getField().setKeyListener(DigitsKeyListener.getInstance("0123456789:"));

			super.getField().addTextChangedListener(new TextWatcher() {
				private boolean delete;
				@Override
				public void beforeTextChanged(CharSequence charSequence,  int start,int count,int after) {
					if(after==0){
						delete=true;
					}
					else {delete=false;}
				}

				@Override
				public void onTextChanged(CharSequence charSequence, int start, int before, int count) {}

				@Override
				public void afterTextChanged(Editable editable) {
					if(editable.length()==2&&!delete){
						editable.append(':');
					}
					else if(!delete&&(editable.length()>2&&editable.charAt(2)!=':') ){
						String s=editable.toString();
						StringBuffer withoutSlashes=new StringBuffer(s.replace(":",""));
						withoutSlashes.insert(2,':');
						editable.clear();
						editable.append(withoutSlashes);
					}
				}
			});
		}

		@Override
		protected void setUsersInputText(String s) {
				String dateformat = "HH:mm";
				SimpleDateFormat dateFormat = new SimpleDateFormat(dateformat);
				dateFormat.setLenient(true);
				try {
					dateFormat.parse(s);
				} catch (ParseException pe) {
					throw new IllegalArgumentException("error, make sure your "
							+ super.getDataField().getColumn().getColumnName() + " matches the following pattern \r\n "
							+ dateformat + " including the colons and the dash");
				}
				super.setUsersInputText(s);

		}
	}

}
