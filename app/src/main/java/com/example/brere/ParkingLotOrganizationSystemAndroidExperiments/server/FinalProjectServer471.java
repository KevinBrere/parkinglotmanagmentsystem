package com.example.brere.ParkingLotOrganizationSystemAndroidExperiments.server;


import java.io.IOException;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;


/** provides an object of type FinalProjectServer which creates the thread which the database will handle inputs (DataBaseBackend).
 * it also links DatabaseBackend class's sockets with the server sockets, which allows that class to communicate with the client
 * it also creates and stores each DataBaseBackend in a ExecutorService, which then starts its process.
 * @author Kevin Brereton
 *@version 1.0
 *@since march 14 2018
 */

//this was copy pasted from my other project, It most likley wont change too much no matter what we do
public class FinalProjectServer471 {
	/**
	 * the server socket it uses to create the sockets, which link to the client
	 */
	private ServerSocket servSocket;
	/**
	 * the pool of threads, which holds all of the DataBaseBackends
	 */
	private ExecutorService pool;
	
	/**
	 * creates a new FinalProjectServer object, instantiating all members, and initiating the sever with an id of 9091
	 */
	public FinalProjectServer471( int i) {
		try {
			servSocket=new ServerSocket(i);
			pool=Executors.newCachedThreadPool();
			
			System.out.println("Server started with address: " + InetAddress.getLocalHost().getHostAddress());
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	/**
	 * looks for a client linking to the server, when it finds them, creates a new messengerHandeler
	 * and adds it to the pool.  
	 */
	public void addClient() {
		try {
			 Socket xSoc=servSocket.accept();	
			 SingleClientServerSide client=new SingleClientServerSide(xSoc);
				pool.execute(client.getReceiver());
				System.out.println("new client");
		} catch (IOException e) {
			System.out.println("IO error with accepting a clinet");
			System.exit(1);
			e.printStackTrace();
		}
	}
	public static void main(String[] args) {		
		//warning, if you change this from 1000, you also must change the value in clients->communicationToServer
		//->ClientsCommunicationSystem
		FinalProjectServer471 server=new FinalProjectServer471(1001);
		while(true) {
			server.addClient();
		}
	}
	
}
