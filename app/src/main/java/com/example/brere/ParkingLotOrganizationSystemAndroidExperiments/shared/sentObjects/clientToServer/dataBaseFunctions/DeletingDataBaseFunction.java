package com.example.brere.ParkingLotOrganizationSystemAndroidExperiments.shared.sentObjects.clientToServer.dataBaseFunctions;

import com.example.brere.ParkingLotOrganizationSystemAndroidExperiments.client.dataFieldStuff.DataFieldList;
import com.example.brere.ParkingLotOrganizationSystemAndroidExperiments.shared.sentObjects.ServerToClient.ErrorData;
import com.example.brere.ParkingLotOrganizationSystemAndroidExperiments.shared.stuffInTheServer.ServerError;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
/**
 * this is a class for deleting a tuple from the database. It can delete any
 * tuple from the database, using the same relation system as searching. it can
 * handle basically any input you put into it, as long as it exists in the
 * database.
 *
 * this is an extremely unsecure implementation Im not going to lie. Like a
 * hacker can literally wipe the whole database in like 5 seconds because of
 * this. This should probably be changed for the sake of security at some point
 *
 * @author brere
 */
public class DeletingDataBaseFunction extends DataBaseFunction{
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -5598726142570671411L;
	/**
	 * Instantiates a new deleting data base function, which is an order for the database to delete the tuples in data
	 * @param data the data which will be searched for, and any matches will be deleted. ie if there is just ID=2 as the
	 * dataField, then any tuples with an id of 2 will be deleted
	 */
	public DeletingDataBaseFunction(DataFieldList data) {
		super(data,"deletion");
	}
	/* (non-Javadoc)
	 * @see shared.serverClientCommunicationFramework.serverOrdersFromClientToServer.dataBaseFunctions.DataBaseFunction#check(client.dataFieldStuff.DataFieldList)
	 */
	@Override
	protected void check(DataFieldList data) {
		if(data.isEmpty()) {
			throw new IllegalArgumentException("error, you tried to delete an empty dataFieldList, you almost deleted all of the tuples");
		}
	}
	/* (non-Javadoc)
	 * @see shared.serverClientCommunicationFramework.serverOrdersFromClientToServer.dataBaseFunctions.DataBaseFunction#execute(java.sql.Connection)
	 */
	@Override
	public ArrayList<DataFieldList> execute(Connection jdbcConnection) throws SQLException, ServerError {
		checkRefrencesErrorAtBeginning(jdbcConnection);
		
		String sql = "DELETE FROM " + super.getData().getTable().getDataBaseTitle() + " WHERE ";
		
		
		sql += super.createOneSearchComparer(super.getData().get(0));
		
		for (int i = 1; i < super.getData().size(); i++) {
			sql += " AND " + super.createOneSearchComparer(super.getData().get(i));
		}
		
		
			PreparedStatement statement = jdbcConnection.prepareStatement(sql);
			
			for(int i=0;i<super.getData().size();i++) {
				statement.setString(i+1, super.getData().get(i).getData());
			}
			
			statement.executeUpdate();
			
		
		ArrayList<DataFieldList> entered=new ArrayList<DataFieldList>();
		entered.add(super.getData());
		return entered;
	}
	/**
	 * checks for any refrence errors, before the function executes. that way nothing will be deleted of any refrential constraints are
	 * violated
	 * @param jdbcConnection the jdbc connection which it uses to check refrences
	 * @throws SQLException if there is an sql error while checking refrences
	 * @throws ServerError if there is an expected error while checking refrences.
	 */
	private void checkRefrencesErrorAtBeginning(Connection jdbcConnection) throws SQLException, ServerError {
		ErrorData error=super.checkRefrencesTothisList(jdbcConnection, super.getData());
		if(error!=null) {
			throw new ServerError(error);
		}
	}
	/* (non-Javadoc)
	 * @see shared.serverClientCommunicationFramework.serverOrdersFromClientToServer.dataBaseFunctions.DataBaseFunction#giveErrorMessage(java.lang.Exception, java.sql.Connection)
	 */
	@Override
	protected ErrorData giveErrorMessage(Exception e, Connection jdbcConnection) {
		return super.doStandardErrorProcedure(e);
	}
	
	
}
