package  com.example.brere.ParkingLotOrganizationSystemAndroidExperiments.client.dataFieldStuff;

import java.io.Serializable;
import java.util.ArrayList;
//TODO refractor table to have less fields
/**
 * this is a class that represents a table in a database. It has a collection of
 * columns, and it keeps track of what columns belong to it, the title of the
 * table, and what Refrences refer to the table.
 */
public class Table implements Serializable{
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 4180912838038251118L;
	/** The columns which are in the table */
	private ArrayList<Column> columns;
	/** The columns which are NonNull. */
	private ArrayList<Column>columnsForInsertion;
	/** The title of the table */
	private String title;
	/** The refrences which refer to this table. */
	private ArrayList<Reference> refrencesReferingToThisTable;
	/** The keys which belong to this table. */
	private  ArrayList<Column> keys;
	/**
	 * Gets the title of this table
	 * @return the title of this table
	 */
    public String getTitle() {
        return title;
    }
	/**
	 * Gets the columns which belong to the table
	 * @return the columns which belong to the table
	 */
    public ArrayList<Column> getColumns() {
        return columns;
    }
	/**
	 * Instantiates a new table.
	 * @param title the title of the table
	 * @param colums the columns which belong to the table
	 */
    public Table(String title,ArrayList<Column> colums) {
    	this(title);
        this.setUp(colums);
    }
	/**
	 * Instantiates a new table, without any columns. the columns are added after
	 * instantiation in setup method
	 * @param title     the title
	 */
    public Table(String title) {
        this.title=title;
    	this.refrencesReferingToThisTable=new ArrayList<Reference>();
    }
	/**
	 * Sets up the Table, so that it is a valid table
	 * @param colums    the columns which belong to the table
	 */
    public void setUp (ArrayList<Column> colums) {
          this.setColumns(colums);
    }
	/**
	 * Sets the columns that belong to the table
	 * @param cols     the new columns
	 */
    protected void setColumns(ArrayList<Column> cols) {
		this.columns=cols;
		setupInsertion();
		setupKeys();
		makeTheColumnsTableGetterWork();
	}


	/**
	 * sets up the columsForInsertion field
	 */
    private void  setupInsertion(){
        columnsForInsertion=new ArrayList<Column>();
        for(int i=0;i<columns.size();i++){
                if(columns.get(i).isNonNull()){
                    columnsForInsertion.add(columns.get(i));
                }
            }
        }
	/**
	 * Sets up the keys field
	 */
    private void  setupKeys(){
        keys=new ArrayList<Column>();
        for(int i=0;i<columns.size();i++){
                if(columns.get(i).isKey()){
                    keys.add(columns.get(i));
                }
            }
        }
	/**
	 * Sets the columns so that they think that they belong to this table
	 */
    private void makeTheColumnsTableGetterWork() {
    	for(Column col : columns) {
    		col.setTableWhichItBelongsTo(this);
    	}
    }
	/**
	 * Gets the key colums for this table
	 * @return the key columns for this table
	 */
	public ArrayList<Column> getKeys() {
		return keys;
	}
	/**
	 * Gets An arrayList full of every Refrence object that Refers to this table
	 * @return An arrayList full of every Refrence object that Refers to this table
	 */
	public ArrayList<Reference> getRefrencesToThisTable() {
		return refrencesReferingToThisTable;
	}
	/**
	 * Adds the refrencee so that we know that it refers to this table
	 *
	 * @param refrence the refrence that will refer to this table
	 */
	public void addRefrenceeToThisTable(Reference refrence) {
		this.refrencesReferingToThisTable.add(refrence);
	}

	/**
	 * checks if the inputed Table is equal to it
	 * @param table the table that is equal to this table
	 * @return true, if the tables are equal
	 */
	public boolean equals(Table table) {
		for(int i=0;i<columns.size();i++) {
			if(!this.columns.get(i).equals(table.columns.get(i))) {
				return false;
			}
		}
		return true;	
	}
	/**
	 * Returns a new DataFieldList which holds a DataField for every Column that is
	 * nonNull
	 *
	 * @return a new DataFieldList which holds a DataField for every Column that is
	 *         nonNull
	 */
	public DataFieldList getDataFieldListForInserting() {
		DataFieldList list=new DataFieldList(this);
    	for(int i=0;i<columnsForInsertion.size();i++) {
    		list.add(new DataField(columnsForInsertion.get(i).copy(columnsForInsertion.get(i).getColumnNumber()),null));
    	}
    	return list;
	}
	/**
	 * Gets each Refrence object in this table.
	 *
	 * @return each Refrence object in this table
	 */
	public ArrayList<Reference> getAllOfThisTablesRefrences() {
		
			ArrayList<Reference> refrences=new ArrayList<Reference>();
			for(int i=0;i<this.columns.size();i++) {
				if(this.columns.get(i) instanceof Reference) {
					refrences.add((Reference)this.columns.get(i));
				}
			}
			return refrences;	
	}
	/**
	 * this returns the column with the specified column number. DOES NOT GIVE COPY.
	 * WILL GIVE ACUTAL VALUE.
	 *
	 * @param i the column nuber of the colum which will be found
	 * @return the column with the column number specified.
	 * @throws IllegalArgumentException
	 *             if it could not find it.
	 */
	public Column getColumnBasedOnColumnNumber(int i) {
		for(int j=0;j<columns.size();j++) {
			if(columns.get(j).getColumnNumber()==i) {
				return columns.get(j);
			}
		}
		throw new IllegalArgumentException("error, you tried to get a label based on a colum number which did not exist column nuber: "+i+" number of columns: "+columns.size());
	}
	/**
	 * Gets the title of the Table, parsed so that it has only upper case letters,
	 * and no non character letters
	 *
	 * @return the title of the Table, parsed so that it it has only upper case
	 *         letters, and no non character letters
	 */
	public String getDataBaseTitle()
	{
		return title.replaceAll("[^a-zA-Z]", "").toUpperCase();
	}
	/**
	 * Gets each Column object which is nonNull.
	 * @return the columns for insertion which are nonNUll
	 */
	  public ArrayList<Column> getColumnsForInsertion() {
	        return columnsForInsertion;
	    }
	/**
	 * returns a new DataFieldList which has each column in the table, and the Data set to null
	 * @return a new DataFieldList which has each column in the table, and the Data set to null
	 */
	    public DataFieldList getAsDataFieldList() {
	    	DataFieldList list=new DataFieldList(this);
	    	for(int i=0;i<columns.size();i++) {
	    	
	    		list.add(new DataField(columns.get(i).copy(columns.get(i).getColumnNumber()),null));
	    	}
	    	return list;
	    }
	
}