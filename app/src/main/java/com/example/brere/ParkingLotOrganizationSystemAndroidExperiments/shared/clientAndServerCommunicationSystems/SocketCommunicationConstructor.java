package com.example.brere.ParkingLotOrganizationSystemAndroidExperiments.shared.clientAndServerCommunicationSystems;

import android.os.AsyncTask;
import android.util.Log;

import com.example.brere.ParkingLotOrganizationSystemAndroidExperiments.client.systems.util.MessageDisplayer;
import com.example.brere.ParkingLotOrganizationSystemAndroidExperiments.shared.clientAndServerCommunicationSystems.SenderToSocket;

import java.io.IOException;
import java.net.Socket;
import java.util.function.Consumer;

/**
 * The Class SocketCommunicationConstructor, which constructs a pair of RecieverFromSocket and SenderToSocket classes
 *
 * @param <T1> the type which will be sent to the socket
 * @param <T2> the type which will be recieved from the socket
 */
public class SocketCommunicationConstructor<T1,T2> {
	/** The socket sent/recieved from. */
	private Socket socket;
	/** The error message displayer. */
	private Consumer<String> errorMessageDisplayer;
	/** The sender. */
	private SenderToSocket<T1> sender;
	/** The receiver. */
	private ReceiverFromSocket<T2>receiver;
	/**
	 * Instantiates a new socket communication constructor.
	 *
	 * @param socket the socket which the sender/reciever will communicate using
	 * @param errorMessageDisplayer the error message displayer which will display
	 * if there was an error message while instantiating, or recieving.
	 */
	public SocketCommunicationConstructor(Socket socket,Consumer<String> errorMessageDisplayer) {
		super();
		this.socket = socket;
		this.errorMessageDisplayer=errorMessageDisplayer;
		constructCommunicators();
	}
	/**
	 * Instantiates a new socket communication constructor.
	 * @param clientName the name of the place that
	 * @param portNum the port number
	 * @param errorMessageDisplayer the error message displayer, which will display an error message if
	 * something goes wrong while recieving, or during the systems instantiation
	 */
	public  SocketCommunicationConstructor(String clientName, int portNum,Consumer<String> errorMessageDisplayer) {

		this.errorMessageDisplayer = errorMessageDisplayer;
		constructCommunicators();

		//TODO refractor this for service, so it is less conveluted
		AsyncTask<Void, Void, Void> sendTask = new AsyncTask<Void, Void, Void>() {

			@Override
			protected Void doInBackground(Void... voids) {
				try {
					System.err.println("tried to set the socket ");
				socket=	new Socket(clientName, portNum);
				getSender().setSocket(socket);
				getSocketReceiver().setSocket(socket);
					Log.e("notifying done", " socket was set in both");
				} catch (IOException e) {
					e.printStackTrace();
					errorMessageDisplayer.accept("error, we were unable to connect to the server. please contact customer support for more details");
				}
				return null;
			}

		};
		sendTask.execute();
	}
	/**
	 * Gets the sender.
	 *
	 * @return the sender
	 */
	public SenderToSocket<T1> getSender() {
		return sender;
	}
	/**
	 * Gets the socket receiver.
	 *
	 * @return the socket receiver
	 */
	public ReceiverFromSocket<T2> getSocketReceiver() {
		return receiver;
	}
	/**
	 * Construct communicators.
	 */
	private void constructCommunicators() {
		this.sender=new SenderToSocket<T1>(socket, errorMessageDisplayer);
		this.receiver=new ReceiverFromSocket<T2>(socket,errorMessageDisplayer);
	}
	/**
	 * Closes the system
	 */
	public void close() {
		sender.close(errorMessageDisplayer);
		receiver.close();
		try {
			socket.close();
		} catch (IOException e) {
			errorMessageDisplayer.accept("error, we were unable to close the socket in socketCommunicationConstructor");
			e.printStackTrace();
		}
	}
}
