package com.example.brere.ParkingLotOrganizationSystemAndroidExperiments.client.communicationToServer;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;

/**
 * this class is a serveice for communicating to the server. Its basically a service so that
 * the android operating system can kill it when it needs to
 */
public class ServerCommunicationService extends Service {
	/** the system used to comunicate to the server */
	private ClientsCommunicationSystem communicationSystem;
	/** the binder to Activities*/
	private IBinder binder;

	/**
	 * creates a new ServerCommunicationService
	 */
	public ServerCommunicationService() {
		System.out.println("instantiated service");
		binder = new ServerCommunicatorBinder();
	}

	/**
	 * gets the server communicator used for communicating with the server
	 * @return the server communicator used for communicating with the server
	 */
	public ServerCommunicator getServerComunicator() {
		return communicationSystem.getCommunicator();

	}

	@Override
	public void onCreate() {
		System.out.println("comunication service created");
		communicationSystem=new ClientsCommunicationSystem(getApplicationContext());
	}
	@Override
	public void onDestroy(){
		System.out.println("Server Communicaton destroyed.");
		communicationSystem.getConstructor().close();
		communicationSystem=null;
	}
	@Override
	public IBinder onBind(Intent intent) {

		return binder;
	}

	/**
	 * this is the binder class which binds the service to other activities
	 */
	public class ServerCommunicatorBinder extends Binder {
		/**
		 * returns the ServerCommunicationService that the activity is binding to
		 * @return the ServerCommunicationService that the activitiy is binding to
		 */
		public ServerCommunicationService getServerComunicationService(){
			return ServerCommunicationService.this;
		}
	}
}
