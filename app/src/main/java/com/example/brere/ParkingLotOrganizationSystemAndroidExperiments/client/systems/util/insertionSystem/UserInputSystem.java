package com.example.brere.ParkingLotOrganizationSystemAndroidExperiments.client.systems.util.insertionSystem;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;

import com.example.brere.ParkingLotOrganizationSystemAndroidExperiments.client.dataFieldStuff.DataField;
import com.example.brere.ParkingLotOrganizationSystemAndroidExperiments.client.dataFieldStuff.DataFieldList;
import com.example.brere.ParkingLotOrganizationSystemAndroidExperiments.client.dataFieldStuff.Reference;
import com.example.brere.ParkingLotOrganizationSystemAndroidExperiments.client.systems.util.MessageDisplayer;
import com.example.brere.ParkingLotOrganizationSystemAndroidExperiments.client.systems.util.insertionSystem.individualColumnStuff.DataFieldInputManager;
import com.example.brere.ParkingLotOrganizationSystemAndroidExperiments.client.systems.ControlSystem;
import com.example.brere.ParkingLotOrganizationSystemAndroidExperiments.client.systems.util.insertionSystem.individualColumnStuff.DataInputBox;
import com.example.brere.ParkingLotOrganizationSystemAndroidExperiments.pretend.ControlSystemsActivity;
import com.example.brere.ParkingLotOrganizationSystemAndroidExperiments.shared.sentObjects.clientToServer.ModifyingDataBaseServerFunction;
import com.example.brere.ParkingLotOrganizationSystemAndroidExperiments.shared.sentObjects.clientToServer.dataBaseFunctions.SearchDataBaseFunction;

import java.util.ArrayList;
import java.util.function.Consumer;

/**
 * this is the largest and most developed system. It is the cornerstone of the project. This system
 * is what allows users to input stuff into the database.  like if you want to add or modify stuff in
 * the database, you would use a subclass of this.
 *
 * @author brere
 */

public abstract class UserInputSystem extends ControlSystem {

	/** a constant for creating bundes*/
	private static final String MULTIDATAFIELDLISTINPUT = "ParkingLotOrganizationSystem.client.systems.util.insertionsystem.UserInputSystem.MultiInputDataFeieldList";

	/**
	 * creates a bundle so that there is a valid userInputSystem instantiated
	 * @param title the title of the system
	 * @param dataFieldLists a dataFieldList having each peice of data the user wants to input to the
	 *                       database
	 * @return a bundle so that this system may be safely instantiated
	 */
	protected static Bundle createBundleForSystem(@NonNull String title, @NonNull ArrayList<DataFieldList> dataFieldLists) {
		Bundle b = ControlSystem.createBundleForSystem(title);
		b.putSerializable(MULTIDATAFIELDLISTINPUT, dataFieldLists);
		return b;
	}
	/** all of the dataInputManagers which manage all of the inputed datafields */
	private ArrayList<DataFieldInputManager> managerList;
	/** the data that the user wants to input*/
	private ArrayList<DataFieldList> data;
	/** the view that the user sees */
	private InsertionView v;

	@Override
	public void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		data = (ArrayList<DataFieldList>) getActivity().getIntent().getExtras().getSerializable(MULTIDATAFIELDLISTINPUT);
		managerList = new ArrayList<DataFieldInputManager>();
		ArrayList<InsertionAdapter.DataInputBoxConstructor> constructors = new ArrayList<>();

		for (DataFieldList list : data) {
			for (DataField field : list) {
				boolean requiredNewManager = addDataFieldToOtherDataFieldInputManagerIfNessicary(field);
				if (requiredNewManager) {
					field.getColumn().getUserInputBoxConstructor().constructDataFieldInputBox(field, list, constructors, managerList);
				}
			}
		}
		this.getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);

		ArrayList<InsertionAdapter.onDataInputBoxBind> insertionBind = new ArrayList<>();
		insertionBind.add(generateRefrenceBind());
		this.setView(createInsetionView(constructors, insertionBind));
	}
	/** this metod creates a binder which will activate each time a DataInputBox is bound
	 * what the binder will do, is add an onclickListener to each DataInputBox that holds a
	 * referance. When the DataInputBox is clicked, it will temporarily save the data (without
	 * checking if its valid) and will call show a dialog created in createRefrenceDialog
	 */
	private InsertionAdapter.onDataInputBoxBind generateRefrenceBind() {
		return (DataInputBox box, int position) -> {
			if (box.getDataField().getColumn() instanceof Reference) {
				box.itemView.setOnClickListener((View v) -> {
							try {
								box.getDataField().getColumn().checkUserInput(box.get());
							}catch (IllegalArgumentException e){
								MessageDisplayer.displayErrorMessage(this.getContext(),e.getMessage());
							}

							managerList.forEach((DataFieldInputManager::temporarilyCopyDataIntoDataFields));

							DataFieldList referedList = box.getListFieldBelongsTo().getPrimaryKeyOfRefrencedTable((Reference) box.getDataField().getColumn());
							AlertDialog generatedDialog = createRefrenceDialog(referedList);
							generatedDialog.show();
						}
				);
			}
		};
	}

	/**
	 * creates an alert dialog asking the user if they want to View the entered list, or add a new
	 * tuple of the same type of the list. If they click the add/view button, the startInsertionSystem
	 * and StartModificationSystem methods are called
	 * @param referedList the list that the user wants to view or add
	 * @return an AlertDialog that asks the user if they want to add/view the given data
	 */
	private AlertDialog createRefrenceDialog(DataFieldList referedList) {
		android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(this.getContext());

		builder.setTitle(referedList.getTable().getTitle());

		builder.setMessage("do you want to add or view a " + referedList.getTable().getTitle() + "?")
				.setCancelable(true)
				.setPositiveButton("add", (DialogInterface dialog, int id) -> {
					startInsertionSystem(referedList);
				})
				.setNegativeButton("view", (DialogInterface dialog, int id) -> {
					startModificationSystem(referedList);
				})
				.setNeutralButton("Cancel", (DialogInterface dialog, int id) -> {
					dialog.dismiss();
				});
		return builder.create();
	}

	/**
	 * Starts a new InsertionSystem activity, using a new dataFieldList, copying its keys from the
	 * referedList
	 * @param referedList the keys of the Data that you want to insert
	 */
	private void startInsertionSystem(DataFieldList referedList) {
		DataFieldList data = referedList.getTable().getAsDataFieldList();
		referedList.copyDataIntoAnotherDataFieldList(data);

		Intent intent = new Intent(this.getActivity(), ControlSystemsActivity.class);
		intent.putExtras(InsertionSystem.createBundleForSystem(data));
		startActivity(intent);
	}

	/**
	 * starts the modification system, by first searching the database to find the
	 * dataFieldList that is refered to by referedList.
	 * It then calls onSuccessFulModificationSearch if the search is successful
	 * @param referedList the list which refers to the thing you want to modify in the database
	 */
	private void startModificationSystem(DataFieldList referedList) {
		super.getServ().sendStuffToServer(this::onSuccessfulModificationSearch, null, new ModifyingDataBaseServerFunction(new SearchDataBaseFunction(referedList)));
	}

	/**
	 * when the database search from startModificationSystem of the database was successful, this method
	 * is called. This method starts a modificationSystem using the first dataFieldList in the ArrayList
	 * @param data the result of the database search. The first DataFieldList in this list will be used
	 *             to start a new modification system
	 */
	private void onSuccessfulModificationSearch(ArrayList<DataFieldList> data) {
		if (data.isEmpty()) {
			MessageDisplayer.displayErrorMessage(this.getContext(), "error, we could not find anything matching your input");
		} else {
			Intent intent = new Intent(this.getActivity(), ControlSystemsActivity.class);
			intent.putExtras(ModificationSystem.createBundleForSystem(data.get(0).getListWithoutHiddenItems()));
			startActivity(intent);
		}
	}

	@Override
	public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
							 @Nullable Bundle savedInstanceState) {
		View v = this.getInsertionView().createView(inflater, container, this.getContext());
		this.getInsertionView().getAdapter().notifyDataSetChanged();
		return v;
	}

	/**
	 * creates the insertionView suited for the system. override this method if you want to change
	 * the view that it creates from the default InsertionView
	 * @param constructors the constructors of all of the DataInputBoxes that the user will see
	 * @param onbind the things that will be done to the DataInputBoxes when they are bound
	 * @return the InsertionView object which the user will view
	 */
	protected InsertionView createInsetionView(ArrayList<InsertionAdapter.DataInputBoxConstructor> constructors, ArrayList<InsertionAdapter.onDataInputBoxBind> onbind) {
		return new InsertionView(constructors, onbind);
	}

	/**
	 * get the insertion view that the user will see
	 * @return the insertion view that the user will see
	 */
	protected InsertionView getInsertionView() {
		return v;
	}

	/**
	 *gets all of the datafieldLists that are in the userInputSystem
	 * @return all of the DataFieldLists that are in the userInputSystem
	 */
	protected ArrayList<DataFieldList> getList() {
		return data;
	}

	/**
	 * sets the insertionview of the system to that object
	 * @param view the insertionview that it will be set to
	 */
	protected void setView(InsertionView view) {
		v = view;
	}

	/**
	 * this method puts the datafield in an already existing DataFieldInputManager.
	 * like if the DataField refers to something that is already in the list, then it adds that
	 * reference to the other dataInputManager
	 * @param current the datafield which may be placed in a new input manager if nessicary
	 * @return if the datafield requires a new manager.
	 */
	private boolean addDataFieldToOtherDataFieldInputManagerIfNessicary(DataField current) {
		if (current.getColumn() instanceof Reference && ((Reference) current.getColumn()).isAutoComplete()) {
			Reference currentRefrence = (Reference) current.getColumn();

			DataFieldInputManager resultOfSearch = getManagerRefrenceBelongsTo(currentRefrence);
			if (resultOfSearch != null) {
				resultOfSearch.add(current);
				return false;
			}
		}
		return true;
	}


	/**
	 * searches the pre-existing dataInputManagers to see if the refrence belongs to
	 * any of them
	 *
	 * @param refrence the refrecne which will be searched if it belongs to any pre
	 *                 existing DataFieldInputManager
	 * @return the dataFieldInputManager it belongs to, or null if it does not
	 * belong to any pre existing ones.
	 */
	private DataFieldInputManager getManagerRefrenceBelongsTo(Reference refrence) {

		for (DataFieldInputManager currentManager : managerList) {
			if (refrence.refersTo(currentManager.getFirstDataField().getColumn())) {

				return currentManager;
			}

		}
		return null;
	}

	/**
	 *this is a method for adding a button to the user input system. like if you want there to be a
	 * buttom at the bottom for doing something, use this method.
	 * @param buttonTitle the text which will be displayed on the button
	 * @param consumer what do you want to do with the data in this Input system. Like when the user
	 *                 pushes this button, what do you do with the data that the user entered? All checks
	 *                 are done before your consumer is applied, so its not nessicary to check any inputs
	 */
	public void addDataFieldListConsumerButton(String buttonTitle, Consumer<ArrayList<DataFieldList>> consumer) {
		View.OnClickListener listener = (View arg0) -> {
			try {
				fillList();
				consumer.accept(data);
				v.getAdapter().setAllBoxesText();
			} catch (IllegalArgumentException e) {
				MessageDisplayer.displayAlertMessage(this.getContext(), "error", e.getMessage());
			}
		};
		v.addButton(listener, buttonTitle);
	}

	/**
	 * Fills the DataFieldlists with the data entered by the user. requires this object to be
	 * opened first
	 */
	public void fillList() {
		for (DataFieldInputManager manager : managerList) {
			manager.copyDataIntoDataFields();
		}
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		managerList = null;
		v = null;
	}

}
