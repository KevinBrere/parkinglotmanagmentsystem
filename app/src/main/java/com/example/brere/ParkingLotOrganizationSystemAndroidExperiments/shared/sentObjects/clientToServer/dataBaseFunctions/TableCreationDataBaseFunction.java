package com.example.brere.ParkingLotOrganizationSystemAndroidExperiments.shared.sentObjects.clientToServer.dataBaseFunctions;

import com.example.brere.ParkingLotOrganizationSystemAndroidExperiments.client.dataFieldStuff.Column;
import com.example.brere.ParkingLotOrganizationSystemAndroidExperiments.client.dataFieldStuff.DataFieldList;
import com.example.brere.ParkingLotOrganizationSystemAndroidExperiments.client.dataFieldStuff.Reference;
import com.example.brere.ParkingLotOrganizationSystemAndroidExperiments.shared.sentObjects.ServerToClient.ErrorData;

import java.sql.*;

import java.util.ArrayList;

/**
 * this is a class for creating a table in the database. it uses a
 * datafieldlists inputed "table" object to specify the conditions which it
 * would create the table with. ie it uses the data in the "column" and
 * "refrence" objects in the "table" object to create a table.
 *
 * @author brere
 *
 */
public class TableCreationDataBaseFunction extends DataBaseFunction{

	/** The Constant serialVersionUID. */

	private static final long serialVersionUID = 8595416352737806435L;
	/**
	 * Instantiates a new table creation data base function.
	 *
	 * @param data the data, whos table will be used In the TableCreationDataBaseFunction
	 */
	public TableCreationDataBaseFunction(DataFieldList data) {
		super(data,"table creation");
	}
	/* (non-Javadoc)
	 * @see shared.serverClientCommunicationFramework.serverOrdersFromClientToServer.dataBaseFunctions.DataBaseFunction#check(client.dataFieldStuff.DataFieldList)
	 */
	@Override
	protected void check(DataFieldList data) {
		if(data.getTable().getKeys().isEmpty()) {
			throw new IllegalArgumentException("error, you tried to enter a table with no keys");
		}
	}
	/* (non-Javadoc)
	 * @see shared.serverClientCommunicationFramework.serverOrdersFromClientToServer.dataBaseFunctions.DataBaseFunction#execute(java.sql.Connection)
	 */
	@Override
	public ArrayList<DataFieldList> execute(Connection jdbcConnection) throws IllegalArgumentException {
	
		//	String sql= "DROP TABLE " + super.getData().getTable().getDataBaseTitle(); 
	
		//databaseTitle
		String sql = "CREATE TABLE IF NOT EXISTS " + super.getData().getTable().getDataBaseTitle() + "(";
		 sql+=createGeneral();
		 sql+=" "+createKeys();
		 sql+=createRefrences();
		 sql+=")";
		 
		System.out.println(sql);
		
		try {
			PreparedStatement statement = jdbcConnection.prepareStatement(sql);
			statement.executeUpdate();
		} catch (SQLException e) {
			throw new IllegalArgumentException("error while creating"+super.getData().getTable().getTitle()+"\r\n"+
					"with message"+sql+"\r\n error code: "+e.getMessage()+"\r\n, sqlState:"+ e.getSQLState());
		}
		
		
		ArrayList<DataFieldList> original=new ArrayList<DataFieldList>();
		original.add(super.getData());
		return original;
	}

	/**
	 * Creates the part of the sql statment responsible for the keys. like stating which ones are
	 * primary keys and such
	 * @return the sql statment responsible for stating which data are keys
	 */
	private String createKeys() {
		ArrayList<Column> keyList=super.getData().getTable().getKeys();
		int i;
		String keys="";
		for( i=0;i<keyList.size()-1;i++) {
			keys+= keyList.get(i).getDataBaseColumnName()+", ";
		}
		keys+=keyList.get(i).getDataBaseColumnName();
		
		return "PRIMARY KEY ("+keys+")";
		 
	}
	/**
	 * Creates the general part of the sql statment, like it states that each peice of data is a varchar,
	 * and states which ones can be null
	 * @return general part of the sql stament which each column is part of
	 */
	private String createGeneral() {
		
		String sql = "";		
		for( int i=1;i<super.getData().getTable().getColumns().size()+1;i++) {
			
			Column current=super.getData().getTable().getColumnBasedOnColumnNumber(i);
			sql+=" "+current.getDataBaseColumnName()+" VARCHAR("+current.getMaxSize()+")";
			
			if(current.isNonNull()) {
				sql+= " NOT NULL";
			}
			
			sql+=",";
		}
		
		return sql;
	}
	/**
	 * Creates the part of the sql statment responsible for the refrences, and setting up how it handles refrential constraint violations
	 * @return the part of the sql statment responsible for refrences
	 */
	private String createRefrences() {
		
		ArrayList<Reference> refrencesList=super.getData().getTable().getAllOfThisTablesRefrences();
		
		String refrenceString="";	
		int i;
		for(i=0;i<refrencesList.size()-1;i++) {
		 refrenceString+= addrefrence(refrencesList.get(i));
		}
		if(!refrencesList.isEmpty()) {
			refrenceString+=addrefrence(refrencesList.get(i));
		}
		
		return refrenceString;
	}
	/**
	 * gives a part of an sql statement responsible for a single reference. like stating what that reference
	 * should do on delete, etc
	 * @param current the current Reference which it will make a sql statement out of
	 * @return part of the sql statement responsible for stating that the reference object is a reference.
	 */
	private String addrefrence(Reference current) {

		String refrenceString=", FOREIGN KEY ("+current.getDataBaseColumnName()+") REFERENCES "+current.getRefrenceTable().getDataBaseTitle()+" ("+current.getRefrenceColumn().getDataBaseColumnName()+") "; 		
		
		refrenceString+=" ON DELETE "+current.getDeletionConstraint().toString();
		refrenceString+=" ON UPDATE CASCADE";
		return refrenceString;
	}
	/* (non-Javadoc)
	 * @see shared.serverClientCommunicationFramework.serverOrdersFromClientToServer.dataBaseFunctions.DataBaseFunction#giveErrorMessage(java.lang.Exception, java.sql.Connection)
	 */
	@Override
	protected ErrorData giveErrorMessage(Exception e, Connection jdbcConnection) throws SQLException {
		return super.doStandardErrorProcedure(e);
	}
}
