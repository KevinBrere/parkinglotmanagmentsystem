package com.example.brere.ParkingLotOrganizationSystemAndroidExperiments.shared.clientAndServerCommunicationSystems;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.function.Consumer;

/**
 * this is a class which sends stuff to the socket, ie it will send whatever object you want
 * through the socket, as long as it is serializable
 * @author brere
 * @param <T> the type which will be sent through the socket
 */

public class SenderToSocket<T> {
	private ObjectOutputStream out;
	private Consumer<String> errorMessageDisplayer;
	/**
	 * instantiates a new senderToSocket
	 * @param sock the socket which it will send inputs through
	 * @param errorMessageDisplayer the error message displayer it will use to display error messages if instantiation fails
	 */
	public SenderToSocket(Socket sock,Consumer<String> errorMessageDisplayer) {
		this.errorMessageDisplayer=errorMessageDisplayer;
		if(sock!=null) {
			setSocket(sock);
		}
	}
	/**
	 * sends whatever is being sent.  if it fails, it will display an error to the inputed error displayer
	 * @param sent the thing that is being sent
	 * @return whether the thing was sent successfully
	 * @throws IOException  when it fails to send
	 */
	public void send(T sent) throws IOException {
		//temporary spaghetti solution before beefing up later into full loading screen
			if(out==null){
				throw new IOException("error, the out was null for some reason");
			}
			out.writeObject(sent);
			out.flush();
			out.reset();
	}
	/**
	 * closes the socket, so it cannot be used.
	 *
	 * @param errorMessageDisplayer the thing which will display error messages if the close operation fails
	 */
	public void close(Consumer<String> errorMessageDisplayer) {
		try {
			out.close();
		} catch (IOException e) {
			errorMessageDisplayer.accept("error, we were unable to close communication with the server.");
			e.printStackTrace();
		}
	}

	/**
	 * sets the socket, so that it will comunicate using the new socket
	 * @param socket the socket which it will comunicate using
	 */
	public void setSocket(Socket socket) {
		try {
			out=new ObjectOutputStream(socket.getOutputStream());
		} catch (IOException e) {
			errorMessageDisplayer.accept("error, unable to connect to server please try again later");
			e.printStackTrace();
		}
	}
}
