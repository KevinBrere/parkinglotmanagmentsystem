package com.example.brere.ParkingLotOrganizationSystemAndroidExperiments.client.systems.util.insertionSystem;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;

import com.example.brere.ParkingLotOrganizationSystemAndroidExperiments.R;

import java.util.ArrayList;

/**
 * this class is the part of the UserInputSystem which is in charge of the UI. It makes a visable user
 * interface that the user can use to insert/modify stuff in the database
 */
public class InsertionView {
	/**the insertionAdapter which will make all of the user inputs for each peace of data visable */
	private InsertionAdapter adapter;
	/** an arrayList full of OnClickListeners, and the text of their operation.
	 * these will be displayed below the user inputs as buttons */
	private ArrayList<OnclickListenerWithText> listeners;

	/**
	 * creates a new insertionview object
	 * @param constructors the constructors of the DataInputBoxes you want the user to see
	 * @param onBind the things to do to each DataInputBox whenever it is bound
	 */
	public InsertionView(ArrayList<InsertionAdapter.DataInputBoxConstructor> constructors, ArrayList<InsertionAdapter.onDataInputBoxBind> onBind){
		adapter=new InsertionAdapter(constructors,onBind);
		listeners=new ArrayList<>();
	}

	/**
	 * creates a visable view for the user to insert/modify stuff in the database using
	 * @param inflater the inflater used to create the view
	 * @param container the container the view will be in
	 * @param fragmentContext the context of the view
	 * @return the view that the user will use to insert/modify stuff in the database using
	 */

	public View createView(LayoutInflater inflater, @Nullable ViewGroup container, Context fragmentContext){
		View rootView=inflater.inflate(R.layout.user_input_system_view,container,false);


		RecyclerView rec=rootView.findViewById(R.id.UserInputSystemRecyclerView);

		rec.setLayoutManager(new LinearLayoutManager(fragmentContext));
		rec.setAdapter(adapter);


		LinearLayout layout=rootView.findViewById(R.id.UserInputSystemLinearLayoutForButtons);

		for(OnclickListenerWithText listener: listeners){
			Button but=new Button(container.getContext());
			but.setText(listener.text);
			but.setOnClickListener(listener.listener);
			layout.addView(but);
		}

		return rootView;
	}

	/**
	 * adds a Button below the user inputs.
	 * @param listener the Listner for the button
	 * @param text the text which will be displayed on the button
	 */
	public void addButton(View.OnClickListener listener, String text){
		listeners.add(new OnclickListenerWithText(text,listener));
	}

	/**
	 * this is a small class representing a button, which has the  OnclickListner the button will have
	 * and the text that the button will display
	 */
	private static class OnclickListenerWithText {
		/** the text displayed on the button*/
		String text;
		/** what the button will do */
		View.OnClickListener listener;

		/**
		 * creates a new OnclickListener object
		 * @param text the text the button will have
		 * @param listener what the button will do
		 */
		OnclickListenerWithText(String text, View.OnClickListener listener) {
			this.text = text;
			this.listener = listener;
		}
	}
	/**gets the adapter this class uses to display the user input boxes */
	public InsertionAdapter getAdapter() {
		return adapter;
	}
}
