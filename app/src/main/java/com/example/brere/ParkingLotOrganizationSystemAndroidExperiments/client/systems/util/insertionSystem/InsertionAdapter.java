//done

package com.example.brere.ParkingLotOrganizationSystemAndroidExperiments.client.systems.util.insertionSystem;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

import com.example.brere.ParkingLotOrganizationSystemAndroidExperiments.client.systems.util.insertionSystem.individualColumnStuff.DataInputBox;

import java.util.ArrayList;

/**
 * this class is the Recyclerview adapter used to display the user inputs, when using any userInputSystem
 */
public class InsertionAdapter extends RecyclerView.Adapter<DataInputBox> {
	/** serial id*/
	private static final long serialVersionUID = 639115811322313809L;
	/** this is a functional interface representing a method that will be used to create a
	 * dataInput box. it takes a ViewGroup and a viewType to create the dataInputBox */
	public static interface DataInputBoxConstructor {
		/**
		 * creates a dataInput box using the viewgroup and viewtype
		 * @param parent the view that will be used to create the data input box
		 * @param viewType the index of the view in the adapter
		 * @return the DataInputBox created from those
		 */
		public DataInputBox createInputBox(@NonNull ViewGroup parent, int viewType);
	}
	/**this is another functional interface, only it is for a method which will be used
	 * after each dataInputBox is bound. After any dataInputBox is bound, this method will
	 * activate*/
	public static interface onDataInputBoxBind{
		public void onBind(DataInputBox box,int position);
	}


	/**
	 * This is an array list of all the things that will happen to each dataInputBox after they are
	 * bound
	 */
	private ArrayList<onDataInputBoxBind> onBind;
	/** all of the DataInputBoxes currently in the adapter */
	private ArrayList<DataInputBox> panels;
	/** all of the input constructors, for constructing each DataInputBox */
	private ArrayList<DataInputBoxConstructor> constructors;

	/**
	 * crates a new InsertionAdapter
	 * @param constructors the constructors for each DataInputBox you want the user to interact with
	 * @param onBind what to do after each DataInputBox is bound
	 */
	public InsertionAdapter(ArrayList<DataInputBoxConstructor> constructors, ArrayList<onDataInputBoxBind> onBind) {
		this.panels = new ArrayList<>();
		this.constructors = constructors;
		this.onBind=onBind;
	}

	/**
	 * set all of the previous current user inputs invisable
	 */
	public void setTextInvisable() {
		for (DataInputBox box : panels) {
			box.setTextInvisable();
		}
	}

	/**
	 * get all of the DataInputBoxes in the system
	 * @return all of the DataInputBoxes in the system
	 */
	public ArrayList<DataInputBox> getPanels() {
		return panels;
	}


	/**
	 * set the text of each dataInputBox to the values stored in the DataFieldList
	 */
	protected void setAllBoxesText() {

		for (int i = 0; i < panels.size(); i++) {
			panels.get(i).setTextVisable();
		}
	}

	@NonNull
	@Override
	public DataInputBox onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
		DataInputBoxConstructor construct=constructors.get(viewType);
		DataInputBox box=construct.createInputBox(parent, viewType);
		panels.add(box);
		return box;
	}
	@Override
	public int getItemViewType(int position) {
		return position;
	}
	@Override
	public void onBindViewHolder(@NonNull DataInputBox holder, int position) {
		for(onDataInputBoxBind binder: onBind){
			binder.onBind(holder,position);
		}

		holder.setTextVisable();
	}
	@Override
	public void onViewRecycled(@NonNull DataInputBox holder) {
		holder.getDataField().setData(holder.get());
		super.onViewRecycled(holder);
	}
	@Override
	public int getItemCount() {
		return constructors.size();
	}
}