package com.example.brere.ParkingLotOrganizationSystemAndroidExperiments.pretend;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;

import com.example.brere.ParkingLotOrganizationSystemAndroidExperiments.R;
import com.example.brere.ParkingLotOrganizationSystemAndroidExperiments.client.communicationToServer.ServerCommunicationService;
import com.example.brere.ParkingLotOrganizationSystemAndroidExperiments.client.systems.ControlSystem;

import java.io.Serializable;

public class ControlSystemsActivity extends AppCompatActivity {




	public  interface CreateControlSystem extends Serializable {
		 ControlSystem generateControlSystem();
	}
	public static final String CONTROLSYSTEMGENERATEORID="PretendFinaltry2.Pretend.ControlSystemsActivity.ControlSystemGenerator";


	private controlSystemToServerServiceConnection serverConnection = new controlSystemToServerServiceConnection();
	private ServerCommunicationService serverCommunicatorService;
	private boolean connected;

	@Override
	public void onCreate( @Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.setContentView(R.layout.root_control_system_activity_layout);


		Intent intent=new Intent(this,ServerCommunicationService.class);
		this.bindService(intent,serverConnection, Context.BIND_AUTO_CREATE);

		CreateControlSystem creator=(CreateControlSystem) getIntent().getSerializableExtra(CONTROLSYSTEMGENERATEORID);

		ControlSystem control=creator.generateControlSystem();



		this.setTitle(getIntent().getExtras().getString(ControlSystem.CONTROLSYSTEMTITLE));

		FragmentTransaction ft= getSupportFragmentManager().beginTransaction();

		ft.add(R.id.AnyControlSystemRootFrame,control);

		ft.commit();
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);

	}
	@Override
	public void onDestroy(){
		if(connected) {
			unbindService(serverConnection);
		}
		super.onDestroy();
	}


	//to create notifcations, you must generate a backstack for the notification.  otherwise it will not work.
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			// Respond to the action bar's Up/Home button
			case android.R.id.home:
				finish();
				return true;
		}
		return super.onOptionsItemSelected(item);
	}
	public ServerCommunicationService getServerCommunicatorService() {
		return serverCommunicatorService;
	}
	private class controlSystemToServerServiceConnection implements ServiceConnection {

		@Override
		public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
			System.out.println("connected");
			ServerCommunicationService.ServerCommunicatorBinder serverBinder = (ServerCommunicationService.ServerCommunicatorBinder) iBinder;
			serverCommunicatorService = serverBinder.getServerComunicationService();
			connected=true;
		}

		@Override
		public void onServiceDisconnected(ComponentName componentName) {
			connected=false;
			System.out.println("disconnected");
		}
	}
}
