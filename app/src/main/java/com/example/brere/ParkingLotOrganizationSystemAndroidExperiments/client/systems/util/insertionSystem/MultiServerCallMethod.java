package com.example.brere.ParkingLotOrganizationSystemAndroidExperiments.client.systems.util.insertionSystem;

import com.example.brere.ParkingLotOrganizationSystemAndroidExperiments.client.communicationToServer.ServerCommunicator;
import com.example.brere.ParkingLotOrganizationSystemAndroidExperiments.client.dataFieldStuff.DataFieldList;
import com.example.brere.ParkingLotOrganizationSystemAndroidExperiments.shared.sentObjects.ServerToClient.ErrorData;
import com.example.brere.ParkingLotOrganizationSystemAndroidExperiments.shared.sentObjects.clientToServer.ModifyingDataBaseServerFunction;
import com.example.brere.ParkingLotOrganizationSystemAndroidExperiments.shared.sentObjects.clientToServer.dataBaseFunctions.DataBaseFunction;

import java.util.ArrayList;
import java.util.function.Consumer;
import java.util.function.Supplier;


//TODO REFRACTOR THIS INTO 1 GIANT SERVER SIDE CALL, for simplicity and efficentys sake
/**
 * this is a class for calling multiple server functions for multiple peices of
 * data at the same time. this can be used to simply insert or modify several
 * peices of data at the same time in a reletivly elegant way. Like if you want
 * to insert 4 peices of data at the same time, you can use a MultiServerCallMethod
 * to do that in one line.
 *
 * If there is an error while doing any given serverFunction, the method will stop the rest
 * from happening
 * @author brere
 */
public class MultiServerCallMethod implements Consumer<ArrayList<DataFieldList>> {
	
	/** the method which will construct a dataBaseFunction that will be sent to the server*/
	private DataBaseFunctionConstructor function;
	/** a mehtod which gets the ServerComunicator (must be supplier in this version because
	 * there is no guarentee that the serverCommunicator will be instantiated by the time
	 * the MultiServerCallMethod is instantiated) */
	private Supplier<ServerCommunicator> comunicatorConstructor;
	/** the thing that will be done when a server error occurs is completed successfully */
	private Consumer<ArrayList<DataFieldList>> onSuccess;
	/** the thing that happens when there is an error*/
	private Consumer<ErrorData> onError;
	/**the name of the dataBaseFunction */
	private String databaseFunctionName;
	/**the text of the last Table sent */
	private String lastListTableTitle;
	/** the progress through the loop */
	private int i;
	/** the inputed DataFieldList */
	private ArrayList<DataFieldList> input;
	/**the serverComunicator used for comunication */
	private ServerCommunicator communicator;

	/**
	 * constructs a dataBaseFunction, using the inputed DataFieldList
	 */
	public static interface DataBaseFunctionConstructor {
		public DataBaseFunction construct(DataFieldList d);
	}

	/**
	 * constructs a multiServerCallMethod object
	 * @param onsuccess what to do when it is successful
	 * @param onerror what to do when there is an error
	 * @param function what constructs the DataBaseFunction
	 * @param comunicatorConstructor what constructs the serverComunicator
	 */
	public MultiServerCallMethod(Consumer<ArrayList<DataFieldList>> onsuccess, Consumer<ErrorData> onerror,
			DataBaseFunctionConstructor function, Supplier<ServerCommunicator> comunicatorConstructor) {
		super();
		this.onSuccess = onsuccess;
		this.onError = onerror;

		this.function = function;
		this.comunicatorConstructor = comunicatorConstructor;
	}

	

	@Override
	public void accept(ArrayList<DataFieldList> list) {

		 communicator=comunicatorConstructor.get();

		if (onError == null) {
			onError = communicator.getDefaultErrorMessageDisplayer();
		}
		if (onSuccess == null) {
			onSuccess = (ArrayList<DataFieldList> d) -> {
				this.onError.accept(
						new ErrorData("your " + databaseFunctionName + " of a " + lastListTableTitle + " completed successfully",ErrorData.ErrorType.DEFAULTSUCESSMESSAGE));
			};
		}




		input=list;
		i=0;


		if (!list.isEmpty()) {
			databaseFunctionName = function.construct(list.get(0)).getNameOfFunction();
		}
		sendAList(list);
	}

	/**
	 * sends a single list to the database, in a databaseFunction constructed by the databaseFunctionConstructor
	 * @param list the lsit that will be sent to the database in a databasefunction.
	 */
	private void sendAList(ArrayList<DataFieldList> list) {
		if(i>=list.size()) { return;}
		lastListTableTitle = list.get(i).getTable().getTitle();

		communicator.sendStuffToServer(this::onSuccess, this.onError,
				new ModifyingDataBaseServerFunction(function.construct(list.get(i))));
		i++;
	}

	/**
	 * activates whenever there is a success
	 * @param d the result of a successful server call
	 */
	public void onSuccess(ArrayList<DataFieldList> d) {
		this.onSuccess.accept(d);
		sendAList(input);
	}

	
}
