package com.example.brere.ParkingLotOrganizationSystemAndroidExperiments.shared.sentObjects.clientToServer;

import com.example.brere.ParkingLotOrganizationSystemAndroidExperiments.client.dataFieldStuff.DataFieldList;
import com.example.brere.ParkingLotOrganizationSystemAndroidExperiments.shared.sentObjects.ServerToClient.WhatToDoWithClientSavedInstructions;
import com.example.brere.ParkingLotOrganizationSystemAndroidExperiments.shared.sentObjects.clientToServer.dataBaseFunctions.DataBaseFunction;
import com.example.brere.ParkingLotOrganizationSystemAndroidExperiments.shared.stuffInTheServer.ServerError;
import com.example.brere.ParkingLotOrganizationSystemAndroidExperiments.shared.stuffInTheServer.ServerSystemsToolBox;

import java.util.ArrayList;

/**
 * this is a method for modifying or searching things in the database. it
 * commands the server to use a method in the database, and to return the result
 * of the search/insert/delete to the client.
 *
 * @author brere
 */
public class ModifyingDataBaseServerFunction extends ServerFunction<ArrayList<DataFieldList>>{
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -7069410541818511911L;
	/** The database function which will be executed */
	private DataBaseFunction function;
	/**
	 * Instantiates a new modifying data base server function, which will do something with the database
	 * @param function the DataBase server function which will be executed by the database
	 */
	public ModifyingDataBaseServerFunction(DataBaseFunction function) {
		super(WhatToDoWithClientSavedInstructions.DELETE);
		if(function==null) {
			throw new IllegalArgumentException("error, you set the function in ModifyingDataBaseServerFunction To Null");
		}
		this.function=function;
	}
	/**
	 * this is the main important method here, you can really ignore the rest of the
	 * stuff, and understand what it is doing by simply knowing how this method
	 * works. basically it uses the toolbox given to it by the server to do
	 * something in the database. it sends the result of the database operation back
	 * to the client, whether it was an error message, or the data.
	 *
	 * note: this method will throw a Server Error when something goes wrong/ an
	 * error message needs to be sent to the client.
	 *
	 * @param box the box
	 * @return the outcome of the query/insertion/etc
	 * @throws ServerError             when there is an error while executing the function in the
	 *             database
	 */
	@Override
	public  ArrayList<DataFieldList>executeServerFunction(ServerSystemsToolBox box) throws ServerError {
			return executeDataBaseFunction(box);
	}
	/**
	 *
	 * executes the databaseFunction. this method needed to exist in a previous
	 * build, but probably shouldnt anymore
	 *
	 * @param box
	 *            the toolbox that will be used to execute the database function
	 * @return the outcome of the dataBaseFunction
	 * @throws ServerError
	 *             when there is an error in the dataBaseFunction execution
	 */
	protected ArrayList<DataFieldList> executeDataBaseFunction(ServerSystemsToolBox box) throws ServerError {
		return box.getDataBaseFunctionExecutor().executeDataBaseServerFunction(function);
	}
}