package com.example.brere.ParkingLotOrganizationSystemAndroidExperiments.client.systems.specific;

import android.os.Bundle;

import com.example.brere.ParkingLotOrganizationSystemAndroidExperiments.client.dataFieldStuff.DataFieldList;
import com.example.brere.ParkingLotOrganizationSystemAndroidExperiments.client.dataFieldStuff.TableCollection;
import com.example.brere.ParkingLotOrganizationSystemAndroidExperiments.client.systems.ControlSystem;
import com.example.brere.ParkingLotOrganizationSystemAndroidExperiments.client.systems.util.controlSystemHandeler.ControlSystemHandeler;
import com.example.brere.ParkingLotOrganizationSystemAndroidExperiments.client.systems.util.insertionSystem.InsertionSystem;
import com.example.brere.ParkingLotOrganizationSystemAndroidExperiments.client.systems.util.listSystem.ListSystem;

import java.util.ArrayList;

/**
 * this class is a place which holds the methods for creating the bundle to launch the Admin System
 * and the guard system.
 */
public class UserSystemsBundleCreationMethods {
	/**
	 * creates a bundle that can launch the guard control system
	 * @return a bundle that can launch the guard control system
	 */
	public static Bundle createGuardSystem(){
		DataFieldList user = TableCollection.USERTABLE.table.getAsDataFieldList();
		ArrayList<Bundle> bundles=new ArrayList<>();

		bundles.add(InsertionSystem.createBundleForSystem(user));

		bundles.add(ListSystem.createBundleForSystem(TableCollection.TICKETTABLE.table));
		bundles.add(InsertionSystem.createBundleForSystem(TableCollection.TICKETTABLE.table.getAsDataFieldList()));

		return ControlSystemHandeler.createBundleForSystem("guard system", bundles);
	}
	/**
	 * creates a bundle that can launch the admin control system
	 * @return a bundle that can launch the admin control system
	 */
	public static Bundle createAdminSystem(){
		ArrayList<Bundle> adminSystemBundle=new ArrayList<>();

		ArrayList<DataFieldList> dataFieldLists=new ArrayList<>();
		dataFieldLists.add(TableCollection.USERTABLE.table.getAsDataFieldList());
		dataFieldLists.add(TableCollection.WORKDAYS.table.getAsDataFieldList());

		adminSystemBundle.add(InsertionSystem.createBundleForSystem(dataFieldLists));
		adminSystemBundle.add(ListSystem.createBundleForSystem(TableCollection.USERTABLE.table));
		adminSystemBundle.add(ListSystem.createBundleForSystem(TableCollection.WORKDAYS.table));


		adminSystemBundle.add(InsertionSystem.createBundleForSystem(TableCollection.TICKETTABLE.table.getAsDataFieldList()));
		adminSystemBundle.add(ListSystem.createBundleForSystem(TableCollection.TICKETTABLE.table));

		adminSystemBundle.add(InsertionSystem.createBundleForSystem(TableCollection.VEHICLETABLE.table.getAsDataFieldList()));
		adminSystemBundle.add(ListSystem.createBundleForSystem(TableCollection.VEHICLETABLE.table));

		adminSystemBundle.add(InsertionSystem.createBundleForSystem(TableCollection.PARKINGLOTTABLE.table.getAsDataFieldList()));
		adminSystemBundle.add(ListSystem.createBundleForSystem(TableCollection.PARKINGLOTTABLE.table));

		adminSystemBundle.add(InsertionSystem.createBundleForSystem(TableCollection.PARKINGPASSTABLE.table.getAsDataFieldList()));
		adminSystemBundle.add(ListSystem.createBundleForSystem(TableCollection.PARKINGPASSTABLE.table));

		adminSystemBundle.add(InsertionSystem.createBundleForSystem(TableCollection.ASSIGNMENTS.table.getAsDataFieldList()));
		adminSystemBundle.add(ListSystem.createBundleForSystem(TableCollection.ASSIGNMENTS.table));

		adminSystemBundle.add(InsertionSystem.createBundleForSystem(TableCollection.OWNERTABLE.table.getAsDataFieldList()));
		adminSystemBundle.add(ListSystem.createBundleForSystem(TableCollection.OWNERTABLE.table));

		adminSystemBundle.add(InsertionSystem.createBundleForSystem(TableCollection.DISPUTETABLE.table.getAsDataFieldList()));
		adminSystemBundle.add(ListSystem.createBundleForSystem(TableCollection.DISPUTETABLE.table));

		return ControlSystemHandeler.createBundleForSystem("admin system",adminSystemBundle);
	}
}
