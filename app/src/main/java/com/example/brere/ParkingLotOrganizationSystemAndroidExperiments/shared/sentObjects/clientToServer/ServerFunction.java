package com.example.brere.ParkingLotOrganizationSystemAndroidExperiments.shared.sentObjects.clientToServer;

import com.example.brere.ParkingLotOrganizationSystemAndroidExperiments.shared.sentObjects.ServerToClient.ErrorData;
import com.example.brere.ParkingLotOrganizationSystemAndroidExperiments.shared.sentObjects.ServerToClient.ErrorMessageFromServerToClient;
import com.example.brere.ParkingLotOrganizationSystemAndroidExperiments.shared.sentObjects.ServerToClient.SuccessMessageFromServerToClient;
import com.example.brere.ParkingLotOrganizationSystemAndroidExperiments.shared.sentObjects.ServerToClient.WhatToDoWithClientSavedInstructions;
import com.example.brere.ParkingLotOrganizationSystemAndroidExperiments.shared.stuffInTheServer.ServerError;
import com.example.brere.ParkingLotOrganizationSystemAndroidExperiments.shared.stuffInTheServer.ServerSystemsToolBox;

import java.io.IOException;
import java.io.Serializable;
import java.util.function.Consumer;

/**
 * this class which is basically a method for the server to execute. When the
 * server receives this thing, it will execute the accept(ServerSystemsToolBox)
 * abstract method, which will then execute the executeServerFunction abstract
 * method. the result of the executeServerFunction method will be packaged, and
 * sent back to the user.
 *
 * the only thing that you really need to know, is to send a subclass of
 * ServerFunction to the server, and the result of executeServerFunction will be
 * recieved by the client, or if an exception was thrown, the message of the
 * exception will be sent to the client.
 *
 * @author brere
 * @param <T> the type of data sent back to the client when it is successful
 */
public abstract class ServerFunction<T> implements Consumer<ServerSystemsToolBox>,Serializable{
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1720851923542547567L;
	/** The id. of the serverFunction. it matches the id of the instructions that you have client side*/
	private int id;
	/** Instructions for what to do with the Clients instructions. */
	private WhatToDoWithClientSavedInstructions whatToDo;
	/**
	 * Instantiates a new server function.
	 * @param whatToDo what to do with the instructions the client has for the output of the serverFunction.
	 * if you dont know what that means, just use WhatToDoWithClientSavedInstructions.DELETE
	 */
	public ServerFunction(WhatToDoWithClientSavedInstructions whatToDo) {
		this.whatToDo=whatToDo;
	}
	/**
	 * Sets the id of the server function
	 *
	 * @param id the new id
	 */
	public void setID(int id) {
		this.id=id;
	}
	/* (non-Javadoc)
	 * @see java.util.function.Consumer#accept(java.lang.Object)
	 */
	public void accept(ServerSystemsToolBox box) {
		T sucessResult=null;
		
		try {
			 sucessResult=executeServerFunction(box);
		}catch(ServerError e) {
			sendErrorMessage(box,e.getData());
			return;
		}
		catch(IllegalArgumentException e) {
		 sendErrorMessage(box, new ErrorData(e.getMessage(),ErrorData.ErrorType.ILLEGALARGUMENT));
		 return;
		}catch(Exception e) {
			String error="error, we found an unknown error server side, please contact the developers for more information \r\n";
			error+="error code: "+e.getMessage();
			e.printStackTrace();
			sendErrorMessage(box, new ErrorData(error,ErrorData.ErrorType.UNKOWN));
			return;
		}
		this.sendSuccessMessage(box, sucessResult);
	}
	/**
	 * this is the method that you should be primarily concerned about, the rest is
	 * server Side stuff that you dont need to know this method the server will
	 * execute when it receives your message. The return value of this method will
	 * be sent back to the user
	 *
	 * @param box
	 *            the ToolBox the serverFunction will use to get its wanted output
	 * @return what you want the client to recieve back when the function is
	 *         executed correctly
	 * @throws IllegalArgumentException
	 *             when there is an illegal Argument in the data
	 * @throws ServerError
	 *             when there is any error that happens server side.
	 */
	protected abstract T executeServerFunction(ServerSystemsToolBox box)throws IllegalArgumentException,ServerError;
	/**
	 * this is a method for sending a sucess message. the only thing that needs to
	 * be done, is sending the result of the search/modify/etc
	 *
	 * @param box
	 *            the box that it will use to send the message
	 * @param result
	 *            the result of the search/etc.
	 */
	private void sendSuccessMessage(ServerSystemsToolBox box,T result){
		SuccessMessageFromServerToClient toClient=new SuccessMessageFromServerToClient(whatToDo,id,result);
		try {
			box.getSenderToClient().send(toClient);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	/**
	 * this is a method which is for the sublcasses, if they want to send an error
	 * message. note: using this method is probably bad practice, since you can
	 * simply throw a ServerError, and It will do the same thing
	 *
	 * @param box
	 *            the box it will use to send the error message
	 * @param errorMessage
	 *            the error message
	 */
	private void sendErrorMessage(ServerSystemsToolBox box,ErrorData errorMessage) {
		ErrorMessageFromServerToClient errorMessageForClient=new ErrorMessageFromServerToClient(whatToDo, id,errorMessage);
		try {
			box.getSenderToClient().send(errorMessageForClient);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	
}
