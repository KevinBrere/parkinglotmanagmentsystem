package com.example.brere.ParkingLotOrganizationSystemAndroidExperiments.client.systems.util.insertionSystem;

import android.os.Bundle;
import android.support.annotation.NonNull;

import com.example.brere.ParkingLotOrganizationSystemAndroidExperiments.client.dataFieldStuff.DataFieldList;
import com.example.brere.ParkingLotOrganizationSystemAndroidExperiments.pretend.ControlSystemsActivity;
import com.example.brere.ParkingLotOrganizationSystemAndroidExperiments.shared.sentObjects.clientToServer.dataBaseFunctions.DataBaseFunction;
import com.example.brere.ParkingLotOrganizationSystemAndroidExperiments.shared.sentObjects.clientToServer.dataBaseFunctions.InsertionDataBaseFunction;

import java.util.ArrayList;

/**
 * REQUIRES: the DataFieldList in the insertion view to be the same
 * datafieldlist in InsertionSystem
 *
 *
 */


public class InsertionSystem extends UserInputSystem {
	/**
	 * creates a bundle for launching a InsertionSystem for Inserting the inputed dataFieldLists into the database
	 * @param dataFieldLists the data that will be inserted by the user
	 * @return a bundle for launching a InsertionSystem for inserting the inputed dataFieldLists into the database
	 */
	public static Bundle createBundleForSystem( @NonNull ArrayList<DataFieldList> dataFieldLists) {
		Bundle b = UserInputSystem.createBundleForSystem("Add a "+dataFieldLists.get(0).getTable().getTitle(),dataFieldLists);
		ControlSystemsActivity.CreateControlSystem instantiate = InsertionSystem::new;
		b.putSerializable(ControlSystemsActivity.CONTROLSYSTEMGENERATEORID, instantiate);
		return b;
	}

	/**
	 * creates a bundle for launching a InsertionSystem for Inserting the inputed dataFieldLists into the database
	 * @param list the data that will be inserted by the user
	 * @return a bundle for launching a InsertionSystem for inserting the inputed dataFieldLists into the database
	 */
	public static Bundle createBundleForSystem(@NonNull DataFieldList list) {
		ArrayList<DataFieldList> dataFieldLists = new ArrayList<>();
		dataFieldLists.add(list);
		return InsertionSystem.createBundleForSystem(dataFieldLists);
	}

	@Override
	public void setView(InsertionView view) {
		super.setView(view);
		addDataFieldListConsumerButton("add",
				new MultiServerCallMethod(null, null, this::createServerFunction, super::getServ));
	}

	/**
	 * creates a database function that  the InsertionSystem wantes
	 * @param list the list that will be ued to create the database function
	 * @return an insertion database function
	 */
	public DataBaseFunction createServerFunction(DataFieldList list)
	{
		return new InsertionDataBaseFunction(list);
	}

}
