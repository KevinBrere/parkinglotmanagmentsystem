package com.example.brere.ParkingLotOrganizationSystemAndroidExperiments.client.dataFieldStuff;

import android.support.annotation.NonNull;
import android.view.ViewGroup;

import com.example.brere.ParkingLotOrganizationSystemAndroidExperiments.client.systems.util.insertionSystem.InsertionAdapter;
import com.example.brere.ParkingLotOrganizationSystemAndroidExperiments.client.systems.util.insertionSystem.individualColumnStuff.DataFieldInputManager;
import com.example.brere.ParkingLotOrganizationSystemAndroidExperiments.client.systems.util.insertionSystem.individualColumnStuff.DataInputBox;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.function.Supplier;

/**
 *
 *this is the class which basically sets up a DataFieldInputManager, so that it can properly get its input
 * into its DataFields. These objects do this by setting the managers supplier to the supplier that it needs
 * and then sends the supplier where it needs to go. Like if the supplier needs to be a part of the InsertionView
 * it would go into the insertionViews arrayList
 *
 * this class had to be heavily modified in order to be ported to android. Instead of reaturning the supplier when it is constructed, it now also "routes" the output to where it needs to
 * go, ie if the output is a datafieldinputBox, it adds the datafieldInputBox's constructor to the insertionview arraylist.
 *
 * to get the constructorofDataFeildInputBox, please use the constuctDataFieldInputBox static method in each DataFieldInputBox extension.
 * @author brere
 *
 */

public interface SetterUpOfDataFieldInputManager extends Serializable {
	/**
	 * constructs the dataFieldInputBox, so that it can be used in a UserInputSystem
	 * @param d the datafield that will go into the new dataFieldInputManager
	 * @param fieldBelongsTo the DataFieldList That the d belongs to
	 * @param constructors an arrayList of DataInputBoxConstructor's which the InsertionView will use. like if you want the inputed
	 *                     supplier to be visable, you must add it to this list.
	 * @param inputmanagers a list of dataFieldInputManagers, which this method will add a new one to.
	 */
	public void constructDataFieldInputBox(DataField d, DataFieldList fieldBelongsTo, ArrayList<InsertionAdapter.DataInputBoxConstructor> constructors, ArrayList<DataFieldInputManager> inputmanagers);


	  interface SerializableSupplier extends Supplier<String>, Serializable{
	}
//	public static Supplier<String> createLoginPasswordSupplier(DataField d) {
//
//			JPasswordField paswordfield=new JPasswordField(10);
//			paswordfield.setEchoChar('*');
//			//change this later.
//			DefaultDataInputBox password=new DefaultDataInputBox(d);
//			password.setField(paswordfield);
//			return password;
//
//	}

	/**
	 * this method creates a SetterUpOfDataFieldInputManager, which will set up a DataFieldInputManager
	 * so that it will supply the inputed string to all of its dataFields.
	 *
	 * @param s the string which the end SetterUpOfDataFieldInputManager's outputed supplier will output.
	 * @return a SetterUpOfDataFieldInputManager which will return a supplier, which will return the inputed argument.
	 */
	public static SetterUpOfDataFieldInputManager createSetStringSupplierConstructor(String s) {
		return new SetterUpOfDataFieldInputManager() {
			private static final long serialVersionUID = -8555095050346472464L;

			@Override
			public void constructDataFieldInputBox(DataField d, DataFieldList fieldBelongsTo, ArrayList<InsertionAdapter.DataInputBoxConstructor> constructors, ArrayList<DataFieldInputManager> inputManagers) {

				inputManagers.add(new DataFieldInputManager(generateSerializableSupplier(s), d));
			}
		};
	}

	/**
	 * Generates a serializable string supplier, which will return the inputed string in its "get"
	 * method
	 * @param s the string that will be returned in the suppliers "get()" method
	 * @return a serializalbe supplier which will return the inputed sting in its get() method
	 */
	 static SerializableSupplier generateSerializableSupplier(String s){
		 return new SerializableSupplier(){
			 private static final long serialVersionUID = -8558095050346472465L;
			 @Override
			 public String get() {
				 return s;
			 }
		 };
	}

	/**
	 * this method returns a SetterUpOfDataFileInputManager, which will set up a DataInputManager such that it will
	 * read the DataInputBox (after it is constructed) and put that data into its datafields.
	 *
	 * @param inputConstructor the Constructor of the DataInputBox which will Feed its output to the DataInputManager
	 *                         after it is constructed
	 * @return a SetterUpOfDataFieldInputManager which will set up a DataFieldInputManager such that the inputed DataInputBox's output
	 * will be fed to the DataInputManagers datafields.
	 */
	public static SetterUpOfDataFieldInputManager createDataInputBoxInputConstructor(DataInputBox.DataInputBoxConstructor inputConstructor) {
		return new SetterUpOfDataFieldInputManager() {
			private static final long serialVersionUID = -8555095050346472465L;

			@Override
			public void constructDataFieldInputBox(DataField d, DataFieldList fieldBelongsTo, ArrayList<InsertionAdapter.DataInputBoxConstructor> constructors, ArrayList<DataFieldInputManager> inputManagers) {
				DataFieldInputManager manager = new DataFieldInputManager( generateSerializableSupplier(null), d);
				inputManagers.add(manager);
				InsertionAdapter.DataInputBoxConstructor constructor = new InsertionAdapter.DataInputBoxConstructor() {
					private static final long serialVersionUID = -8555095050346472365L;

					@Override
					public DataInputBox createInputBox(@NonNull ViewGroup parent, int viewType) {
						DataInputBox box = inputConstructor.constructDataInputBox(d, fieldBelongsTo, parent, viewType);
						manager.setInput(box);
						return box;
					}
				};
				constructors.add(constructor);
			}
		};
	}
}