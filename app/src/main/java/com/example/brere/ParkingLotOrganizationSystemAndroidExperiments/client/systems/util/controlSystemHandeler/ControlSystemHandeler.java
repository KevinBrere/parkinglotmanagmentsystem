package com.example.brere.ParkingLotOrganizationSystemAndroidExperiments.client.systems.util.controlSystemHandeler;



import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.brere.ParkingLotOrganizationSystemAndroidExperiments.R;
import com.example.brere.ParkingLotOrganizationSystemAndroidExperiments.client.systems.ControlSystem;
import com.example.brere.ParkingLotOrganizationSystemAndroidExperiments.pretend.ControlSystemsActivity;

import java.util.ArrayList;

/**
 * this class basically is the "menu" system that we have, which displays a list of all of the controlSystems
 * available. when one of these controlSystems titles is clicked on, then it will open that controlSystem
 */
public class ControlSystemHandeler extends ControlSystem {
	/**constant for creating a bundle */
	public static final String BUNDLESFORACTIVITIES="PRETENDFINALTRY2.pretend.OriginalView.BundlesForActivities";

	/**
	 * this is the main instantiator for the bundle if you want to use this ControlSystem. use the outputed bundle
	 * to launch the controlSystem
	 * @param title the title of the system
	 * @param bundlesForOtherSystem the bundles, for each other controlSystem which it will launch,using
	 *        its menu
	 * @return a bundle which will be able to launch the controlSystem
	 */
	public static Bundle createBundleForSystem(@NonNull String title, @NonNull ArrayList<Bundle> bundlesForOtherSystem){
		Bundle b=ControlSystem.createBundleForSystem(title);
		b.putParcelableArrayList(BUNDLESFORACTIVITIES,bundlesForOtherSystem);
		ControlSystemsActivity.CreateControlSystem instantiate=ControlSystemHandeler::new;
		b.putSerializable(ControlSystemsActivity.CONTROLSYSTEMGENERATEORID,instantiate);
		return b;
	}


	@Override
	public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
							 @Nullable Bundle savedInstanceState) {

		View rootView=inflater.inflate(R.layout.control_system_handeler_view,container,false);

		ArrayList<? extends Parcelable> parcelables= getActivity().getIntent().getExtras().getParcelableArrayList(BUNDLESFORACTIVITIES);
		ArrayList<Bundle> bundles=(ArrayList<Bundle>)parcelables;

		ControlSystemHandlerRecyclerAdapter adapt=new ControlSystemHandlerRecyclerAdapter(bundles,this::useHolder);

		RecyclerView rec=rootView.findViewById(R.id.RecyclerViewForControlSystems);
		rec.setLayoutManager(new LinearLayoutManager(this.getActivity()));
		rec.setAdapter(adapt);

		return rootView;
	}

	/**
	 * a method for launching an activity using the SingLeSystemViewHolders bundle. It is used as an
	 * onClickListener for each view in the ControlSystemHandelerRecyclerAdapter
	 * @param s the viewHolder that It will use the bundle of to launch the activity
	 */
	private  void useHolder(ControlSystemHandlerRecyclerAdapter.SingleSystemViewHolder s){
		Intent intent = new Intent(this.getActivity(), ControlSystemsActivity.class);
		intent.putExtras(s.getBundle());
		startActivity(intent);
	}
}
